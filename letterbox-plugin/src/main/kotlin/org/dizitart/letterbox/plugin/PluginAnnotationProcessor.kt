package org.dizitart.letterbox.plugin

import org.dizitart.letterbox.api.plugin.PluginManifest
import org.dizitart.letterbox.api.plugin.PluginManifestEntry
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic


/**
 *
 * @author Anindya Chatterjee
 */
class PluginAnnotationProcessor : AbstractProcessor() {
    private val index = HashMap<String, MutableSet<String>>()
    private var indexWriter: AnnotationIndexWriter? = null

    companion object {
        internal const val manifest = "manifest"
        internal const val id = "id"
        internal const val version = "version"
        internal const val dependency = "dependency"
    }

    @Synchronized
    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        indexWriter = AnnotationIndexWriter(this)
        info("%s init", PluginAnnotationProcessor::class)
    }

    override fun getSupportedAnnotationTypes(): Set<String> {
        return setOf(PluginManifest::class.java.canonicalName)
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment?): Boolean {
        if (roundEnv?.processingOver() == true) {
            return false
        }

        val manifestElements = roundEnv?.getElementsAnnotatedWith(PluginManifest::class.java)

        if (manifestElements == null) {
            error("No plugin manifest found")
            throw IllegalStateException("No plugin manifest found")
        }

        if (manifestElements.size > 1) {
            error("More than one plugin manifest found. It should be only one")
            throw IllegalStateException("More than one plugin manifest found")
        }

        manifestElements.first().apply {
            info("Plugin Manifest found %s", this)

            if (this !is TypeElement) {
                error("The annotation should be used on class only")
                throw IllegalStateException("Element is not a TypeElement")
            }

            if (!isManifest(this.asType())) {
                return@apply
            }

            validateConstructor(this, false)

            val pluginManifest = this.getAnnotation(PluginManifest::class.java)
            val pluginId = pluginManifest.uniqueId
            val pluginVersion = pluginManifest.version
            val dependsOn = pluginManifest.dependsOn

            index[id] = mutableSetOf(pluginId)
            index[version] = mutableSetOf(pluginVersion)
            index[dependency] = dependsOn.map { it -> "${it.pluginId}:${it.versionSupport}" }.toMutableSet()

            val manifestEntry = getBinaryName(this)
            index[manifest] = mutableSetOf(manifestEntry)
        }

        indexWriter?.write(index)
        return false
    }

    private fun validateConstructor(element: TypeElement, isExtension: Boolean) {
        val enclosedElements = element.enclosedElements
        val constructors = mutableListOf<ExecutableElement>()
        for (enclosedElement in enclosedElements) {
            if (enclosedElement.kind == ElementKind.CONSTRUCTOR) {
                constructors.add(enclosedElement as ExecutableElement)
            }
        }

        var ctorFound = false
        for (constructor in constructors) {
            if (constructor.parameters.size == 0) {
                ctorFound = true
                break
            }
        }

        if (!ctorFound) {
            error("No parameter-less constructor found for ${if (isExtension) "Extension" else "Plugin Manifest" }")
            throw IllegalStateException("${if (isExtension) "Extension" else "Plugin Manifest" } should have one parameter-less constructor")
        }
    }

    internal fun info(message: String?, vararg args: Any) {
        processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, String.format(message ?: "", *args))
    }

    internal fun getProcessingEnvironment(): ProcessingEnvironment {
        return processingEnv
    }

    private fun error(message: String?, vararg args: Any) {
        processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, String.format(message ?: "", *args))
    }

    private fun isManifest(typeMirror: TypeMirror): Boolean {
        return processingEnv.typeUtils.isAssignable(typeMirror, getManifestEntryType())
    }

    private fun getManifestEntryType(): TypeMirror {
        return processingEnv.elementUtils.getTypeElement(PluginManifestEntry::class.java.name).asType()
    }

    private fun getBinaryName(element: TypeElement): String {
        return processingEnv.elementUtils.getBinaryName(element).toString()
    }
}