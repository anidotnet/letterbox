# Improve Startup Time
    
    http://august.nagro.us/jvm-startup.html
    https://github.com/ReadyTalk/avian
    
## Startup JVM from Kotlin/Native
    
    Latest from Oracle
     - https://docs.oracle.com/javase/10/docs/specs/jni/invocation.html
     - https://github.com/dmlloyd/openjdk/blob/jdk/jdk10/test/jdk/sun/management/jmxremote/bootstrap/launcher.c
     - https://github.com/dmlloyd/openjdk/blob/jdk/jdk10/src/java.base/share/native/libjli/java.c
    
    http://www.inonit.com/cygwin/jni/invocationApi/c.html
    http://journals.ecs.soton.ac.uk/java/tutorial/native1.1/implementing/example-1.1/invoke.c
    https://www.developer.com/java/data/how-to-create-a-jvm-instance-in-jni.html
    https://www.codeproject.com/Articles/22881/How-to-Call-Java-Functions-from-C-Using-JNI
    
    
# Create Plugin Architecture

    https://proandroiddev.com/hello-world-of-annotation-processing-in-kotlin-3ec0290c1fdd
    https://medium.com/@Tarek360/annotation-processor-to-avoid-boilerplate-af0b8820297d
    
# Design

## Startup

1. At opening the application will load email/calendar/task/notes plugins
2. Email, Calendar, Task, Notes will register itself as a service provider to the core
3. Core will populate all the enabled service providers
4. 

## Email Steps

1. Email plugin will check the main database to look for any stored accounts
2. If accounts is found, it will create connections using the account settings and create connection pool using the connection
3. If no account found, it will show the account wizard to add account.

        3.1. Wizard will display various register account providers
        3.2. Upon selecting one account provider, user will be shown later wizard pages to input account settings
        3.3. All the input data is validated by successfully creating a connection
        3.4. An email account is created with all connection parameters and saved in the master database.
        3.5. A new account connection will be created and submitted to the connection pool
        3.6. A new account database would be created separated from the master database where all email messages will be saved

4. A sync thread will run in the background to invoke connections from the pool to synchronize local database
5. Local -> Remote Sync : The thread will check the flag `modified` for each object, if true it will queue it for synchronization
6. Remote -> Local Sync : The thread will follow each protocol specific settings to sync local objects to remote



## Sync Service

1. Upon starting of application initialize some background workers
2. Search for accounts in the main database
3. Initiate AccountSyncService for each account and register them to the background worker

## Plugin Service

1. Plugin Manager will look for the plugin path and filter out jars
2. It will scan the filtered jar and classpath jar for "plugins.idx" file
3. It will parse all the files and create a map of pluginManifest to plugins
4. It will create a PluginInfo object and save to the database
5. It will 



### Icon Color

normal - 969696
hover - 323232
active - 90CAF9