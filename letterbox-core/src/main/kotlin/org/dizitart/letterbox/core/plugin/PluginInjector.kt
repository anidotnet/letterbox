package org.dizitart.letterbox.core.plugin

import mu.KotlinLogging
import org.dizitart.letterbox.api.di.LetterBoxContext

/**
 * @author Anindya Chatterjee.
 */
class PluginInjector {
    private val logger = KotlinLogging.logger {  }

    fun inject(pluginId: String?, pluginInfo: PluginInfo?) {
        if (pluginInfo != null) {
            val beanConfig = pluginInfo.pluginManifest.beanCatalog
            LetterBoxContext.register(beanConfig)
        }
    }
}