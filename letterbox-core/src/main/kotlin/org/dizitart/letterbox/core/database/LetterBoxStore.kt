package org.dizitart.letterbox.core.database

import org.dizitart.kno2.nitrite
import org.dizitart.letterbox.api.Entity
import org.dizitart.letterbox.api.database.RepositoryOperation
import org.dizitart.letterbox.common.ACCOUNT_STORE_PATH
import org.dizitart.letterbox.common.APP_DATA_DIR
import org.dizitart.no2.Nitrite
import tornadofx.ConfigProperties
import tornadofx.beforeShutdown
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.collections.set

/**
 *
 * @author Anindya Chatterjee
 */
object LetterBoxStore {
    private lateinit var db: Nitrite
    private lateinit var repoMap: MutableMap<Class<*>, RepositoryOperation<*, *>>
    private lateinit var appConfig: ConfigProperties

    fun init(config: ConfigProperties) {
        appConfig = config

        db = nitrite {
            file = storeFile()
        }

        beforeShutdown {
            if (!db.isClosed) {
                db.close()
            }
        }

        repoMap = mutableMapOf()
    }

    @Suppress("UNCHECKED_CAST")
    fun <Id, T : Entity<Id>> repository(clazz: Class<T>) : RepositoryOperation<Id, T> {
        if (repoMap.containsKey(clazz)) {
            return repoMap[clazz] as RepositoryOperation<Id, T>
        }
        val repository = RepositoryOperation(db, clazz)
        repoMap[clazz] = repository
        return repository
    }

    private fun storeFile() : File {
        val storeDir = appConfig
                .string(ACCOUNT_STORE_PATH, APP_DATA_DIR + File.separator + "data")
        appConfig.save()

        if (!Files.exists(Paths.get(storeDir))) {
            Files.createDirectory(Paths.get(storeDir))
        }

        return File(storeDir + File.separator + "letterbox.db")
    }
}