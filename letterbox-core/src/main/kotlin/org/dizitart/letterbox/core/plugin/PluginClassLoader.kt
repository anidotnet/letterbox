package org.dizitart.letterbox.core.plugin

import mu.KotlinLogging
import java.io.IOException
import java.net.URL
import java.net.URLClassLoader
import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */
class PluginClassLoader(parent: ClassLoader,
                        private val pluginManager: PluginManager,
                        private val pluginMeta: PluginMeta)
    : URLClassLoader(arrayOf(), parent) {

    private val logger = KotlinLogging.logger {  }
    private val javaPackagePrefix = "java."

    init {
        addPath(pluginMeta.path)
    }

    public override fun addURL(url: URL?) {
        logger.debug { "Add '$url'" }
        super.addURL(url)
    }

    override fun getResource(name: String): URL? {
        logger.trace("Received request to load resource '$name'")
        val url = findResource(name)
        if (url != null) {
            logger.trace("Found resource '$name' in plugin classpath")
            return url
        }

        logger.trace("Couldn't find resource '$name' in plugin classpath. Delegating to parent")

        return super.getResource(name)
    }

    override fun loadClass(className: String?): Class<*> {
        synchronized(getClassLoadingLock(className)) {
            if (className?.startsWith(javaPackagePrefix) == true) {
                return findSystemClass(className)
            }

            var loadedClass: Class<*>? = findLoadedClass(className)
            if (loadedClass != null) {
                logger.trace("Found loaded class '$className'")
                return loadedClass
            }

            try {
                loadedClass = findClass(className)
                logger.trace("Found class '$className' in plugin classpath")
                return loadedClass
            } catch (e: ClassNotFoundException) {
                // try next step
            }

            loadedClass = loadClassFromDependencies(className)
            if (loadedClass != null) {
                logger.trace("Found class '$className' in dependencies")
                return loadedClass
            }

            logger.trace("Couldn't find class '$className' in plugin classpath. Delegating to parent")

            return super.loadClass(className)
        }
    }

    private fun loadClassFromDependencies(className: String?): Class<*>? {
        logger.trace("Search in dependencies for class '$className'")
        val dependencies = pluginMeta.dependencies
        for (dependency in dependencies) {
            val classLoader = pluginManager.getPluginClassLoader(dependency.pluginId)
            try {
                return classLoader?.loadClass(className)
            } catch (e: ClassNotFoundException) {
                // try next dependency
            }
        }
        return null
    }

    private fun addPath(path: Path) {
        try {
            addURL(path.toFile().canonicalFile.toURI().toURL())
        } catch (e: IOException) {
            logger.error(e) { e.message }
        }
    }
}