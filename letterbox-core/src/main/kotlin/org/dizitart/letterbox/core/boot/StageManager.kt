package org.dizitart.letterbox.core.boot

import dorkbox.systemTray.MenuItem
import dorkbox.systemTray.SystemTray
import javafx.application.Platform
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.stage.StageStyle
import mu.KotlinLogging
import org.dizitart.letterbox.common.fx.ResizeHelper
import tornadofx.*
import java.awt.event.ActionListener
import java.io.IOException

/**
 * @author Anindya Chatterjee.
 */
class StageManager(private val app: App) {
    private val logger = KotlinLogging.logger {  }

    fun prepareMainStage(stage: Stage) {
        // border less window
        stage.initStyle(StageStyle.TRANSPARENT)

        // load config
        loadMainStageConfig(stage)
    }

    fun showMainStage(stage: Stage) {
        // transparent background
        stage.scene.fill = Color.TRANSPARENT

        // resize handler
        ResizeHelper.addResizeListener(stage)

        // system tray
        createSystemTray(app.resources)
    }


    private fun createSystemTray(resources: ResourceLookup) {
        //NOTE: need to install for Ubuntu https://extensions.gnome.org/extension/1031/topicons/

        val systemTray = SystemTray.get() ?:
        throw RuntimeException("Unable to start SystemTray!")

        try {
            val osName = System.getProperty("os.name")
            val iconFileName = when {
                osName.contains("win") -> "/icons/icon.ico"
                else -> "/icons/icon.png"
            }
            systemTray.setImage(resources.stream(iconFileName))
        } catch (e: IOException) {
            logger.error(e) { "Error while loading icon" }
        }

        systemTray.menu.add(MenuItem("Exit", ActionListener {
            systemTray.shutdown()
            Platform.exit()
        }))
    }

    private fun loadMainStageConfig(stage: Stage) {
        stage.width = 760.0
        stage.height = 440.0

//        // bind maximize state with configBean
//        stage.isMaximized = LetterBoxContext.appConfig.boolean(MAIN_WINDOW_MAXIMIZED, true)!!
//        LetterBoxContext.appConfig.bind(MAIN_WINDOW_MAXIMIZED, stage.maximizedProperty())
//
//        // bind width with configBean
//        stage.width = LetterBoxContext.appConfig.double(MAIN_WINDOW_WIDTH, 760.0)!!
//        LetterBoxContext.appConfig.bind(MAIN_WINDOW_WIDTH, stage.widthProperty())
//
//        // bind height with configBean
//        stage.height = LetterBoxContext.appConfig.double(MAIN_WINDOW_HEIGHT, 440.0)!!
//        LetterBoxContext.appConfig.bind(MAIN_WINDOW_HEIGHT, stage.heightProperty())
    }
}