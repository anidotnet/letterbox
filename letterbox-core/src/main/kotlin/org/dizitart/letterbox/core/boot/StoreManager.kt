package org.dizitart.letterbox.core.boot

import mu.KotlinLogging
import org.dizitart.letterbox.core.database.LetterBoxStore
import tornadofx.ConfigProperties

/**
 * @author Anindya Chatterjee.
 */
class StoreManager {
    private val logger = KotlinLogging.logger {  }

    fun openLetterBoxStore(config: ConfigProperties) {
        try {
            LetterBoxStore.init(config)
        } catch (t: Throwable) {
            logger.error(t) { "Another instance is currently running" }
            tornadofx.error(header = "LetterBox is already opened", content = "Another instance of LetterBox is already" +
                    "running, please close that instance first.", title = "Error")
        }
    }
}