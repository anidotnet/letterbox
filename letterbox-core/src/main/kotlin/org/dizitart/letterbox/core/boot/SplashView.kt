package org.dizitart.letterbox.core.boot

import javafx.animation.FadeTransition
import javafx.concurrent.Task
import javafx.concurrent.Worker
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.ProgressBar
import javafx.scene.image.Image
import javafx.scene.paint.Color
import javafx.stage.Screen
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.util.Duration
import org.dizitart.letterbox.api.styles.BaseSplashTheme
import org.dizitart.letterbox.common.APP_ICON
import org.dizitart.letterbox.common.APP_VERSION
import org.dizitart.letterbox.common.SPLASH_HEIGHT
import org.dizitart.letterbox.common.SPLASH_WIDTH
import org.dizitart.letterbox.core.boot.styles.SplashTheme
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class SplashView : View() {
    private var loadProgress: ProgressBar? = null
    private var progressText: Label? = null

    override val root = vbox {
        addClass(BaseSplashTheme.screen)
        prefWidth = SPLASH_WIDTH
        prefHeight = SPLASH_HEIGHT
        region { minHeight = 250.0 }
        hbox {
            // providerLogo
            region { minWidth = 30.0 }
            label {
                addClass(BaseSplashTheme.firstLargeLabel)
                text = "Letter"
            }
            label {
                addClass(BaseSplashTheme.secondLargeLabel)
                text = "Box"
            }
            region { minWidth = 20.0 }
            label {
                addClass(BaseSplashTheme.versionLabel)
                text = APP_VERSION
            }
        }
        region { minHeight = 10.0 }
        // loadProgressBar bar
        loadProgress = progressbar {
            addClass(BaseSplashTheme.loadProgressBar)
            minWidth = this@vbox.prefWidth
        }
        hbox {
            // loadProgressBar text
            region { minWidth = 30.0 }
            progressText = label {
                addClass(BaseSplashTheme.progressLabel)
            }
        }
    }

    fun showSplash(task: Task<*>?, complete: () -> Unit) {
        // import default splash theme
        importStylesheet(SplashTheme::class)

        // bind label with loadProgressBar messages
        progressText?.textProperty()?.bind(task?.messageProperty())

        // bind loadProgressBar bar
        loadProgress?.progressProperty()?.bind(task?.progressProperty())

        val splashStage = Stage(StageStyle.UNDECORATED)
        splashStage.icons.add(Image(APP_ICON))

        // set a task completed callback to show main stage
        task?.stateProperty()?.onChange {
            if (it == Worker.State.SUCCEEDED) {
                // once the startup task is completed, fade it out
                loadProgress?.progressProperty()?.unbind()
                loadProgress?.progress = 1.0
                splashStage.toFront()
                val fadeSplash = FadeTransition(Duration.seconds(1.2), root)
                fadeSplash.fromValue = 1.0
                fadeSplash.toValue = 0.0
                fadeSplash.setOnFinished { _ -> splashStage.hide() }
                fadeSplash.play()

                // show main stage
                complete()
            }
        }

        val screenBounds = Screen.getPrimary().visualBounds
        splashStage.x = (screenBounds.width - SPLASH_WIDTH) / 2
        splashStage.y = (screenBounds.height - SPLASH_HEIGHT) / 2

        val splashScene = Scene(root, SPLASH_WIDTH, SPLASH_HEIGHT, Color.TRANSPARENT)

        FX.applyStylesheetsTo(splashScene)
        splashStage.scene = splashScene
        splashStage.initStyle(StageStyle.TRANSPARENT)
        splashStage.isAlwaysOnTop = true
        splashStage.show()
    }
}