package org.dizitart.letterbox.core.controllers

import javafx.scene.control.Button
import org.dizitart.letterbox.api.setActive
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class NotificationController: Controller() {
    fun setNotificationButton(button: Button) {
        button.setActive(false)
    }

    private fun showNotifications() {

    }
}