package org.dizitart.letterbox.core

import org.dizitart.letterbox.api.ComponentProvider
import org.dizitart.letterbox.api.di.LetterBoxContext

/**
 *
 * @author Anindya Chatterjee
 */
class ComponentManager {
    private val componentProviders = mutableListOf<ComponentProvider>()

    fun initializeProviders() {
        val componentProviderBeans = LetterBoxContext.getBeans(ComponentProvider::class)

        if (componentProviderBeans.isNotEmpty()) {
            componentProviderBeans.forEach { provider ->
                provider.init()
                if (provider.enabled) {
                    componentProviders.add(provider)
                }
            }
        }

        if (componentProviders.isNotEmpty()) {
            componentProviders.sortBy(ComponentProvider::ordinal)
        }
    }

    fun getComponentProviders(): List<ComponentProvider> = componentProviders
}