package org.dizitart.letterbox.core.boot

import org.dizitart.letterbox.api.di.beans
import org.dizitart.letterbox.core.ComponentManager
import org.dizitart.letterbox.core.config.LetterBoxConfigRepository
import org.dizitart.letterbox.core.controllers.*
import org.dizitart.letterbox.core.plugin.LetterBoxPluginManager
import org.dizitart.letterbox.core.plugin.PluginInjector
import org.dizitart.letterbox.core.plugin.PluginStateManager

/**
 *
 * @author Anindya Chatterjee
 */

val beanConfig = beans {
    // utils
    singleton { PluginInjector() }
    singleton { PluginStateManager() }
    singleton { LetterBoxPluginManager(get(), get()) }
    singleton { ComponentManager() }
    singleton { LetterBoxConfigRepository() }

    // controllers
    singleton { LetterBoxController() }
    singleton { LetterBoxMenuController() }
    singleton { NotificationController() }
    singleton { WizardController() }
    singleton { ConfigController() }

}