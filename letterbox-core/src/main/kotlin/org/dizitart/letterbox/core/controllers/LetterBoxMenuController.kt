package org.dizitart.letterbox.core.controllers

import javafx.application.Platform
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import javafx.scene.control.SeparatorMenuItem
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import org.dizitart.letterbox.api.ComponentProvider
import org.dizitart.letterbox.common.fx.createModal
import org.dizitart.letterbox.core.ProviderChangeEvent
import org.dizitart.letterbox.core.views.ConfigManagerView
import tornadofx.Controller
import tornadofx.action

/**
 * @author Anindya Chatterjee.
 */
class LetterBoxMenuController : Controller() {
    private var primaryMenu: MenuButton? = null

    init {
        subscribe<ProviderChangeEvent> { event ->
            constructMenu(event.provider)
        }
    }

    fun setPrimaryMenu(menuButton: MenuButton) {
        primaryMenu = menuButton
        letterBoxMenuItems.forEach { menuItem ->
            primaryMenu?.items?.add(menuItem)
        }
    }

    private fun constructMenu(provider: ComponentProvider) {
        primaryMenu?.items?.clear()
        provider.menu.forEach { menuItem ->
            primaryMenu?.items?.add(menuItem)
        }

        if (provider.menu.isNotEmpty()) {
            primaryMenu?.items?.add(SeparatorMenuItem())
        }

        letterBoxMenuItems.forEach { menuItem ->
            primaryMenu?.items?.add(menuItem)
        }
    }

    private val letterBoxMenuItems: Set<MenuItem> by lazy {
        setOf(
                accountMenu(),
                pluginsMenu(),
                preferenceMenu(),
                SeparatorMenuItem(),
                helpMenu(),
                aboutMenu(),
                exitMenu()
        )
    }

    private fun exitMenu(): MenuItem {
        return MenuItem("Exit").apply {
            accelerator = KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN)
            action {
                Platform.runLater {
                    Platform.exit()
                }
            }
        }
    }

    private fun aboutMenu(): MenuItem {
        return MenuItem("About").apply {
            action {
                TODO("not implemented")
            }
        }
    }

    private fun helpMenu(): MenuItem {
        return MenuItem("Help").apply {
            accelerator = KeyCodeCombination(KeyCode.F1)
            action {
                TODO("not implemented")
            }
        }
    }

    private fun preferenceMenu(): MenuItem {
        return MenuItem("Preference").apply {
            accelerator = KeyCodeCombination(KeyCode.S, KeyCombination.SHIFT_DOWN, KeyCombination.CONTROL_DOWN)
            action {
                val stage = createModal(ConfigManagerView())
                stage.initOwner(primaryStage)
                stage.show()
            }
        }
    }

    private fun pluginsMenu(): MenuItem {
        return MenuItem("Plugin").apply {
            action {
                TODO("not implemented")
            }
        }
    }

    private fun accountMenu(): MenuItem {
        return MenuItem("Account").apply {
            action {
                TODO("not implemented")
            }
        }
    }

}