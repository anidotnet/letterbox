package org.dizitart.letterbox.core.plugin

/**
 * @author Anindya Chatterjee.
 */
class PluginStateManager {
    fun getPluginState(pluginId: String): PluginState? {
        return PluginState(pluginId, true)
    }
}