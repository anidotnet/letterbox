package org.dizitart.letterbox.core.controllers

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TreeItem
import javafx.scene.layout.VBox
import org.dizitart.letterbox.api.config.ConfigView
import org.dizitart.letterbox.core.config.ConfigGroup
import org.dizitart.letterbox.core.config.ConfigGroupBuilder
import org.dizitart.letterbox.core.config.ConfigGroupChanged
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class ConfigController : Controller() {
    private val configGroupBuilder = ConfigGroupBuilder()

    internal lateinit var container: VBox

    init {
        subscribe<ConfigGroupChanged> {
            val view = it.configGroup.view
            if (view != null) {
                showSettings(view)
            }
        }
    }

    val rootNode by lazy {
        val root = createNode(configGroupBuilder.createConfigGroup())
        root
    }

    private fun createNode(configGroup: ConfigGroup): TreeItem<ConfigGroup> {
        return object: TreeItem<ConfigGroup>(configGroup) {
            private var isItLeaf: Boolean = false
            private var isFirstTimeChildren = true
            private var isFirstTimeLeaf = true

            init {
                isExpanded = true
            }

            override fun getChildren(): ObservableList<TreeItem<ConfigGroup>> {
                if (isFirstTimeChildren) {
                    isFirstTimeChildren = false
                    super.getChildren().setAll(buildChildren())
                }
                return super.getChildren()
            }

            override fun isLeaf(): Boolean {
                if (isFirstTimeLeaf) {
                    isFirstTimeLeaf = false
                    isItLeaf = value.children.isEmpty()
                }
                return isItLeaf
            }

            private fun buildChildren()
                    : ObservableList<TreeItem<ConfigGroup>> {
                if (value == null) return FXCollections.emptyObservableList()
                if (value.children.isEmpty()) return FXCollections.emptyObservableList()

                val groups = value.children
                val children = FXCollections.observableArrayList<TreeItem<ConfigGroup>>()
                groups.forEach { group ->
                    children.add(createNode(group))
                }
                return children
            }
        }
    }

    private fun showSettings(configView: ConfigView) {
        container.children.clear()
        container.children.add(configView.root)
    }
}