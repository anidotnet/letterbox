package org.dizitart.letterbox.core.config

import org.dizitart.letterbox.api.config.Configuration
import org.dizitart.letterbox.api.di.LetterBoxContext

/**
 * @author Anindya Chatterjee.
 */
internal class ConfigGroupBuilder {

    fun createConfigGroup(): ConfigGroup {
        val root = ConfigGroup("Settings")
        val groupMap = mutableMapOf<String, ConfigGroup>()
        val configurations = LetterBoxContext.getBeans(Configuration::class)

        configurations
                .forEach { configuration ->
                    val groups = configuration.groups
                    for ((index, value) in groups.withIndex()) {
                        // add children and find parent
                        if (index != 0) {
                            val parent = groups[index - 1]
                            var parentGroup = groupMap[parent]
                            if (parentGroup == null) {
                                parentGroup = ConfigGroup(parent)
                                groupMap[parent] = parentGroup
                            }
                            val group = ConfigGroup(value)
                            groupMap[value] = group
                            parentGroup.children.add(group)
                        } else {
                            if (!groupMap.containsKey(value)) {
                                val group = ConfigGroup(value)
                                groupMap[value] = group
                                root.children.add(group)
                            }
                        }

                        if (index == groups.size - 1) {
                            val group = groupMap[value]
                            group?.view = configuration.getView()
                        }
                    }
                }
        return root
    }
}