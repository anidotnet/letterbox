package org.dizitart.letterbox.core.controllers

import javafx.scene.control.Button
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import org.dizitart.letterbox.api.addImage
import org.dizitart.letterbox.api.di.LetterBoxContext
import org.dizitart.letterbox.api.setActive
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import org.dizitart.letterbox.core.ComponentManager
import org.dizitart.letterbox.core.ProviderChangeEvent
import tornadofx.*
import kotlin.collections.set

/**
 * @author Anindya Chatterjee.
 */
open class LetterBoxController : Controller() {
    private var container: AnchorPane? = null
    private var actionBar: HBox? = null
    private val tabButtonGraphicsMap = mutableMapOf<Button, CssRule>()

    fun setActionBar(hBox: HBox) {
        actionBar = hBox
    }

    fun setContainer(anchorPane: AnchorPane) {
        container = anchorPane
    }

    internal val serviceButtons: List<Button> by lazy {
        val buttons = mutableListOf<Button>()
        val componentManager = LetterBoxContext.getBean(ComponentManager::class)!!

        val componentProviders = componentManager.getComponentProviders().sortedBy { -it.ordinal }

        componentProviders.forEach { provider ->
            val button = Button().apply {
                addClass(LetterBoxStylesheet.lbButton)
                addImage(provider.tabGraphics)

                tabButtonGraphicsMap[this] = provider.tabGraphics

                setOnAction {
                    container?.children?.clear()
                    container?.addChildIfPossible(provider.pageView.root)

                    actionBar?.children?.clear()
                    actionBar?.addChildIfPossible(region { minWidth = 20.0 })

                    // main action button
                    actionBar?.addChildIfPossible(provider.actionView.root)
                    actionBar?.addChildIfPossible(region { minWidth = 20.0 })

                    // search view
                    actionBar?.addChildIfPossible(provider.searchView.root)

                    setActive(this)

                    fire(ProviderChangeEvent(provider))
                }
            }
            buttons.add(button)
        }
        buttons
    }

    private fun setActive(button: Button) {
        serviceButtons.forEach {
            it.setActive(false)
        }

        button.setActive(true)
    }
}
