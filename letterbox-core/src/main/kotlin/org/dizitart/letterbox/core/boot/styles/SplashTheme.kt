package org.dizitart.letterbox.core.boot.styles

import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import javafx.scene.paint.LinearGradient
import javafx.scene.text.FontWeight
import org.dizitart.letterbox.api.styles.BaseSplashTheme
import org.dizitart.letterbox.api.styles.BlankPalette
import org.dizitart.letterbox.common.fx.url
import tornadofx.box
import tornadofx.em
import tornadofx.px

/**
 *
 * @author Anindya Chatterjee
 */
class SplashTheme : BaseSplashTheme(BlankPalette()) {

    init {
        screen {
            backgroundImage += url("splashes/ray_splash.jpg")
            backgroundSize += BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, true, true, false, true)
        }

        loadProgressBar {
            padding = box(10.px)
        }

        loadProgressBar child track {
            backgroundColor += Color.TRANSPARENT
        }

        loadProgressBar child bar {
            backgroundInsets += box(0.px)
            padding = box(0.05.em)
            backgroundRadius += box(0.px)
            backgroundColor += LinearGradient.valueOf("linear-gradient(to bottom, #0096C9, #0096C9)")
        }

        progressLabel {
            textFill = Color.WHITE
            fontSize = 11.px
            fontWeight = FontWeight.EXTRA_LIGHT
        }

        firstLargeLabel {
            textFill = Color.WHITE
            fontSize = 25.px
            fontWeight = FontWeight.BOLD
            fontFamily = "Nunito"
        }

        secondLargeLabel {
            textFill = Color.DEEPSKYBLUE
            fontSize = 25.px
            fontWeight = FontWeight.BOLD
            fontFamily = "Nunito"
        }

        versionLabel {
            textFill = Color.WHITE
            fontSize = 13.px
        }
    }
}