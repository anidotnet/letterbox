package org.dizitart.letterbox.core.boot

import javafx.concurrent.Task
import mu.KotlinLogging
import org.dizitart.letterbox.common.styles.ThemeManager.loadThemes
import org.dizitart.letterbox.common.APP_DATA_DIR
import org.dizitart.letterbox.common.StopWatch
import org.dizitart.letterbox.api.di.LetterBoxContext
import org.dizitart.letterbox.core.plugin.PluginManager
import tornadofx.*
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.reflect.KClass

open class StartupTask(private val config: ConfigProperties) : Task<Unit>() {
    private val logger = KotlinLogging.logger {}

    override fun call() {
        try {
            val stopWatch = StopWatch()
            updateMessage("Loading config . . .")

            // start letterbox context
            stopWatch.start("Load LetterBoxContext")
            loadLetterBoxContext()
            stopWatch.stop()

            // register shutdown hooks
            registerShutdownHooks()

            // start plugins from plugin folder
            updateMessage("Loading plugins . . .")
            stopWatch.start("Load Plugins")
            loadPlugins()
            stopWatch.stop()

            // load themes
            stopWatch.start("Load Themes")
            loadThemes()
            stopWatch.stop()

            updateProgress(1.0, 1.0)
            logger.info { "\n${stopWatch.prettyPrint()}" }
        } catch (t: Throwable) {
            logger.error(t) { "LetterBox has failed to start due to an error." }
        }
    }

    private fun loadLetterBoxContext() {
        updateProgress(0.05, 1.0)
        
        // add application config properties
        beanConfig.singleton { config }
        
        // initialize di container
        LetterBoxContext.register(beanConfig)
        
        // set up tornado-ui di container
        FX.dicontainer = object : DIContainer {
            override fun <T : Any> getInstance(type: KClass<T>): T = LetterBoxContext.getBean(type)!!
        }

        updateProgress(0.5, 1.0)
    }

    private fun registerShutdownHooks() {
        // register shutdown tasks
        beforeShutdown {
            val pluginManager = LetterBoxContext.getBean(PluginManager::class)!!

            // stop the plugin service
            pluginManager.stopPlugins()

            // save the configBean to file
            config.save()

            // close the letterbox context
            LetterBoxContext.destroy()

            // terminate all async executors
            terminateAsyncExecutors(1000)
        }
    }

    private fun loadPlugins() {
        val pluginManager = LetterBoxContext.getBean(PluginManager::class)!!

        pluginDirs().forEach { path ->
            pluginManager.addPath(path)
        }

        // load all plugins
        pluginManager.loadPlugins()

        // start plugin lifecycle
        pluginManager.startPlugins()

        updateProgress(0.90, 1.0)
    }

    private fun pluginDirs() : List<Path> {
        val userPluginPath = Paths.get(APP_DATA_DIR + File.separator + "plugins")
        val installPath = FileSystems.getDefault().getPath(".").toAbsolutePath().parent
        val systemPluginPath = installPath.resolve("/plugins")

        val devPath = FileSystems.getDefault().getPath("").toAbsolutePath()
        val devPluginPath = devPath.resolve("dist/plugins")

        logger.debug { "Checking plugins from $systemPluginPath and $userPluginPath" }
        return listOf(systemPluginPath, userPluginPath, devPluginPath)
    }
}