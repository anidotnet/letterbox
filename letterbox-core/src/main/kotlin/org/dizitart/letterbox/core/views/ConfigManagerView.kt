package org.dizitart.letterbox.core.views

import javafx.geometry.Orientation
import org.dizitart.letterbox.api.view.ModalView
import org.dizitart.letterbox.core.config.ConfigGroup
import org.dizitart.letterbox.core.config.ConfigGroupChanged
import org.dizitart.letterbox.core.controllers.ConfigController
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class ConfigManagerView : ModalView("Preference") {
    private val controller: ConfigController by inject()

    override val container = splitpane(Orientation.HORIZONTAL) {
        treeview<ConfigGroup> {
            root = controller.rootNode
            isShowRoot = false
            cellFormat { text = it.name }

            onUserSelect {
                fire(ConfigGroupChanged(it))
            }
        }

        vbox {
            controller.container = this
        }
    }
}