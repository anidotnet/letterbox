package org.dizitart.letterbox.core.config

import javafx.beans.property.SimpleBooleanProperty
import org.dizitart.letterbox.api.config.ConfigElement
import org.dizitart.letterbox.api.config.ConfigRepository

/**
 * @author Anindya Chatterjee.
 */
internal class LetterBoxConfigRepository : ConfigRepository {
    private val configElements = mutableMapOf<String, ConfigElement<*>>()

    val dirty = SimpleBooleanProperty()

    override fun containsId(id: String): Boolean = configElements.containsKey(id)

    override fun get(id: String): ConfigElement<*>? {
        return configElements[id]
    }

    override fun set(id: String, element: ConfigElement<*>) {
        configElements[id] = element
    }

    override fun markDirty() {
        dirty.value = true
    }
}