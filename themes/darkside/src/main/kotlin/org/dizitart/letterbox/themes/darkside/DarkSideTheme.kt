package org.dizitart.letterbox.themes.darkside

import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import org.dizitart.letterbox.common.DEFAULT_FONT_FAMILY
import org.dizitart.letterbox.common.DEFAULT_FONT_SIZE
import tornadofx.ConfigProperties
import tornadofx.px

/**
 *
 * @author Anindya Chatterjee
 */
@Default
open class DarkSideTheme(appConfig: ConfigProperties) : LetterBoxStylesheet(DarkSidePalette()) {
    init {
        root {
            fontFamily = appConfig.string(DEFAULT_FONT_FAMILY, "Noto Sans")
            fontSize = appConfig.int(DEFAULT_FONT_SIZE, -1)?.px!!
        }
    }
}