package org.dizitart.letterbox.themes.darkside

import javafx.scene.paint.Color
import org.dizitart.letterbox.api.styles.BaseLetterBoxTheme2
import org.dizitart.letterbox.api.styles.ColorPalette2
import tornadofx.Stylesheet
import tornadofx.c
import tornadofx.derive

/**
 *
 * @author Anindya Chatterjee
 */
open class DarkSideTheme2 : BaseLetterBoxTheme2(DarkSidePalette2(), "DarkSide")

class DarkSidePalette2 : Stylesheet(), ColorPalette2 {
    override val color1: Color = c("#34495E")
    override val color2: Color = c("#2980B9")
    override val color3: Color = c("#27AE60")
    override val color4: Color = c("#ff8d25")
    override val color5: Color = c("#909090")
    override val color6: Color = c("#000")
    override val color7: Color = c("#FFF")

    override val color8: Color = color6.derive(0.3)
    override val color9: Color = color4.derive(0.1)
    override val color10: Color = color5.derive(0.5)
    override val color11: Color = color1.derive(0.8)
    override val color12: Color = color6.derive(0.5)
    override val color13: Color = color2.derive(0.8)
    override val color14: Color = color7.derive(0.8)
    override val color15: Color = color2.derive(0.2)
    override val color16: Color = color3.derive(0.2)

    override val transparent: Color = Color.TRANSPARENT

    override val darkIconColor: Color = color1
    override val lightIconColor: Color = color7
}