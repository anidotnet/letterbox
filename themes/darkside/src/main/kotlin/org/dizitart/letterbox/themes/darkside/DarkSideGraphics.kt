package org.dizitart.letterbox.themes.darkside

import org.dizitart.lb.research.service.api.ServiceGraphicsCatalog
import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.styles.ButtonGraphics
import org.dizitart.letterbox.api.styles.LetterBoxGraphicsCatalog

/**
 * @author Anindya Chatterjee.
 */
@Default
open class DarkSideGraphics : LetterBoxGraphicsCatalog, ServiceGraphicsCatalog {

    override fun registerGraphics() {
        super<LetterBoxGraphicsCatalog>.registerGraphics()
        super<ServiceGraphicsCatalog>.registerGraphics()
    }

    override val closeButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/close.svg",
            hoverImage = "graphics/close-hover.svg",
            activeImage = "graphics/close.svg")

    override val maximizeButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/maximize.svg",
            hoverImage = "graphics/maximize-hover.svg",
            activeImage = "graphics/maximize.svg")

    override val minimizeButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/minimize.svg",
            hoverImage = "graphics/minimize-hover.svg",
            activeImage = "graphics/minimize.svg")

    override val menuButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/menu.svg",
            hoverImage = "graphics/menu-hover.svg",
            activeImage = "graphics/menu.svg")

    override val notificationButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/bell.svg",
            hoverImage = "graphics/bell-hover.svg",
            activeImage = "graphics/bell-active.svg")

    override val emailTabButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/email.svg",
            hoverImage = "graphics/email-hover.svg",
            activeImage = "graphics/email-active.svg")

    override val calendarTabButtonIcon: ButtonGraphics = ButtonGraphics(
            image = "graphics/calendar.svg",
            hoverImage = "graphics/calendar-hover.svg",
            activeImage = "graphics/calendar-active.svg")
}