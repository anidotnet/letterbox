package org.dizitart.letterbox.themes.darkside

import javafx.scene.paint.Color
import org.dizitart.letterbox.api.styles.LetterBoxPalette
import tornadofx.Stylesheet

/**
 *
 * @author Anindya Chatterjee
 */
open class DarkSidePalette : Stylesheet(), LetterBoxPalette {
    override val transparent: Color = Color.TRANSPARENT

    override val controlBackground: Color = transparent
    override val controlBorder: Color = transparent
    override val buttonBorder: Color = transparent
    override val buttonBackground: Color = transparent
}