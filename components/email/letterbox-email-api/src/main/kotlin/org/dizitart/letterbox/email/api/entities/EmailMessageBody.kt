package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailMessageBody() : Entity<Long?> {
    override var id: Long? = null            // set it to the hashcode of text
    var text: String? = null
    var html: Boolean = true
    var attachments: MutableList<Attachment> = mutableListOf()
    var inlineResources: MutableList<InlineResource> = mutableListOf()
    override var itemContext = ItemContext()

    constructor(text: String, html: Boolean) : this() {
        this.text = text
        this.html = html
    }

    val hasAttachment: Boolean get() = attachments.isNotEmpty()
    val hasInlineResource: Boolean get() = inlineResources.isNotEmpty()

    fun setText(text: String, html: Boolean) {
        this.text = text
        this.html = html
    }

    fun addAttachment(fileName: String, stream: ByteArray, contentType: String = "application/octet-stream") {
        val attachment = Attachment(fileName, stream)
        attachment.contentType = contentType
        attachments.add(attachment)
    }

    fun addInlineResource(contentId: String, stream: ByteArray, contentType: String) {
        val inlineResource = InlineResource(contentId, stream)
        inlineResource.contentType = contentType
        inlineResources.add(inlineResource)
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is EmailMessageBody) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    override fun createId(): Long? {
        return hash {
            appendString(text)
            appendInt(if (html) 1 else 0)
        }
    }
}