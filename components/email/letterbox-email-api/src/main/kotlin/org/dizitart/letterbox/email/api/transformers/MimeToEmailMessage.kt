package org.dizitart.letterbox.email.api.transformers

import com.sun.mail.imap.IMAPMessage
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.dizitart.letterbox.api.exceptions.LetterBoxError
import org.dizitart.letterbox.api.exceptions.MIME_TO_EMAIL_CONVERSION_ERROR
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.email.api.FetchMode
import org.dizitart.letterbox.email.api.WellKnownHeaders
import org.dizitart.letterbox.email.api.entities.EmailAddress
import org.dizitart.letterbox.email.api.entities.EmailMessage
import org.dizitart.letterbox.email.api.entities.EmailMessageBody
import org.dizitart.letterbox.email.api.sanitizeFilename
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

/**
 *
 * @author Anindya Chatterjee
 */
class MimeToEmailMessage() : Transformer<MimeMessage, EmailMessage> {
    private val DEFAULT = FetchMode.IdOnly
    private var fetchMode = DEFAULT
    private val HIGHEST_PRIORITY = "1"
    var session: Session? = null

    constructor(session: Session) : this() {
        this.session = session
    }

    override fun transform(source: MimeMessage?): EmailMessage? {
        if (source == null) return null

        try {
            val mailMessage = EmailMessage()

            if (fetchMode === FetchMode.Partial || fetchMode === FetchMode.Full) {
                val toRecipients = source.getRecipients(javax.mail.Message.RecipientType.TO)
                mailMessage.toRecipients = toEmailAddressSet(toRecipients)

                val ccRecipients = source.getRecipients(javax.mail.Message.RecipientType.CC)
                mailMessage.ccRecipients = toEmailAddressSet(ccRecipients)

                val bccRecipients = source.getRecipients(javax.mail.Message.RecipientType.BCC)
                mailMessage.bccRecipients = toEmailAddressSet(bccRecipients)

                mailMessage.subject = source.subject
                val fromAddresses = source.from
                if (fromAddresses != null && fromAddresses.isNotEmpty()) {
                    val from = fromAddresses[0] as InternetAddress
                    mailMessage.from = EmailAddress(from.personal, from.address)
                }

                if (source.receivedDate != null) {
                    mailMessage.timestamp = ZonedDateTime.ofInstant(source.receivedDate.toInstant(), ZoneId.systemDefault())
                } else {
                    mailMessage.timestamp = ZonedDateTime.ofInstant(source.sentDate.toInstant(), ZoneId.systemDefault())
                }
                mailMessage.unread = !source.isSet(Flags.Flag.SEEN)

                val headers = source.allHeaders
                if (headers != null) {
                    while (headers.hasMoreElements()) {
                        val header = headers.nextElement() as Header
                        if (StringUtils.isNotEmpty(header.name)) {
                            if (StringUtils.isNotEmpty(header.value)) {
                                mailMessage.setHeader(header.name, header.value)
                            }

                            if (header.name.equals(WellKnownHeaders.DISPOSITION_NOTIFICATION_TO, ignoreCase = true)) {
                                mailMessage.readReceiptRequired = true
                            }

                            if (header.name.equals(WellKnownHeaders.X_PRIORITY, ignoreCase = true) && HIGHEST_PRIORITY.equals(header.value, ignoreCase = true)) {
                                mailMessage.important = true
                            }
                        }
                    }
                }

                if (fetchMode === FetchMode.Full) {
                    val messageBody = EmailMessageBody()
                    // don't mark the message as read while calling getContent()
                    if (source is IMAPMessage) {
                        source.peek = true
                    }
                    parsePart(source, messageBody)
                    mailMessage.messageBody = messageBody
                }
            }

            return mailMessage
        } catch (e: Exception) {
            throw LetterBoxError(I18N.get(MIME_TO_EMAIL_CONVERSION_ERROR), e)
        }
    }

    fun setFetchMode(fetchMode: FetchMode?) {
        if (fetchMode != null) {
            this.fetchMode = fetchMode
        } else {
            this.fetchMode = DEFAULT
        }
    }

    private fun parsePart(part: Part, messageBody: EmailMessageBody) {
        if (part.isMimeType("text/*") && StringUtils.isEmpty(part.disposition)) {
            val isHtml = part.isMimeType("text/html")
            if ((isHtml && !messageBody.text.isNullOrEmpty())
                    || messageBody.text.isNullOrEmpty()) {
                /*
                * Sometimes an email contains both plain text and html part.
                * This logic is to ensure that we only take html part if both
                * are available. If only plain text part is available, we will
                * take that and same goes for only html part.
                * */
                messageBody.setText(part.content as String, isHtml)
            }
        } else if (part.isMimeType("multipart/*")) {
            val mp = part.content as Multipart
            val count = mp.count
            for (i in 0 until count) {
                parsePart(mp.getBodyPart(i), messageBody)
            }
        } else if (part.isMimeType("message/rfc822")) {
            val mimeMessage = part.content as MimeMessage
            val message = transform(mimeMessage)
            val emlStreamMapper = EmailMessageToStream(session!!)
            val messageStream = emlStreamMapper.transform(message!!)
            messageStream?.let {
                val name = sanitizeFilename(message.subject) + ".eml"
                messageBody.addAttachment(name, messageStream, "message/rfc822")
            }
        } else {
            if (Part.ATTACHMENT.equals(part.disposition, ignoreCase = true)) {
                val contentType = part.contentType
                val stream = part.inputStream
                val fileName = part.fileName
                val content = IOUtils.toByteArray(stream)
                stream.close()

                messageBody.addAttachment(fileName, content, contentType)
            } else if (Part.INLINE.equals(part.disposition, ignoreCase = true)) {
                val headerValues = part.getHeader(WellKnownHeaders.CONTENT_ID)
                if (headerValues != null && headerValues.isNotEmpty()) {
                    val encodedContentId = headerValues[0]
                    if (StringUtils.isNotEmpty(encodedContentId)) {
                        val contentId = StringUtils.strip(encodedContentId, "<>")
                        val contentType = part.contentType
                        val stream = part.inputStream
                        val content = IOUtils.toByteArray(stream)
                        stream.close()

                        messageBody.addInlineResource(contentId, content, contentType)
                    }
                }
            }
        }
    }

    private fun toEmailAddressSet(addresses: Array<Address>?): MutableSet<EmailAddress> {
        if (addresses != null && addresses.isNotEmpty()) {
            return addresses
                    .filterIsInstance<InternetAddress>()
                    .map { EmailAddress(it.personal, it.address) }
                    .toMutableSet()
        }
        return HashSet()
    }
}