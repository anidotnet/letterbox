package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.ItemContext
import java.time.ZonedDateTime

/**
 *
 * @author Anindya Chatterjee
 */
class EmailMessage : MailBoxItem {
    override var id: Long? = null           // set it to the hashcode of uniqueItemId, folderId
    override var uniqueItemId: String? = null
    override var folderId: Long? = null
    override var timestamp: ZonedDateTime? = null
    override var accountId: Long? = null
    override var itemContext = ItemContext()

    var subject: String? = null
    var toRecipients: MutableSet<EmailAddress> = mutableSetOf()
    var ccRecipients: MutableSet<EmailAddress> = mutableSetOf()
    var bccRecipients: MutableSet<EmailAddress> = mutableSetOf()
    var from: EmailAddress? = null
    var headers: MutableMap<String, String> = mutableMapOf()
    var messageBody: EmailMessageBody? = null
    var favorite: Boolean = false
    var readReceiptRequired: Boolean = false
    var important: Boolean = false
    var unread: Boolean = true

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is EmailMessage) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    val hasAttachment: Boolean get() = messageBody != null && messageBody?.hasAttachment!!
    val hasInlineResource: Boolean get() = messageBody != null && messageBody?.hasInlineResource!!

    fun setHeader(name: String, value: String) {
        headers.put(name, value)
    }

    fun getHeader(name: String): String? {
        return if (headers.containsKey(name)) {
            headers[name]
        } else null
    }
}