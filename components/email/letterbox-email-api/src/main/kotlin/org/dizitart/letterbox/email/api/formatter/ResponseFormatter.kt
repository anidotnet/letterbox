package org.dizitart.letterbox.email.api.formatter

import org.dizitart.letterbox.email.api.entities.EmailAddress
import java.time.ZonedDateTime

/**
 *
 * @author Anindya Chatterjee
 */
interface ResponseFormatter {
    fun answerTemplate(): ResponseTemplate
    fun replyPrefix(): String
    fun forwardPrefix(): String
    fun answer(body: String, from: EmailAddress,
               to: Set<EmailAddress>, cc: Set<EmailAddress>,
               subject: String, time: ZonedDateTime): String
}