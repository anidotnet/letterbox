package org.dizitart.letterbox.email.api.pool

import org.apache.commons.pool2.impl.GenericKeyedObjectPool
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig
import org.dizitart.letterbox.email.api.MailBoxConnection

/**
 *
 * @author Anindya Chatterjee
 */
class MailBoxConnectionPool<Connection : MailBoxConnection>(
        connectionFactory: MailBoxConnectionFactory<Connection>,
        config: GenericKeyedObjectPoolConfig = GenericKeyedObjectPoolConfig())
    : GenericKeyedObjectPool<Long, Connection>(connectionFactory, config)