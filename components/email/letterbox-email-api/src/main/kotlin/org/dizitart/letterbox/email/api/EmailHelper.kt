package org.dizitart.letterbox.email.api

import org.apache.commons.lang3.StringUtils
import org.dizitart.letterbox.api.exceptions.EMAIL_ADDRESS_ENCODING_ERROR
import org.dizitart.letterbox.api.exceptions.EMAIL_ADDRESS_LIST_PARSING_ERROR
import org.dizitart.letterbox.api.exceptions.EmailAddressException
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.email.api.entities.*
import java.nio.charset.StandardCharsets
import javax.mail.internet.InternetAddress

/**
 *
 * @author Anindya Chatterjee
 */
const val EMAIL_ADDRESS_SEPARATOR = ","

fun emailAccount(op: (EmailAccountInfo.() -> Unit)? = null) : EmailAccountInfo {
    val account = EmailAccountInfo()
    op?.invoke(account)
    return account
}

fun emailMessage(op: (EmailMessage.() -> Unit)? = null): EmailMessage {
    val emailMessage = EmailMessage()
    op?.invoke(emailMessage)
    return emailMessage
}

fun attachment(op: (Attachment.() -> Unit)? = null): Attachment {
    val attachment = Attachment()
    op?.invoke(attachment)
    return attachment
}

fun inlineResource(op: (InlineResource.() -> Unit)? = null): InlineResource {
    val inlineResource = InlineResource()
    op?.invoke(inlineResource)
    return inlineResource
}

fun emailFolder(op: (EmailFolder.() -> Unit)? = null): EmailFolder {
    val emailFolder = EmailFolder()
    op?.invoke(emailFolder)
    return emailFolder
}

fun messageBody(op: (EmailMessageBody.() -> Unit)? =null): EmailMessageBody {
    val emailMessageBody = EmailMessageBody()
    op?.invoke(emailMessageBody)
    return emailMessageBody
}

fun encode(vararg addresses: EmailAddress): String? {
    if (addresses.isNotEmpty()) {
        val stringBuilder = StringBuilder()
        var iteration = 1
        for (address in addresses) {
            val rfc822Address = encodeSingle(address)
            if (StringUtils.isNotEmpty(rfc822Address)) {
                stringBuilder.append(rfc822Address)
            }
            if (iteration != addresses.size) {
                stringBuilder.append(EMAIL_ADDRESS_SEPARATOR).append(" ")
            }
            iteration++
        }
        return stringBuilder.toString()
    }
    return null
}

fun decode(addressList: String): Collection<EmailAddress>? {
    val trimmedList = StringUtils.trimToEmpty(addressList)
    if (StringUtils.isNotEmpty(trimmedList)) {
        try {
            val internetAddresses = InternetAddress.parse(trimmedList, true)
            if (internetAddresses.isNotEmpty()) {
                val addressSet = internetAddresses
                        .map { EmailAddress(it) }
                        .toSet()
                if (!addressSet.isEmpty()) {
                    return addressSet
                }
            }
        } catch (e: Exception) {
            throw EmailAddressException(I18N.get(EMAIL_ADDRESS_LIST_PARSING_ERROR, addressList), e)
        }

    }
    return null
}

fun sanitizeFilename(name: String?): String? {
    return name?.replace("[:\\\\/*?|<> \"]".toRegex(), "_")
}

private fun encodeSingle(emailAddress: EmailAddress?): String? {
    if (emailAddress != null) {
        try {
            val internetAddress = InternetAddress(emailAddress.address,
                    emailAddress.name, StandardCharsets.UTF_8.name())
            return internetAddress.toUnicodeString()
        } catch (e: Exception) {
            throw EmailAddressException(I18N.get(EMAIL_ADDRESS_ENCODING_ERROR), e)
        }

    }
    return null
}