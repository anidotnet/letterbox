package org.dizitart.letterbox.email.api

import org.dizitart.letterbox.email.api.entities.EmailFolder
import org.dizitart.letterbox.email.api.entities.EmailMessage
import org.dizitart.letterbox.email.api.entities.MailBoxItem
import org.dizitart.letterbox.email.api.formatter.ResponseFormatter
import org.dizitart.letterbox.email.api.search.SearchFilter
import org.dizitart.letterbox.email.api.search.SearchResult


/**
 *
 * @author Anindya Chatterjee
 */
interface MailBoxConnection {
    var accountId: Long
    
    fun getFolder(folderId: String): EmailFolder?
    fun getAllFolders(fetchMode: FetchMode): List<EmailFolder>
    fun getAllChildFolders(folderId: String, fetchMode: FetchMode): List<EmailFolder>
    fun getStandardEmailFolder(standardFolder: StandardFolder): EmailFolder
    fun getUnreadCount(folderId: String): Int?
    fun getTotalMessageCount(folderId: String): Int?
    fun createFolder(folderName: String, parentId: String): EmailFolder
    fun renameFolder(folderId: String, folderName: String)
    fun deleteFolder(folderId: String, deleteMode: DeleteMode)
    fun clearFolder(folderId: String, deleteMode: DeleteMode, recursive: Boolean)

    fun sendMessage(message: EmailMessage, beforeSend: (EmailMessage.() -> EmailMessage)? = null)
    fun fetchMessages(folderId: String, fetchOption: FetchOption): SearchResult<out MailBoxItem>
    fun search(folderId: String, searchFilter: SearchFilter, fetchOption: FetchOption): SearchResult<out MailBoxItem>
    fun getItemById(itemId: String, fetchMode: FetchMode): MailBoxItem
    fun changeItemStatus(itemId: String, readStatus: ReadStatus, sendReadReceipt: Boolean)
    fun moveMessage(itemId: String, targetFolderId: String): MailBoxItem
    fun copyMessage(itemId: String, targetFolderId: String): MailBoxItem
    fun deleteMessage(itemId: String, deleteMode: DeleteMode)
    fun saveDraft(message: EmailMessage)
    fun createReplyMessage(message: EmailMessage, replyAll: Boolean, formatter: ResponseFormatter): EmailMessage
    fun createForwardMessage(message: EmailMessage, formatter: ResponseFormatter): EmailMessage

    fun close()
    fun isValid(): Boolean
    fun reset()
    //https://dzone.com/articles/creating-object-pool-java
}