package org.dizitart.letterbox.email.api.search

import org.dizitart.letterbox.email.api.entities.MailBoxItem

/**
 *
 * @author Anindya Chatterjee
 */
class SearchResult<T : MailBoxItem> : Iterable<T> {
    var totalCount: Int = 0
    var nextPageOffset: Int = 0
    var hasMore: Boolean = false
    var items: MutableList<T> = mutableListOf()

    override fun iterator(): Iterator<T> {
        return items.iterator()
    }
}