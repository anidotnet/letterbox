package org.dizitart.letterbox.email.api

import org.dizitart.letterbox.email.api.entities.EmailAccount
import org.dizitart.letterbox.email.api.entities.EmailMessage
import org.dizitart.letterbox.email.api.entities.MailBoxItem
import org.dizitart.letterbox.email.api.formatter.ResponseFormatter
import org.dizitart.letterbox.email.api.search.SearchFilter
import org.dizitart.letterbox.email.api.search.SearchResult


/**
 *
 * @author Anindya Chatterjee
 */
interface EmailMessageOperations {
    fun sendMessage(message: EmailMessage, beforeSend: (EmailMessage.() -> EmailMessage)? = null)
    fun fetchMessages(folderId: EmailAccount, fetchOption: FetchOption): SearchResult<out MailBoxItem>
    fun search(folderId: EmailAccount, searchFilter: SearchFilter, fetchOption: FetchOption): SearchResult<out MailBoxItem>
    fun getItemById(itemId: Long, fetchMode: FetchMode): MailBoxItem
    fun changeItemStatus(itemId: Long, readStatus: ReadStatus, sendReadReceipt: Boolean)
    fun moveMessage(itemId: Long, targetFolderId: EmailAccount): MailBoxItem
    fun copyMessage(itemId: Long, targetFolderId: EmailAccount): MailBoxItem
    fun deleteMessage(itemId: Long, deleteMode: DeleteMode)
    fun saveDraft(message: EmailMessage)
    fun createReplyMessage(message: EmailMessage, replyAll: Boolean, formatter: ResponseFormatter): EmailMessage
    fun createForwardMessage(message: EmailMessage, formatter: ResponseFormatter): EmailMessage
}