package org.dizitart.letterbox.email.api.search

/**
 *
 * @author Anindya Chatterjee
 */
enum class SearchField {
    Read,
    Subject,
    Body,
    From,
    Recipient,
    ReceivedDate
}