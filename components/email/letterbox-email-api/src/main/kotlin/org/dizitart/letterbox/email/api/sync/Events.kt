package org.dizitart.letterbox.email.api.sync

import org.dizitart.letterbox.email.api.entities.EmailAddress

/**
 *
 * @author Anindya Chatterjee
 */
interface RemoteEmailFolderEvent {
    var folderId: String
    var eventType: EventType
    var emailAddress: EmailAddress
}

interface RemoteMailBoxItemEvent {
    var itemId: String
    var eventType: EventType
    var read: Boolean
    var emailAddress: EmailAddress
}