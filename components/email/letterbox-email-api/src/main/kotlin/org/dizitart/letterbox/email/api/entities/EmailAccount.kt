package org.dizitart.letterbox.email.api.entities

/**
 * @author Anindya Chatterjee.
 */
class EmailAccount(accountInfo: EmailAccountInfo) {
    val id: Long? = accountInfo.id
    val emailAddress: String? = accountInfo.emailAddress
    val connectionProviderClass: String? = accountInfo.connectionProviderClass
}