package org.dizitart.letterbox.email.api.entities

import org.dizitart.letterbox.api.Entity
import org.dizitart.letterbox.api.appendLong
import org.dizitart.letterbox.api.appendString
import org.dizitart.letterbox.api.hash
import java.time.ZonedDateTime

/**
 *
 * @author Anindya Chatterjee
 */
interface MailBoxItem : Entity<Long?> {
    override var id: Long?
    var uniqueItemId: String?
    var folderId: Long?
    var timestamp: ZonedDateTime?
    var accountId: Long?

    override fun createId(): Long? {
        return hash {
            appendLong(timestamp?.toInstant()?.toEpochMilli())
            appendString(uniqueItemId)
            appendLong(folderId)
            appendLong(accountId)
        }
    }
}