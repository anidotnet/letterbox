package org.dizitart.letterbox.email.api.entities

/**
 *
 * @author Anindya Chatterjee
 */
enum class RecipientType(val text: String) {
    TO("To"),
    CC("Cc"),
    BCC("Bcc");
}