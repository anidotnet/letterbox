package org.dizitart.letterbox.email.api.sync

/**
 *
 * @author Anindya Chatterjee
 */
enum class EventType {
    // An item or folder was created.
    Create,

    // An item or folder was modified.
    Update,

    // An item or folder was deleted.
    Delete,

    // An item's IsRead flag was changed.
    ReadFlagChange
}