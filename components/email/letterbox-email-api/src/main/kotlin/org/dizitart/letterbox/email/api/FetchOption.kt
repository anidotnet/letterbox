package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
data class FetchOption(
        var pageSize: Int,
        var offset: Int,
        var fetchMode: FetchMode
)