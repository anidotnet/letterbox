package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import java.io.Serializable
import javax.mail.internet.InternetAddress

class EmailAddress(): Serializable {
    var name: String? = null
    var address: String? = null

    constructor(name: String? = null, address: String?) : this() {
        this.name = name
        this.address = address
    }

    constructor(internetAddress: InternetAddress)
            : this(internetAddress.address,
            internetAddress.personal)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is EmailAddress) {
            return EqualsBuilder()
                    .append(address, other.address)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(address)
                .build()!!
    }

    override fun toString(): String {
        return "\"$name\" <$address>"
    }
}