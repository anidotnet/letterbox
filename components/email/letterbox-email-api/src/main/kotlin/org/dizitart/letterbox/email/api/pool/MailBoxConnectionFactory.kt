package org.dizitart.letterbox.email.api.pool

import org.apache.commons.pool2.BaseKeyedPooledObjectFactory
import org.apache.commons.pool2.PooledObject
import org.apache.commons.pool2.impl.DefaultPooledObject
import org.dizitart.letterbox.email.api.ConnectionProvider
import org.dizitart.letterbox.email.api.MailBoxConnection

/**
 *
 * @author Anindya Chatterjee
 */
class MailBoxConnectionFactory<Connection : MailBoxConnection>(
        private val connectionProvider: ConnectionProvider<Connection>)
    : BaseKeyedPooledObjectFactory<Long, Connection>() {

    override fun wrap(value: Connection): PooledObject<Connection> {
        return DefaultPooledObject<Connection>(value)
    }

    @Suppress("UNCHECKED_CAST")
    override fun create(key: Long): Connection {
        val connection = connectionProvider.createConnection(key)
        connection.accountId = key
        return connection
    }

    override fun validateObject(key: Long, p: PooledObject<Connection>): Boolean {
        val connection = p.`object`
        return connection.isValid()
    }

    override fun passivateObject(key: Long?, p: PooledObject<Connection>) {
        val connection = p.`object`
        return connection.reset()
    }

    override fun destroyObject(key: Long, p: PooledObject<Connection>) {
        val connection = p.`object`
        return connection.close()
    }
}