package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
enum class StandardFolder {
    Inbox,
    Drafts,
    Trash,
    Sent,
    Junk,
    None
}