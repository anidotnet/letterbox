package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.*

/**
 *
 * @author Anindya Chatterjee
 */
class InlineResource() : Entity<Long?> {
    override var id: Long? = null        // set it to the hashcode resource
    var contentId: String? = null
    var resource: ByteArray? = null
    var contentType: String = "image/*"
    override var itemContext = ItemContext()

    constructor(contentId: String, resource: ByteArray) : this() {
        this.contentId = contentId
        this.resource = resource
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is InlineResource) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!

        }
        return false
    }

    override fun createId(): Long? {
        return hash {
            appendString(contentId)
            appendByteArray(resource)
            appendString(contentType)
        }
    }
}