package org.dizitart.letterbox.email.api.transformers

import mu.KotlinLogging
import org.dizitart.letterbox.api.exceptions.EMAIL_TO_EML_CONVERSION_ERROR
import org.dizitart.letterbox.api.exceptions.LetterBoxError
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.email.api.entities.EmailMessage
import java.io.ByteArrayOutputStream
import javax.mail.Session

/**
 *
 * @author Anindya Chatterjee
 */
class EmailMessageToStream() : Transformer<EmailMessage, ByteArray> {
    private val logger = KotlinLogging.logger { }
    private var mimeMapper: EmailMessageToMime = EmailMessageToMime()

    constructor(session: Session) : this() {
        mimeMapper = EmailMessageToMime(session)
    }

    override fun transform(source: EmailMessage?): ByteArray? {
        try {
            if (source == null) return null

            val mimeMessage = mimeMapper.transform(source)
            val os = ByteArrayOutputStream()
            try {
                mimeMessage?.writeTo(os)
                return os.toByteArray()
            } finally {
                try {
                    os.close()
                } catch (e: Exception) {
                    logger.error(e) { "Error while closing eml file" }
                }
            }
        } catch (e: Exception) {
            throw LetterBoxError(I18N.get(EMAIL_TO_EML_CONVERSION_ERROR), e)
        }
    }
}