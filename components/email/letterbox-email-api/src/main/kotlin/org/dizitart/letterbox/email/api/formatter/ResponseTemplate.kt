package org.dizitart.letterbox.email.api.formatter

/**
 *
 * @author Anindya Chatterjee
 */
interface ResponseTemplate {
    fun bodyTemplate(): String
}