package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.*
import org.dizitart.letterbox.email.api.StandardFolder

class EmailFolder : ChangeAware, Entity<Long?> {
    override var id: Long? = null        // set it to the hashcode of uniqueFolderId, name, parentId
    var uniqueFolderId: String? = null
    var name: String? = null
    var parentId: Long = -1L
    var accountId: Long? = null
    var folderType: StandardFolder = StandardFolder.None
    override var itemContext = ItemContext()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is EmailFolder) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    override fun createId(): Long? {
        return hash {
            appendString(name)
            appendString(uniqueFolderId)
            appendLong(parentId)
            appendInt(folderType.ordinal)
        }
    }

    @Transient
    override var onChange: (() -> Unit)? = null
}
