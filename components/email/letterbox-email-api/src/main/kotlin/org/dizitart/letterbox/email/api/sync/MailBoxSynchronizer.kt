package org.dizitart.letterbox.email.api.sync

/**
 *
 * @author Anindya Chatterjee
 */
interface MailBoxSynchronizer<T> {
    fun syncFolder(folderId: String, oldState: SyncState<T>): SyncState<T>
    fun syncMessages(folderId: String, oldState: SyncState<T>): SyncState<T>
}