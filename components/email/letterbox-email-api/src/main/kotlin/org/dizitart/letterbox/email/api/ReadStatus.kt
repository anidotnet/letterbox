package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
enum class ReadStatus {
    Read,
    Unread
}