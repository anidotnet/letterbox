package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
enum class FetchMode {
    IdOnly,
    Partial,
    Full
}