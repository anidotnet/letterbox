package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
enum class DeleteMode {
    Hard,
    Soft
}