package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
interface ConnectionProvider<out Connection: MailBoxConnection> {
    fun createConnection(accountId: Long): Connection
}