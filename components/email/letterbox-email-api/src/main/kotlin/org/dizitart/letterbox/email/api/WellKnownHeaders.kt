package org.dizitart.letterbox.email.api

/**
 *
 * @author Anindya Chatterjee
 */
object WellKnownHeaders {
    val DISPOSITION_NOTIFICATION_TO = "Disposition-Notification-To"
    val REFERENCES = "References"
    val MESSAGE_ID = "Message-Id"
    val IN_REPLY_TO = "In-Reply-To"
    val CONTENT_ID = "Content-ID"
    val SUBJECT = "Subject"
    val RECEIVED = "Received"
    val DATE = "Date"
    val FROM = "From"
    val SENDER = "Sender"
    val REPLY_TO = "Reply-To"
    val TO = "To"
    val CC = "Cc"
    val BCC = "Bcc"
    val COMMENTS = "Comments"
    val KEYWORDS = "Keywords"
    val ERRORS_TO = "Errors-To"
    val MIME_VERSION = "MIME-Version"
    val CONTENT_TYPE = "Content-Type"
    val CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding"
    val CONTENT_MD5 = "Content-MD5"
    val COLON = ":"
    val CONTENT_LENGTH = "Content-Length"
    val STATUS = "Status"

    val RETURN_PATH = "Return-Path"
    val RESENT_DATE = "Resent-Date"
    val RESENT_FROM = "Resent-From"
    val RESENT_SENDER = "Resent-Sender"
    val RESENT_TO = "Resent-To"
    val RESENT_CC = "Resent-Cc"
    val RESENT_MESSAGE_ID = "Resent-Message-Id"

    val X_PRIORITY = "X-Priority"
    val X_MAILER = "X-Mailer"
    val X_SPAM_STATUS = "X-Spam-Status"
    val X_SPAM_LEVEL = "X-Spam-Level"
    val X_ORIGINATING_IP = "X-Originating-IP"
}