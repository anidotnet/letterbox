package org.dizitart.letterbox.email.api.search

import java.time.ZonedDateTime

/**
 *
 * @author Anindya Chatterjee
 */
interface SearchFilter
data class EqualsFilter<T>(var searchField: SearchField, var data: T) : SearchFilter
data class AndFilter(var leftFilter: SearchFilter, var rightFilter: SearchFilter) : SearchFilter
data class OrFilter(var leftFilter: SearchFilter, var rightFilter: SearchFilter) : SearchFilter
data class RangeFilter(var startDate: ZonedDateTime, var endDate: ZonedDateTime) : SearchFilter
data class NotFilter(var filter: SearchFilter) : SearchFilter

fun <T> equals(searchField: SearchField, data: T) : SearchFilter = EqualsFilter(searchField, data)
fun and(leftFilter: SearchFilter, rightFilter: SearchFilter) : SearchFilter = AndFilter(leftFilter, rightFilter)
fun or(leftFilter: SearchFilter, rightFilter: SearchFilter) : SearchFilter = OrFilter(leftFilter, rightFilter)
fun between(startDate: ZonedDateTime, endDate: ZonedDateTime) : SearchFilter = RangeFilter(startDate, endDate)
fun not(filter: SearchFilter) : SearchFilter = NotFilter(filter)
