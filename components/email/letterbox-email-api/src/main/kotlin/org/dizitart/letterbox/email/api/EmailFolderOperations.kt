package org.dizitart.letterbox.email.api

import org.dizitart.letterbox.email.api.entities.EmailAccount
import org.dizitart.letterbox.email.api.entities.EmailFolder


/**
 *
 * @author Anindya Chatterjee
 */
interface EmailFolderOperations {
    fun getAllFolders(fetchMode: FetchMode): List<EmailFolder>
    fun getAllChildFolders(folderId: EmailAccount, fetchMode: FetchMode): List<EmailFolder>
    fun createFolder(folderName: String, parentId: EmailAccount): EmailFolder?
    fun getStandardFolder(standardFolder: StandardFolder): EmailFolder?
    fun getFolder(folderId: EmailAccount): EmailFolder?
    fun renameFolder(folderName: String, folderId: EmailAccount)
    fun deleteFolder(folderId: EmailAccount, deleteMode: DeleteMode)
    fun clearFolder(folderId: EmailAccount, deleteMode: DeleteMode, recursive: Boolean)
    fun unreadCount(folderId: EmailAccount): Int?
    fun totalMessageCount(folderId: EmailAccount): Int?
}