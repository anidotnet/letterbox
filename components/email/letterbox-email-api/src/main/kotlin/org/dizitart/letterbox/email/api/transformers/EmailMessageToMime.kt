package org.dizitart.letterbox.email.api.transformers

import mu.KotlinLogging
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang3.StringUtils
import org.dizitart.letterbox.email.api.WellKnownHeaders
import org.dizitart.letterbox.email.api.encode
import org.dizitart.letterbox.api.exceptions.EMAIL_TO_MIME_CONVERSION_ERROR
import org.dizitart.letterbox.api.exceptions.LetterBoxError
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.email.api.entities.EmailAddress
import org.dizitart.letterbox.email.api.entities.EmailMessage
import java.io.UnsupportedEncodingException
import java.nio.charset.StandardCharsets
import javax.mail.Address
import javax.mail.Flags
import javax.mail.Session
import javax.mail.internet.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailMessageToMime() : Transformer<EmailMessage, MimeMessage> {
    private val logger = KotlinLogging.logger { }
    private var session: Session? = null
    private val HIGHEST_PRIORITY = "1"

    constructor(session: Session) : this() {
        this.session = session
    }

    override fun transform(source: EmailMessage?): MimeMessage? {
        if (source == null) return null
        try {
            val props = System.getProperties()
            props.put("mail.mime.address.strict", "false")
            if (session == null) {
                session = Session.getDefaultInstance(props)
            }

            val mimeMessage = MimeMessage(session)

            mimeMessage.setSubject(source.subject, StandardCharsets.UTF_8.name())
            if (source.from != null) {
                mimeMessage.setFrom(InternetAddress(source.from?.address, null, StandardCharsets.UTF_8.name()))
            }
            mimeMessage.setFlag(Flags.Flag.SEEN, !source.unread)

            setAddresses(source.toRecipients, javax.mail.Message.RecipientType.TO, mimeMessage)
            setAddresses(source.ccRecipients, javax.mail.Message.RecipientType.CC, mimeMessage)
            setAddresses(source.bccRecipients, javax.mail.Message.RecipientType.BCC, mimeMessage)

            // set headers
            if (source.id == null) {
                // new message
                for ((key, value) in source.headers) {
                    mimeMessage.setHeader(key, value)
                }
            }

            if (source.readReceiptRequired && source.from != null) {
                mimeMessage.setHeader(WellKnownHeaders.DISPOSITION_NOTIFICATION_TO, encode(source.from!!))
            }

            if (source.important) {
                mimeMessage.setHeader(WellKnownHeaders.X_PRIORITY, HIGHEST_PRIORITY)
            }

            if (source.messageBody != null) {
                val multipart = MimeMultipart("related")

                // Body text
                var messageBodyPart = MimeBodyPart()

                var text = source.messageBody?.text
                if (StringUtils.isEmpty(text)) {
                    // to avoid NPE at com.sun.mail.handlers.text_plain.writeTo(text_plain.java:129)
                    text = ""
                }
                if (source.messageBody?.html!!) {
                    messageBodyPart.setContent(text,
                            "text/html; charset=" + StandardCharsets.UTF_8.name())
                } else {
                    messageBodyPart.setText(text, StandardCharsets.UTF_8.name())
                }
                multipart.addBodyPart(messageBodyPart)

                // attachment
                if (source.hasAttachment) {
                    for (attachment in source.messageBody?.attachments!!) {
                        messageBodyPart = PreencodedMimeBodyPart("base64")
                        val contentType = attachment.contentType
                        val base64Content = String(Base64.encodeBase64(attachment.fileStream))

                        messageBodyPart.setContent(base64Content, contentType)
                        messageBodyPart.setFileName(MimeUtility.encodeText(attachment.fileName,
                                StandardCharsets.UTF_8.name(), null))
                        messageBodyPart.setDisposition(MimeBodyPart.ATTACHMENT)
                        multipart.addBodyPart(messageBodyPart)
                    }
                }

                // inline resource
                if (source.hasInlineResource) {
                    for (inlineResource in source.messageBody?.inlineResources!!) {
                        messageBodyPart = PreencodedMimeBodyPart("base64")
                        val base64Content = String(Base64.encodeBase64(inlineResource.resource))
                        val contentType = inlineResource.contentType
                        messageBodyPart.setContent(base64Content, contentType)
                        messageBodyPart.setContentID("<" + inlineResource.contentId + ">")
                        messageBodyPart.setFileName(MimeUtility.encodeText(inlineResource.contentId,
                                StandardCharsets.UTF_8.name(), null))
                        messageBodyPart.setDisposition(MimeBodyPart.INLINE)
                        multipart.addBodyPart(messageBodyPart)
                    }
                }

                mimeMessage.setContent(multipart)
            }

            return mimeMessage
        } catch (e: Exception) {
            throw LetterBoxError(I18N.get(EMAIL_TO_MIME_CONVERSION_ERROR), e)
        }
    }

    private fun setAddresses(addressSet: Set<EmailAddress>?,
                             recipientType: javax.mail.Message.RecipientType,
                             mimeMessage: javax.mail.Message) {
        if (addressSet != null) {
            val addresses = arrayOfNulls<Address>(addressSet.size)
            var count = 0
            for (address in addressSet) {
                try {
                    addresses[count] = InternetAddress(address.address, address.name, StandardCharsets.UTF_8.name())
                } catch (e: UnsupportedEncodingException) {
                    logger.error(e) { "Invalid email address encoding - " + address }
                }

                count++
            }
            if (count > 0) {
                mimeMessage.setRecipients(recipientType, addresses)
            }
        }
    }

}