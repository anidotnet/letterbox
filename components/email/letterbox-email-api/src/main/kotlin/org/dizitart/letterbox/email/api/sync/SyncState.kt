package org.dizitart.letterbox.email.api.sync

/**
 *
 * @author Anindya Chatterjee
 */
interface SyncState<out T> {
    val syncState: T
}