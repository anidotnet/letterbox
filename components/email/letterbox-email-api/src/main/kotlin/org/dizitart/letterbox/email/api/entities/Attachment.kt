package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.*

/**
 *
 * @author Anindya Chatterjee
 */
class Attachment() : Entity<Long?> {
    override var id: Long? = null            // set it to the hashcode of the content
    var fileName: String? = null
    var fileStream: ByteArray? = null
    var contentType: String = "application/octet-stream"
    override var itemContext = ItemContext()

    constructor(fileName: String, fileStream: ByteArray) : this() {
        this.fileName = fileName
        this.fileStream = fileStream
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other === this) return true
        if (other is Attachment) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun createId(): Long? {
        return hash {
            appendString(fileName)
            appendByteArray(fileStream ?: byteArrayOf())
            appendString(contentType)
        }
    }
}
