package org.dizitart.letterbox.email.api.transformers

import org.dizitart.letterbox.api.exceptions.EML_TO_EMAIL_CONVERSION_ERROR
import org.dizitart.letterbox.api.exceptions.LetterBoxError
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.email.api.entities.EmailMessage
import java.io.ByteArrayInputStream
import javax.mail.Session
import javax.mail.internet.MimeMessage

/**
 *
 * @author Anindya Chatterjee
 */
class StreamToEmailMessage() : Transformer<ByteArray, EmailMessage> {
    private var mimeMapper: MimeToEmailMessage = MimeToEmailMessage()

    constructor(session: Session) : this() {
        mimeMapper = MimeToEmailMessage(session)
    }

    override fun transform(source: ByteArray?): EmailMessage? {
        try {
            val props = System.getProperties()
            props.put("mail.mime.address.strict", "false")
            val session = Session.getDefaultInstance(props)
            val mimeMessage = MimeMessage(session, ByteArrayInputStream(source))
            return mimeMapper.transform(mimeMessage)
        } catch (e: Exception) {
            throw LetterBoxError(I18N.get(EML_TO_EMAIL_CONVERSION_ERROR), e)
        }
    }

}