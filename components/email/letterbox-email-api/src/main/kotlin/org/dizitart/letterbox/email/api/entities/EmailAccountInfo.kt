package org.dizitart.letterbox.email.api.entities

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.*

/**
 *
 * @author Anindya Chatterjee
 */
open class EmailAccountInfo : ChangeAware, Entity<Long?> {
    override var id: Long? = null        // set it to the hashcode of emailAddress
    var emailAddress: String? = null
    var name: String? = null
    var password: String? = null
    var storePath: String? = null
    var connectionProviderClass: String? = null
    override var itemContext = ItemContext()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is EmailAccountInfo) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }
    override fun createId(): Long? {
        return hash {
            appendString(emailAddress)
            appendString(name)
        }
    }

    @Transient
    override var onChange: (() -> Unit)? = null
}