package org.dizitart.letterbox.email.controllers

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.email.service.EmailAddressInfoService
import org.springframework.stereotype.Component
import tornadofx.*
import java.io.ByteArrayInputStream
import java.util.*

/**
 *
 * @author Anindya Chatterjee
 */
@Component
class AvatarController : Controller() {
    private val avatarService: EmailAddressInfoService by di()

    private val random: Random = Random(System.currentTimeMillis())
    private val palette = arrayOf(
            Color.web("#C91F37"),
            Color.web("#DC3023"),
            Color.web("#9D2933"),
            Color.web("#CF3A24"),
            Color.web("#8F1D21"),
            Color.web("#C93756"),
            Color.web("#F62459"),
            Color.web("#DB5A6B"),
            Color.web("#5D3F6A"),
            Color.web("#875F9A"),
            Color.web("#763568"),
            Color.web("#5B3256"),
            Color.web("#8E44AD"),
            Color.web("#8D608C"),
            Color.web("#4D8FAC"),
            Color.web("#317589"),
            Color.web("#003171"),
            Color.web("#1F4788"),
            Color.web("#264348"),
            Color.web("#044F67"),
            Color.web("#006442"),
            Color.web("#407A52"),
            Color.web("#A17917"),
            Color.web("#CA6924"),
            Color.web("#757D75"),
            Color.web("#6C7A89"),
            Color.web("#F9690E"),
            Color.web("#03A678"),
            Color.web("#48929B")
    )

    fun createAvatarImage(emailAddress: EmailAddress, size: Double) : Image {
        try {
            val image = avatarService.getAvatarImage(emailAddress)
            return if (image != null) {
                image
            } else {
                val initials = if (emailAddress.name != null) getInitial(emailAddress.name!!) else "?"
                val color = palette[random.nextInt(palette.size - 1)]
                val colorValue = "#" + color.toString().substring(2, 8)

                val stream = ByteArrayInputStream(createSvg(initials, colorValue, size).toByteArray())
                Image(stream, size, size, true, true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

    private fun getInitial(name: String): String {
        return if (name.contains(" ")) {
            val splits = name.split(" ")
            if (splits.size >= 2) {
                "${splits[0][0]}" + "${splits[splits.size - 1][0]}"
            } else {
                "${splits[0][0]}"
            }
        } else {
            "${name[0]}"
        }
    }

    private fun createSvg(initials: String, colorValue: String, size: Double): String {
        return "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>" +
                "<g>" +
                "   <rect x='0' y='0' height='100' width='100' style='fill:" + colorValue + "'></rect>" +
                "   <text text-anchor='middle' x='50%' y='70%' style='font-size:" + size / 2 + "; fill: white;'>" + initials.toUpperCase() + "</text>" +
                "</g>" +
                "</svg>"
    }
}