package org.dizitart.letterbox.email.providers.yahoo

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension

@Extension
class YahooAccountProvider: EmailAccountProvider {
    override val providerColor: Color = Color.LIGHTGREEN
    override val providerLogo = Image("/images/yahoo.svg", 50.0, 50.0, true, true)
    override val providerName = "Yahoo"
    override val wizardPage: LetterBoxWizardPage? = null
}