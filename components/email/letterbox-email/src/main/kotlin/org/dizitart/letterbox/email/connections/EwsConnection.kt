package org.dizitart.letterbox.email.connections

import org.dizitart.letterbox.api.email.*
import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.api.email.entities.EmailMessage
import org.dizitart.letterbox.api.email.entities.MailBoxItem
import org.dizitart.letterbox.api.email.formatter.ResponseFormatter
import org.dizitart.letterbox.api.email.search.SearchFilter
import org.dizitart.letterbox.api.email.search.SearchResult

/**
 *
 * @author Anindya Chatterjee
 */
class EwsConnection : MailBoxConnection {
    override var accountId: Long
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        set(value) {}

    override fun getFolder(folderId: String): EmailFolder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllFolders(fetchMode: FetchMode): List<EmailFolder> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllChildFolders(folderId: String, fetchMode: FetchMode): List<EmailFolder> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getStandardEmailFolder(standardFolder: StandardFolder): EmailFolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUnreadCount(folderId: String): Int? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getTotalMessageCount(folderId: String): Int? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createFolder(folderName: String, parentId: String): EmailFolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun renameFolder(folderId: String, folderName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteFolder(folderId: String, deleteMode: DeleteMode) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun clearFolder(folderId: String, deleteMode: DeleteMode, recursive: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendMessage(message: EmailMessage, beforeSend: (EmailMessage.() -> EmailMessage)?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun fetchMessages(folderId: String, fetchOption: FetchOption): SearchResult<out MailBoxItem> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun search(folderId: String, searchFilter: SearchFilter, fetchOption: FetchOption): SearchResult<out MailBoxItem> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemById(itemId: String, fetchMode: FetchMode): MailBoxItem {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeItemStatus(itemId: String, readStatus: ReadStatus, sendReadReceipt: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun moveMessage(itemId: String, targetFolderId: String): MailBoxItem {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun copyMessage(itemId: String, targetFolderId: String): MailBoxItem {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteMessage(itemId: String, deleteMode: DeleteMode) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveDraft(message: EmailMessage) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createReplyMessage(message: EmailMessage, replyAll: Boolean, formatter: ResponseFormatter): EmailMessage {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createForwardMessage(message: EmailMessage, formatter: ResponseFormatter): EmailMessage {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun close() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isValid(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reset() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}