package org.dizitart.letterbox.email.views

import javafx.beans.property.Property
import javafx.beans.property.SimpleDoubleProperty
import javafx.scene.image.ImageView
import javafx.scene.shape.Circle
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.email.controllers.AvatarController
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class Avatar(private val circular: Boolean = true) : Fragment() {
    private val controller: AvatarController by di()
    private var imageView: ImageView by singleAssign()

    val sizeProperty = SimpleDoubleProperty(40.0)
    var size by sizeProperty

    override val root = pane {
        maxWidthProperty().bind(sizeProperty)
        maxHeightProperty().bind(sizeProperty)
        imageview {
            imageView = this
        }
    }

    fun bind(emailAddress: Property<EmailAddress>) {
        emailAddress.onChange { address ->
            address?.let {
                runAsync(true) {
                    val image = controller.createAvatarImage(address, size)
                    runLater {
                        imageView.image = image
                        if (circular) {
                            val clip = Circle(size / 2, size / 2, size / 2)
                            imageView.clip = clip
                        }
                    }
                }
            }
        }
    }

    fun render(address: EmailAddress) {
        runAsync(true) {
            val image = controller.createAvatarImage(address, size)
            runLater {
                imageView.image = image
                if (circular) {
                    val clip = Circle(size / 2, size / 2, size / 2)
                    imageView.clip = clip
                }
            }
        }
    }
}