package org.dizitart.letterbox.email.models

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.ChangeAware
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.api.email.entities.EmailFolderId
import org.dizitart.letterbox.api.email.entities.MailBoxItemId
import java.io.Serializable
import java.time.ZonedDateTime

open class EmailMessageLite : ChangeAware, Serializable {
    var id: MailBoxItemId? = null
    var folderId: EmailFolderId? = null
    var timestamp: ZonedDateTime? = null
    var account: EmailAccount? = null
    var subject: String? = null
    var from: EmailAddress? = null
    var favorite: Boolean = false
    var important: Boolean = false
    var unread: Boolean = true
    var attachment: Boolean = false
    var summary: String? = null

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is EmailMessageLite) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .build()!!
    }

    @Transient
    override var onChange: (() -> Unit)? = null
}