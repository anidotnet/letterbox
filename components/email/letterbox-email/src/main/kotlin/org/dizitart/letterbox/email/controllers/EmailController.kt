package org.dizitart.letterbox.email.controllers

import org.dizitart.letterbox.common.controllers.PageController
import org.dizitart.letterbox.email.service.EmailService
import org.dizitart.letterbox.email.EmailFolderSelected
import org.dizitart.letterbox.email.EmailMessageSelected
import org.dizitart.letterbox.email.EmailView
import org.dizitart.letterbox.email.StartAccountSync
import org.dizitart.letterbox.email.views.EmailDetailsView
import org.pf4j.Extension


/**
 * @author Anindya Chatterjee.
 */
@Extension
class EmailController : PageController() {
    private val service: EmailService by di()

    override val tabView: EmailView by inject()

    lateinit var emailDetails: EmailDetailsView

    init {
        subscribe<EmailFolderSelected> {
            emailDetails.root.isVisible = false
            fire(EmailMessageSelected(null))
        }

        subscribe<EmailMessageSelected> { e ->
            if (e.emailMessage != null) {
                emailDetails.root.isVisible = true
            }
        }
    }

    override fun init() {
        super.init()
        fire(StartAccountSync)
//        val accountList = service.getAccounts()

//        find<ProviderPicker> {
//            openWindow()
//        }
//        if (accountList.isEmpty()) {
//            // TODO: run wizard to add account
//        } else {
//            fire(StartAccountSync)
//        }
    }
}


