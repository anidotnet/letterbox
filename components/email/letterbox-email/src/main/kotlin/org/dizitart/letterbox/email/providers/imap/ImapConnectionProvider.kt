package org.dizitart.letterbox.email.providers.imap

import org.dizitart.letterbox.api.email.ConnectionProvider
import org.dizitart.letterbox.api.email.models.EmailAccount
import org.dizitart.letterbox.email.connections.ImapConnection

/**
 *
 * @author Anindya Chatterjee
 */
class ImapConnectionProvider : ConnectionProvider<ImapConnection> {
    override fun createConnection(key: EmailAccount): ImapConnection {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}