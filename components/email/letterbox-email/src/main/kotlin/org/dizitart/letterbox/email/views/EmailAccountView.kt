package org.dizitart.letterbox.email.views

import javafx.geometry.Pos
import javafx.scene.control.TreeItem
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Priority
import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.email.EmailFolderSelected
import org.dizitart.letterbox.email.controllers.EmailAccountController
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailAccountView : View() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    private val controller: EmailAccountController by di()

    override val root = vbox {
        layoutY = 6.0
        prefWidth = 100.0
        AnchorPane.setBottomAnchor(this, 0.0)
        AnchorPane.setTopAnchor(this, 0.0)
        AnchorPane.setLeftAnchor(this, 0.0)
        AnchorPane.setRightAnchor(this, 0.0)
        button {
            addClass(BaseLetterBoxTheme.newButton)
            alignment = Pos.CENTER
            maxHeight = 80.0
            maxWidth = Double.POSITIVE_INFINITY
            minHeight = 80.0
            vgrow = Priority.ALWAYS
            image = iconSet.newItemImage
            action {
                controller.newEmail()
            }
        }
        vbox {
            vgrow = Priority.ALWAYS
            treetableview<EmailFolderModel> {
                addClass(BaseLetterBoxTheme.folderTree, BaseLetterBoxTheme.vScroll, BaseLetterBoxTheme.noHScroll)
                vgrow = Priority.ALWAYS

                // construct root with dummy EmailAccountInfo node
                val dummFolder = EmailFolder()
                val rootFolder = EmailFolderModel(dummFolder).apply {
                    children = controller.accountFolderList
                }
                root = TreeItem(rootFolder)
                isShowRoot = false

                column("Folder", EmailFolderModel::nameProperty).apply {
                    prefWidthProperty().bind(this@treetableview.widthProperty().multiply(0.85))
                    setCellFactory {
                        AccountFolderCell(iconSet)
                    }
                }
                column("Folder", EmailFolderModel::unreadCountProperty).apply {
                    addClass(BaseLetterBoxTheme.countColumn)
                    prefWidthProperty().bind(this@treetableview.widthProperty().multiply(0.15))
                    this.cellFormat {
                        text = if (it == 0L) "" else it.toString()
                    }
                }

                populate { it.value.children }

                root.isExpanded = true
                root.children.forEach { it.isExpanded = true }
                controller.accountRoot = root

                selectionModel.selectedItemProperty().onChange {
                    if (!it?.value?.accountFolder!!) {
                        fire(EmailFolderSelected(it.value))
                    }
                }
            }
        }
    }
}
