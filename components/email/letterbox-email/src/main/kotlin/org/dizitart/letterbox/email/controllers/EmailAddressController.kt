package org.dizitart.letterbox.email.controllers

import javafx.scene.layout.HBox
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.springframework.stereotype.Component
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
@Component
class EmailAddressController : Controller() {

    fun infoUi(emailAddress: EmailAddress): HBox {
        return HBox()
    }

    fun saveAddress(emailAddress: EmailAddress?) {

    }

    fun sendEmail(emailAddress: EmailAddress?) {

    }

}