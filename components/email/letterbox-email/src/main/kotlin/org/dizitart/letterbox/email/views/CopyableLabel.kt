package org.dizitart.letterbox.email.views

import javafx.beans.property.SimpleStringProperty
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import tornadofx.*


/**
 *
 * @author Anindya Chatterjee
 */
class CopyableLabel(text: String = "") : Fragment() {
    val textProperty = SimpleStringProperty()
    var text by textProperty

    override val root = label(text) {
        isWrapText = true
        maxHeight = Double.POSITIVE_INFINITY
        minHeight = Double.NEGATIVE_INFINITY
        contextmenu {
            item("Copy Text") {
                action {
                    if (this@label.text.isNotEmpty()) {
                        val content = ClipboardContent()
                        content.putString(this@label.text)
                        Clipboard.getSystemClipboard().setContent(content)
                    }
                }
            }
        }
        textProperty().bindBidirectional(textProperty)
    }

    fun addClass(vararg styleRules: CssRule) {
        root.addClass(*styleRules)
    }
}