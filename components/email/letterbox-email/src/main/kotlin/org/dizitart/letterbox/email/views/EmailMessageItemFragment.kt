package org.dizitart.letterbox.email.views

import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import org.dizitart.letterbox.email.models.EmailMessageModel
import org.dizitart.letterbox.email.models.EmailMessageViewModel
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.formatter.toSummeryDate
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailMessageItemFragment : ListCellFragment<EmailMessageModel>() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    private val emailMessage = EmailMessageViewModel(itemProperty)

    private var attachmentImageView: ImageView by singleAssign()
    private var favoriteImageView: ImageView by singleAssign()
    private var fromLabel: Label by singleAssign()
    private var dateLabel: Label by singleAssign()

    override val root = hbox {
        addClass(BaseLetterBoxTheme.emailSummery)
        alignment = Pos.CENTER_LEFT
        prefWidth = 300.0
        prefHeight = 70.0
        spacing = 5.0
        region {
            toggleClass(BaseLetterBoxTheme.unreadIndicator, emailMessage.unread)
            prefWidth = 5.0
        }
        avatar {
            size = 40.0
            bind(emailMessage.from)
        }
        region {
            prefWidth = 10.0
        }
        vbox {
            prefHeight = 200.0
            prefWidth = 100.0
            hgrow = Priority.ALWAYS
            region {
                prefHeight = 5.0
            }
            hbox {
                label {
                    addClass(BaseLetterBoxTheme.fromLabel)
                    fromLabel = this
                }
                region {
                    hgrow = Priority.ALWAYS
                }
                label {
                    addClass(BaseLetterBoxTheme.dateLabel)
                    dateLabel = this
                }
                region {
                    prefWidth = 5.0
                }
            }
            region {
                prefHeight = 5.0
            }
            hbox {
                label(emailMessage.subject) {
                    addClass(BaseLetterBoxTheme.subjectLabel)
                }
                region {
                    hgrow = Priority.ALWAYS
                }
                imageview {
                    attachmentImageView = this
                }
                region {
                    prefWidth = 5.0
                }
            }
            region {
                prefHeight = 5.0
            }
            hbox {
                vgrow = Priority.ALWAYS
                label(emailMessage.summary) {
                    addClass(BaseLetterBoxTheme.contentLabel)
                    maxWidth = 400.0
                }
                region {
                    hgrow = Priority.ALWAYS
                }
                imageview {
                    favoriteImageView = this
                }
                region {
                    minWidth = 5.0
                }
            }
        }
    }

    init {
        with(emailMessage) {
            attachment.onChange { hasAttachment ->
                hasAttachment?.let {
                    runLater {
                        if (hasAttachment) {
                            attachmentImageView.image = iconSet.attachmentImage
                        }
                    }
                }
            }

            favorite.onChange { isFavorite ->
                isFavorite?.let {
                    runLater {
                        if (isFavorite) {
                            favoriteImageView.image = iconSet.favoriteImageSmall
                        } else {
                            favoriteImageView.image = null
                        }
                    }
                }
            }

            from.onChange { emailAddress ->
                emailAddress?.let {
                    runLater {
                        fromLabel.text = emailAddress.name
                    }
                }
            }

            date.onChange { dateTime ->
                dateTime?.let {
                    runLater {
                        dateLabel.text = dateTime.toSummeryDate()
                    }
                }
            }
        }
    }
}