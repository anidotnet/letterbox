package org.dizitart.letterbox.email.providers.gmail

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
@Extension
class GmailAccountProvider : EmailAccountProvider {
    override val providerColor: Color = Color.RED
    override val wizardPage: LetterBoxWizardPage by lazy { find(GmailAccountInfoPage::class) }
    override val providerLogo = Image("/images/gmail.svg", 50.0, 50.0, true, true)
    override val providerName = "GMail / GSuite"
}

