package org.dizitart.letterbox.email.providers.gmail

import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.email.connections.ImapConnection
import org.dizitart.letterbox.email.providers.BaseConnectionProvider

/**
 *
 * @author Anindya Chatterjee
 */
class GmailConnectionProvider : BaseConnectionProvider<ImapConnection>() {

    override fun createConnectionWithAccount(accountInfo: EmailAccountInfo): ImapConnection {

    }
}