package org.dizitart.letterbox.email.views

import javafx.event.EventTarget
import org.dizitart.letterbox.api.email.entities.EmailAddress
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */

fun EventTarget.add(uiComponent: UIComponent, op: (UIComponent.() -> Unit)? = null) {
    plusAssign(uiComponent.root)
    op?.invoke(uiComponent)
}

fun EventTarget.copyablelabel(text: String = "", op: (CopyableLabel.() -> Unit)? = null): CopyableLabel {
    val label = CopyableLabel(text)
    add(label) {
        op?.invoke(label)
    }
    return label
}

fun EventTarget.avatar(circular: Boolean = true, op: (Avatar.() -> Unit)? = null): Avatar {
    val avatar = Avatar(circular)
    add(avatar) {
        op?.invoke(avatar)
    }
    return avatar
}

fun EventTarget.emailaddress(emailAddress: EmailAddress? = null,
                             closable: Boolean = false,
                             large: Boolean = false,
                             decorated: Boolean = false,
                             op: (EmailAddressFragment.() -> Unit)? = null): EmailAddressFragment {
    val emailAddressFragment = EmailAddressFragment(emailAddress, closable, large, decorated)
    add(emailAddressFragment) {
        op?.invoke(emailAddressFragment)
    }
    return emailAddressFragment
}
