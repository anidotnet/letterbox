package org.dizitart.letterbox.email.service

import javafx.scene.image.Image
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


/**
 *
 * @author Anindya Chatterjee
 */
@Lazy
@Service
class EmailAddressInfoService : InitializingBean {
    @Autowired private lateinit var infoProviders: List<EmailAddressInfoProvider>

    private val domainProviderMap: MutableMap<String, EmailAddressInfoProvider> = HashMap()

    override fun afterPropertiesSet() {
        for (provider in infoProviders) {
            val domain = provider.domain
            if (!domainProviderMap.containsKey(domain)) {
                domainProviderMap.put(domain, provider)
            }
        }
    }

    fun getAvatarImage(emailAddress: EmailAddress) : Image? {
        val domain = emailAddress.address?.split("@")!![1]
        return domainProviderMap[domain]?.extractAvatarImage(emailAddress)
    }
}