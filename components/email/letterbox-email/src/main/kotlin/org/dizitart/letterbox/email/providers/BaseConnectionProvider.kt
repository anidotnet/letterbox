package org.dizitart.letterbox.email.providers

import org.dizitart.letterbox.api.email.ConnectionProvider
import org.dizitart.letterbox.api.email.MailBoxConnection
import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.api.config.di
import org.dizitart.letterbox.email.service.EmailAccountService

/**
 *
 * @author Anindya Chatterjee
 */
abstract class BaseConnectionProvider<out Connection : MailBoxConnection>
    : ConnectionProvider<Connection> {

    private val accountService: EmailAccountService by di()

    @Suppress("UNCHECKED_CAST")
    override fun createConnection(accountId: Long): Connection {
        val accountInfo = accountService.emailAccount(accountId)
        return createConnectionWithAccount(accountInfo!!)
    }

    internal abstract fun createConnectionWithAccount(accountInfo: EmailAccountInfo) : Connection
}