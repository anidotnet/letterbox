package org.dizitart.letterbox.email.transformers

import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.api.mappers.transform
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.service.EmailFolderStoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * @author Anindya Chatterjee.
 */
@Component
internal class EmailFolderToModel : Transformer<EmailFolder, EmailFolderModel> {

    @Autowired private lateinit var storeService: EmailFolderStoreService

    override fun transform(source: EmailFolder?): EmailFolderModel? {
        if (source == null) return null
        val model = EmailFolderModel()
        model.apply {
            id = source.id
            name = source.name
            accountId = source.accountId!!
            parent = parent(source)
            unreadCount = storeService.unreadCount(source)
            ordinal = source.folderType.ordinal
            accountFolder = false
            folderType = source.folderType
            children.addAll(children(source))
        }
        return model
    }

    private fun parent(source: EmailFolder) : EmailFolderModel? {
        return transform(storeService.parentFolder(source))
    }

    private fun children(source: EmailFolder) : List<EmailFolderModel> {
        return storeService.children(source)
                .transform(this)
                .toList()
    }
}