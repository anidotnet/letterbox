package org.dizitart.letterbox.email.controllers

import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
@Lazy
@Component
class AccountWizardController : Controller() {
    @Autowired lateinit var accountProviders: List<EmailAccountProvider>
}