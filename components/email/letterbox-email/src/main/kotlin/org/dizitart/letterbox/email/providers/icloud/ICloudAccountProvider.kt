package org.dizitart.letterbox.email.providers.icloud

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension

@Extension
class ICloudAccountProvider: EmailAccountProvider {
    override val providerColor: Color = Color.DARKGRAY
    override val providerLogo = Image("/images/icloud.svg", 50.0, 50.0, true, true)
    override val providerName = "iCloud"
    override val wizardPage: LetterBoxWizardPage? = null
}