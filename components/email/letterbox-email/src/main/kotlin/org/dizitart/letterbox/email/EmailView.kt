package org.dizitart.letterbox.email

import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.SplitPane
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Priority
import org.dizitart.letterbox.api.views.PageView
import org.dizitart.letterbox.common.MAIL_LIST_WIDTH
import org.dizitart.letterbox.common.MAIL_TREE_WIDTH
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.views.LetterBoxMenu
import org.dizitart.letterbox.common.views.PageView
import org.dizitart.letterbox.common.views.screenWidth
import org.dizitart.letterbox.email.controllers.EmailController
import org.dizitart.letterbox.email.views.EmailAccountView
import org.dizitart.letterbox.email.views.EmailDetailsView
import org.dizitart.letterbox.email.views.EmailListView
import tornadofx.*


class EmailView : View("Email"), PageView {

    private val taskStatus: TaskStatus by inject()

    private val controller: EmailController by di()

    override val root: Parent = anchorpane {
        AnchorPane.setTopAnchor(this, 0.0)
        AnchorPane.setRightAnchor(this, 0.0)
        AnchorPane.setBottomAnchor(this, 0.0)
        AnchorPane.setLeftAnchor(this, 0.0)

        splitpane {
            AnchorPane.setTopAnchor(this, 0.0)
            AnchorPane.setRightAnchor(this, 0.0)
            AnchorPane.setBottomAnchor(this, 0.0)
            AnchorPane.setLeftAnchor(this, 0.0)
            setDividerPositions(0.15, 0.45)

            anchorpane {
                addClass(BaseLetterBoxTheme.treePane)
                SplitPane.setResizableWithParent(this, false)
                minWidth = screenWidth() * 0.14
                prefWidth = LetterBoxContext.appConfig.double(MAIL_TREE_WIDTH, screenWidth() * 0.14)!!
                add(EmailAccountView::class)
                LetterBoxContext.appConfig.bind(MAIL_TREE_WIDTH, widthProperty())
            }
            anchorpane {
                addClass(BaseLetterBoxTheme.listPane)
                SplitPane.setResizableWithParent(this, false)
                minWidth = screenWidth() * 0.25
                prefWidth = LetterBoxContext.appConfig.double(MAIL_LIST_WIDTH, screenWidth() * 0.25)!!
                add(EmailListView::class)
                LetterBoxContext.appConfig.bind(MAIL_LIST_WIDTH, widthProperty())
            }
            anchorpane {
                addClass(BaseLetterBoxTheme.detailsPane)
                SplitPane.setResizableWithParent(this, true)
                vbox {
                    AnchorPane.setBottomAnchor(this, 0.0)
                    AnchorPane.setTopAnchor(this, 0.0)
                    AnchorPane.setLeftAnchor(this, 0.0)
                    AnchorPane.setRightAnchor(this, 0.0)
                    hbox {
                        region {
                            hgrow = Priority.ALWAYS
                        }
                        add(LetterBoxMenu::class)
                    }
                    pane {
                        minHeight = 10.0
                    }
                    add(EmailDetailsView::class) {
                        root.isVisible = false
                        controller.emailDetails = this
                    }
                    hbox {
                        addClass(BaseLetterBoxTheme.statusPane)
                        alignment = Pos.CENTER_RIGHT
                        label(taskStatus.message) {
                            addClass(BaseLetterBoxTheme.statusLabel)
                            visibleWhen {
                                taskStatus.running
                            }
                        }
                        progressbar (taskStatus.progress) {
                            addClass(BaseLetterBoxTheme.statusProgress)
                            visibleWhen {
                                taskStatus.running
                            }
                        }
                    }
                }
            }
        }
    }
}