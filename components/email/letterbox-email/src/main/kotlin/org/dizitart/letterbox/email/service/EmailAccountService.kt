package org.dizitart.letterbox.email.service

import javafx.collections.ObservableList
import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.api.mappers.transform
import org.dizitart.letterbox.common.collections.update
import org.dizitart.letterbox.api.database.LetterBoxStore
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.store.EmailStore
import org.dizitart.letterbox.email.validators.Validator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 *
 * @author Anindya Chatterjee
 */
@Component
class EmailAccountService {
    @Autowired private lateinit var accountValidator: Validator<EmailAccountInfo>
    @Autowired private lateinit var emailStore: EmailStore
    @Autowired private lateinit var accountInfoTransformer: Transformer<EmailAccountInfo, EmailFolderModel>

    fun syncAccountFolders(accountList: ObservableList<EmailFolderModel>) {
        val folderList = emailStore.getAccounts()
                .transform(accountInfoTransformer)
        for (emailFolder in folderList) {
            accountList.update(emailFolder)
        }
    }

    fun validateAndSaveAccount(item: EmailAccountInfo?) {
        if (item != null && accountValidator.validate(item)) {
            LetterBoxStore.repository(EmailAccountInfo::class.java).save(item)
        }
    }

    fun emailAccountIdList() : List<Long> {
        val repository = LetterBoxStore.repository(EmailAccountInfo::class.java)
        if (repository.size() == 0L) return ArrayList()
        return repository.findAll().map(EmailAccountInfo::id)
    }

    fun emailAccount(accountId: Long) : EmailAccountInfo? {
        return emailStore.getAccountById(accountId)
    }
}