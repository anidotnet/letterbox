package org.dizitart.letterbox.email.store

import org.dizitart.kno2.nitrite
import org.dizitart.letterbox.api.email.StandardFolder
import org.dizitart.letterbox.api.email.emailAccount
import org.dizitart.letterbox.api.email.emailFolder
import org.dizitart.letterbox.api.email.emailMessage
import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.api.email.entities.EmailMessage
import org.dizitart.letterbox.api.database.LetterBoxStore
import org.dizitart.letterbox.api.database.RepositoryOperation
import org.dizitart.no2.Nitrite
import org.springframework.stereotype.Component
import tornadofx.*
import uk.co.jemos.podam.api.PodamFactoryImpl
import java.time.ZonedDateTime
import java.util.*
import javax.annotation.PostConstruct


/**
 * @author Anindya Chatterjee.
 */
@Component
class EmailStore {
    private var accountStoreMap: MutableMap<Long, Nitrite> = mutableMapOf()
    private var accountEmailStoreMap: MutableMap<Long, RepositoryOperation<EmailMessage>> = mutableMapOf()
    private var accountFolderStoreMap: MutableMap<Long, RepositoryOperation<EmailFolder>> = mutableMapOf()

    @PostConstruct
    fun init() {
        createDummyData()
    }

    fun getAccounts() : List<EmailAccountInfo> {
        return LetterBoxStore.repository(EmailAccountInfo::class.java)
                .findAll()
                .toList()
    }

    fun getAccountById(accountId: Long): EmailAccountInfo? {
        return LetterBoxStore.repository(EmailAccountInfo::class.java)
                .find(EmailAccountInfo::id eq accountId)
                .firstOrDefault()
    }

    // each email account will have separate db for storing message
    fun emailMessageRepository(accountId: Long) : RepositoryOperation<EmailMessage> {
        var messageRepo = accountEmailStoreMap[accountId]
        if (messageRepo != null) return messageRepo

        val accountDb = accountDb(accountId)
        messageRepo = RepositoryOperation(accountDb, EmailMessage::class.java)
        accountEmailStoreMap[accountId] = messageRepo
        return messageRepo
    }

    // each email account will have separate db for storing message folder
    fun emailFolderRepository(accountId: Long) : RepositoryOperation<EmailFolder> {
        var folderRepo = accountFolderStoreMap[accountId]
        if (folderRepo != null) return folderRepo

        val accountDb = accountDb(accountId)
        folderRepo = RepositoryOperation(accountDb, EmailFolder::class.java)
        accountFolderStoreMap[accountId] = folderRepo
        return folderRepo
    }

    // account specific db
    private fun accountDb(accountId: Long) : Nitrite {
        var accountDb = accountStoreMap[accountId]
        if (accountDb == null) {
            accountDb = nitrite {
                val cursor = LetterBoxStore.repository(EmailAccountInfo::class.java)
                        .find(EmailAccountInfo::id eq accountId)
                val emailAccountInfo = cursor.firstOrDefault()
                path = emailAccountInfo.storePath
            }

            beforeShutdown {
                accountDb.close()
            }

            accountStoreMap[accountId] = accountDb
        }
        return accountDb
    }

    private fun createDummyData() {
        val gmailAccount = emailAccount {
            emailAddress = "anidotnet@gmail.com"
            name = "Anindya Gmail"
            password = "123"
        }
        val outlookAccount = emailAccount {
            emailAddress = "anidotnet@outlook.com"
            name = "Anindya Outlook"
            password = "123"
        }

        LetterBoxStore.repository(EmailAccountInfo::class.java).save(gmailAccount, outlookAccount)

        val gmailInbox = emailFolder {
            uniqueFolderId = "[Gmail]/Inbox"
            name = "Inbox"
            accountId = gmailAccount.id
            folderType = StandardFolder.Inbox
        }
        val gmailInboxImportant = emailFolder {
            uniqueFolderId = "[Gmail]/Inbox/Important"
            name = "Important"
            accountId = gmailAccount.id
            parentId = gmailInbox.id
            folderType = StandardFolder.None
        }
        val gmailSent = emailFolder {
            uniqueFolderId = "[Gmail]/Sent Items"
            name = "Sent Items"
            accountId = gmailAccount.id
            folderType = StandardFolder.Sent
        }
        val gmailTrash = emailFolder {
            uniqueFolderId = "[Gmail]/Deleted Items"
            name = "Deleted Items"
            accountId = gmailAccount.id
            folderType = StandardFolder.Trash
        }
        val gmailOutbox = emailFolder {
            uniqueFolderId = "[Gmail]/Outbox"
            name = "Outbox"
            accountId = gmailAccount.id
            folderType = StandardFolder.None
        }
        val gmailDraft = emailFolder {
            uniqueFolderId = "[Gmail]/Draft"
            name = "Draft"
            accountId = gmailAccount.id
            folderType = StandardFolder.Drafts
        }
        val gmailSpam = emailFolder {
            uniqueFolderId = "[Gmail]/Spam"
            name = "Spam"
            accountId = gmailAccount.id
            folderType = StandardFolder.Junk
        }
        emailFolderRepository(gmailAccount.id)
                .save(gmailInbox, gmailInboxImportant, gmailSent, gmailTrash, gmailOutbox, gmailDraft, gmailSpam)

        val outlookInbox = emailFolder {
            uniqueFolderId = "Inbox"
            name = "Inbox"
            accountId = outlookAccount.id
            folderType = StandardFolder.Inbox
        }
        val outlookSent = emailFolder {
            uniqueFolderId = "Sent Items"
            name = "Sent Items"
            accountId = outlookAccount.id
            folderType = StandardFolder.Sent
        }
        val outlookTrash = emailFolder {
            uniqueFolderId = "Trash"
            name = "Trash"
            accountId = outlookAccount.id
            folderType = StandardFolder.Trash
        }
        val outlookOutbox = emailFolder {
            uniqueFolderId = "Outbox"
            name = "Outbox"
            accountId = outlookAccount.id
            folderType = StandardFolder.None
        }
        val outlookDraft = emailFolder {
            uniqueFolderId = "Draft"
            name = "Draft"
            accountId = outlookAccount.id
            folderType = StandardFolder.Drafts
        }
        val outlookSpam = emailFolder {
            uniqueFolderId = "Spam"
            name = "Spam"
            accountId = outlookAccount.id
            folderType = StandardFolder.Junk
        }

        emailFolderRepository(outlookAccount.id)
                .save(outlookInbox, outlookSent, outlookTrash, outlookOutbox, outlookDraft, outlookSpam)

        createMessages()
    }

    private fun createMessages() {
        val accountCursor = LetterBoxStore.repository(EmailAccountInfo::class.java).findAll()
        val random = Random()
        val factory = PodamFactoryImpl()

        accountCursor.forEach {
            val emailAccount = it.id
            val folderCursor = emailFolderRepository(emailAccount).findAll()
            folderCursor.forEach {
                val nameLc = it.name?.toLowerCase()!!
                when {
                    nameLc.contains("inbox") -> {
                        for (count in 1..random.nextInt(20)) {
                            emailMessageRepository(emailAccount).save(emailMessage {
                                folderId = it.id
                                timestamp = factory.manufacturePojo(ZonedDateTime::class.java)
                                accountId = emailAccount
                                subject = factory.manufacturePojo(String::class.java)
                                from = EmailAddress(factory.manufacturePojo(String::class.java), "anidotnet@gmail.com")
                                favorite = random.nextBoolean()
                                important = random.nextBoolean()
                                unread = random.nextBoolean()
                                toRecipients = createRecipients()
                                ccRecipients = createRecipients()
                                bccRecipients = createRecipients()
                                readReceiptRequired = random.nextBoolean()
                                messageBody = org.dizitart.letterbox.api.email.messageBody {
                                    setText(factory.manufacturePojo(String::class.java), false)
                                    if (random.nextBoolean()) {
                                        val content = ByteArray(random.nextInt())
                                        random.nextBytes(content)
                                        addAttachment(factory.manufacturePojo(String::class.java), content)
                                    }
                                }
                            })
                        }
                    }
                    nameLc.contains("important") -> {
                        for (count in 1..random.nextInt(20)) {
                            emailMessageRepository(emailAccount).save(emailMessage {
                                folderId = it.id
                                timestamp = factory.manufacturePojo(ZonedDateTime::class.java)
                                accountId = emailAccount
                                subject = factory.manufacturePojo(String::class.java)
                                from = EmailAddress(factory.manufacturePojo(String::class.java), "anidotnet@gmail.com")
                                favorite = random.nextBoolean()
                                important = random.nextBoolean()
                                unread = random.nextBoolean()
                                toRecipients = createRecipients()
                                ccRecipients = createRecipients()
                                bccRecipients = createRecipients()
                                readReceiptRequired = random.nextBoolean()
                                messageBody = org.dizitart.letterbox.api.email.messageBody {
                                    setText(factory.manufacturePojo(String::class.java), false)
                                    if (random.nextBoolean()) {
                                        val content = ByteArray(random.nextInt())
                                        random.nextBytes(content)
                                        addAttachment(factory.manufacturePojo(String::class.java), content)
                                    }
                                }
                            })
                        }
                    }
                    nameLc.contains("sent") -> {
                        for (count in 1..random.nextInt(20)) {
                            emailMessageRepository(emailAccount).save(emailMessage {
                                folderId = it.id
                                timestamp = factory.manufacturePojo(ZonedDateTime::class.java)
                                accountId = emailAccount
                                subject = factory.manufacturePojo(String::class.java)
                                from = EmailAddress(factory.manufacturePojo(String::class.java), "anidotnet@gmail.com")
                                favorite = random.nextBoolean()
                                important = random.nextBoolean()
                                unread = random.nextBoolean()
                                toRecipients = createRecipients()
                                ccRecipients = createRecipients()
                                bccRecipients = createRecipients()
                                readReceiptRequired = random.nextBoolean()
                                messageBody = org.dizitart.letterbox.api.email.messageBody {
                                    setText(factory.manufacturePojo(String::class.java), false)
                                    if (random.nextBoolean()) {
                                        val content = ByteArray(random.nextInt())
                                        random.nextBytes(content)
                                        addAttachment(factory.manufacturePojo(String::class.java), content)
                                    }
                                }
                            })
                        }
                    }
                    nameLc.contains("trash") -> {
                        for (count in 1..random.nextInt(20)) {
                            emailMessageRepository(emailAccount).save(emailMessage {
                                folderId = it.id
                                timestamp = factory.manufacturePojo(ZonedDateTime::class.java)
                                accountId = emailAccount
                                subject = factory.manufacturePojo(String::class.java)
                                from = EmailAddress(factory.manufacturePojo(String::class.java), "anidotnet@gmail.com")
                                favorite = random.nextBoolean()
                                important = random.nextBoolean()
                                unread = random.nextBoolean()
                                toRecipients = createRecipients()
                                ccRecipients = createRecipients()
                                bccRecipients = createRecipients()
                                readReceiptRequired = random.nextBoolean()
                                messageBody = org.dizitart.letterbox.api.email.messageBody {
                                    setText(factory.manufacturePojo(String::class.java), false)
                                    if (random.nextBoolean()) {
                                        val content = ByteArray(random.nextInt())
                                        random.nextBytes(content)
                                        addAttachment(factory.manufacturePojo(String::class.java), content)
                                    }
                                }
                            })
                        }
                    }
                    nameLc.contains("spam") -> {
                        emailMessageRepository(emailAccount).save(emailMessage {
                            folderId = it.id
                            timestamp = factory.manufacturePojo(ZonedDateTime::class.java)
                            accountId = emailAccount
                            subject = factory.manufacturePojo(String::class.java)
                            from = EmailAddress(factory.manufacturePojo(String::class.java), "anidotnet@gmail.com")
                            favorite = random.nextBoolean()
                            important = random.nextBoolean()
                            unread = random.nextBoolean()
                            toRecipients = createRecipients()
                            ccRecipients = createRecipients()
                            bccRecipients = createRecipients()
                            readReceiptRequired = random.nextBoolean()
                            messageBody = org.dizitart.letterbox.api.email.messageBody {
                                setText(factory.manufacturePojo(String::class.java), false)
                                if (random.nextBoolean()) {
                                    val content = ByteArray(random.nextInt())
                                    random.nextBytes(content)
                                    addAttachment(factory.manufacturePojo(String::class.java), content)
                                }
                            }
                        })
                    }
                }
            }
        }
    }

    private fun createRecipients(): MutableSet<EmailAddress> {
        val random = Random(System.nanoTime())
        val address = mutableSetOf<EmailAddress>()
        val factory = PodamFactoryImpl()
        for(i in 1..random.nextInt(20)) {
            address.add(EmailAddress(factory.manufacturePojo(String::class.java)
                    + " " + factory.manufacturePojo(String::class.java),
                    factory.manufacturePojo(String::class.java)
                            + "@" + factory.manufacturePojo(String::class.java) + ".com"))
        }
        return address
    }
}