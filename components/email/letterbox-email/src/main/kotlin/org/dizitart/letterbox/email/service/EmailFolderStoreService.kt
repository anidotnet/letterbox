package org.dizitart.letterbox.email.service

import org.dizitart.kno2.filters.and
import org.dizitart.kno2.filters.eq
import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.api.email.entities.EmailMessage
import org.dizitart.letterbox.email.store.EmailStore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * @author Anindya Chatterjee.
 */
@Component
class EmailFolderStoreService {
    @Autowired private lateinit var emailStore: EmailStore

    fun parentFolder(folder: EmailFolder) : EmailFolder? {
        return emailStore.emailFolderRepository(folder.accountId!!)
                .find(EmailFolder::id eq folder.id).firstOrDefault()
    }

    fun unreadCount(folder: EmailFolder) : Long {
        return emailStore.emailMessageRepository(folder.accountId!!)
                .find((EmailMessage::unread eq true) and
                        (EmailMessage::folderId eq folder.id))
                .size()
                .toLong()
    }

    fun children(folder: EmailFolder) : Iterable<EmailFolder> {
        return emailStore.emailFolderRepository(folder.accountId!!)
                .find(EmailFolder::parentId eq folder.id)
    }

    fun topLevelFolders(accountId: Long) : Iterable<EmailFolder> {
        return emailStore.emailFolderRepository(accountId)
                .find((EmailFolder::accountId eq accountId)
                        and (EmailFolder::parentId eq -1L))
    }
}