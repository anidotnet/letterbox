package org.dizitart.letterbox.email.views

import javafx.geometry.Pos
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import tornadofx.*

class ProviderTile(private val provider: EmailAccountProvider) : Fragment() {
    private var onClick: (() -> Unit)? = null

    override val root = hbox {
        minHeight = 60.0
        minWidth = 220.0
        alignment = Pos.CENTER_LEFT
        addClass(BaseLetterBoxTheme.accountTile)
        pane {
            isMouseTransparent = true
            minWidth = 4.0
            background = Background(BackgroundFill(provider.providerColor, null, null))
        }
        pane {
            isMouseTransparent = true
            minWidth = 10.0
        }
        imageview {
            isMouseTransparent = true
            image = provider.providerLogo
        }
        pane {
            isMouseTransparent = true
            minWidth = 20.0
        }
        label {
            isMouseTransparent = true
            text = provider.providerName
            style {
                textFill = Color.GREY
                fontWeight = FontWeight.MEDIUM
                fontSize = 14.px
            }
        }
        pane {
            isMouseTransparent = true
            minWidth = 10.0
        }

        setOnMouseClicked {
            parent.childrenUnmodifiable.forEach {
                it.removePseudoClass("selected")
            }
            addPseudoClass("selected")
            onClick?.invoke()
        }
    }

    fun setOnClick(op: (() -> Unit)) {
        onClick = op
    }
}