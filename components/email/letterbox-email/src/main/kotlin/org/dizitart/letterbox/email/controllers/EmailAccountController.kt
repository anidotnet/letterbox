package org.dizitart.letterbox.email.controllers

import javafx.scene.control.TreeItem
import org.dizitart.letterbox.email.EmailFolderSelected
import org.dizitart.letterbox.email.StartAccountSync
import org.dizitart.letterbox.email.SyncEmailMessages
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.service.EmailAccountService
import org.springframework.stereotype.Component
import tornadofx.Controller
import tornadofx.observable
import tornadofx.runLater
import tornadofx.singleAssign
import kotlin.concurrent.timer

/**
 * @author Anindya Chatterjee.
 */
@Component
class EmailAccountController : Controller() {
    private val service: EmailAccountService? by di()
    private var firstTime = true

    val accountFolderList = mutableListOf<EmailFolderModel>().observable()
    var accountRoot: TreeItem<EmailFolderModel> by singleAssign()
    private var selectedFolder: EmailFolderModel? = null

    init {
        subscribe<StartAccountSync> {
            scheduleAccountSync()
        }

        subscribe<EmailFolderSelected> { (emailFolder) ->
            selectedFolder = emailFolder
        }
    }

    private fun scheduleAccountSync() {
        timer("", true, 500, 1000) {
            runLater {
                if (service != null) {
                    service?.syncAccountFolders(accountFolderList)
                    accountRoot.isExpanded = true
                    if (firstTime) {
                        accountRoot.children.forEach {
                            it.isExpanded = true
                        }
                        firstTime = false
                    }

                    if (selectedFolder != null) {
                        fire(SyncEmailMessages(selectedFolder!!))
                    }
                }
            }
        }
    }

    fun newEmail() {
        runAsync {
            updateMessage("Starting job")
            updateProgress(0.5, 1.0)
            Thread.sleep(1000)
            updateProgress(1.0, 1.0)
        }
    }
}