package org.dizitart.letterbox.email.models

import javafx.beans.property.*
import org.dizitart.letterbox.api.Identifiable
import org.dizitart.letterbox.api.email.entities.*
import org.dizitart.letterbox.api.email.models.EmailAccount
import org.dizitart.letterbox.api.email.models.EmailMessageLite
import tornadofx.*
import java.time.ZonedDateTime

/**
 * @author Anindya Chatterjee.
 */

class EmailAccountModel : ItemViewModel<EmailAccount>() {
    val id = bind(EmailAccount::id)
    val emailAddress = bind(EmailAccount::emailAddress)
    var name = bind(EmailAccount::name)
}

class EmailAccountInfoModel : ItemViewModel<EmailAccountInfo>() {
    val id = bind(EmailAccountInfo::id)
    val address = bind(EmailAccountInfo::address)
    var name = bind(EmailAccountInfo::name)
    var password= bind(EmailAccountInfo::password)
}

class EmailMessageViewModel(property: ObjectProperty<EmailMessageModel>)
    : ItemViewModel<EmailMessageModel>(itemProperty = property) {

    val id = bind(EmailMessageModel::id)
    val folder = bind { item?.folderProperty }
    val from = bind { item?.fromProperty }
    val date = bind { item?.dateProperty }
    val subject = bind { item?.subjectProperty }
    val summary = bind { item?.summaryProperty }
    val attachment = bind { item?.attachmentProperty }
    val favorite = bind(autocommit = true) { item?.favoriteProperty }
    val unread = bind(autocommit = true) { item?.unreadProperty }
}

class EmailMessageModel(message: EmailMessageLite, folder: EmailFolderModel)
    : Identifiable<MailBoxItemId?> {

    val idProperty = SimpleObjectProperty(message.id)
    override val id by idProperty

    val folderProperty = SimpleObjectProperty(folder)
    val folder by folderProperty

    val fromProperty = SimpleObjectProperty<EmailAddress>(message.from)
    var from by fromProperty

    val dateProperty = SimpleObjectProperty<ZonedDateTime>(message.timestamp)
    var date by dateProperty

    val subjectProperty = SimpleStringProperty(message.subject)
    var subject by subjectProperty

    val summaryProperty = SimpleStringProperty(message.summary)
    var summary by summaryProperty

    val attachmentProperty = SimpleBooleanProperty(message.attachment)
    var attachment by attachmentProperty

    val favoriteProperty = SimpleBooleanProperty(message.favorite)
    var favorite by favoriteProperty

    val unreadProperty = SimpleBooleanProperty(message.unread)
    var unread by unreadProperty

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as EmailMessageModel

        if (id == other.id) return true
        return false
    }

    override fun hashCode(): Int {
        return id?.hashCode()!!
    }

    init {
        favoriteProperty.onChange {
            message.favorite = it
            runAsync {
                message.changed()
            }
        }

        unreadProperty.onChange {
            message.unread = it
            runAsync {
                message.changed()
            }
        }
    }
}

