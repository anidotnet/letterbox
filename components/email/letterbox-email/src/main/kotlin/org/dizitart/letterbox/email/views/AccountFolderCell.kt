package org.dizitart.letterbox.email.views

import javafx.scene.control.TreeTableCell
import javafx.scene.image.ImageView
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.common.styles.IconSet

class AccountFolderCell(private val iconSet: IconSet) : TreeTableCell<EmailFolderModel, String>() {

    override fun updateItem(item: String?, empty: Boolean) {
        super.updateItem(item, empty)

        if (!empty) {
            text = item
            val lcText = text.toLowerCase()
            val image = if (lcText.contains("inbox")) {
                iconSet.inboxFolderImage
            } else if (lcText.contains("sent")) {
                iconSet.sentFolderImage
            } else if (lcText.contains("delete") || lcText.contains("trash")) {
                iconSet.trashFolderImage
            } else if (lcText.contains("draft")) {
                iconSet.draftFolderImage
            } else if (lcText.contains("outbox")) {
                iconSet.outboxFolderImage
            } else if (lcText.contains("junk") || lcText.contains("spam")) {
                iconSet.spamFolderImage
            } else if (lcText.contains("@") && lcText.contains(".")){
                null
            } else {
                iconSet.folderImage
            }
            graphic = ImageView(image)
            graphicTextGap = 10.0
        } else {
            text = null
            graphic = null
        }
    }
}