package org.dizitart.letterbox.email.providers.ews

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension

@Extension
class ExchangeAccountProvider: EmailAccountProvider {
    override val providerColor: Color = Color.LIGHTBLUE
    override val providerLogo = Image("/images/exchange.svg", 50.0, 50.0, true, true)
    override val providerName = "Exchange (EWS)"
    override val wizardPage: LetterBoxWizardPage? = null
}