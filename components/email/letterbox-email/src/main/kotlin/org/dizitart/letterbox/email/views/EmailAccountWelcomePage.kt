package org.dizitart.letterbox.email.views

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.dizitart.letterbox.email.controllers.AccountWizardController
import tornadofx.*


class EmailAccountWelcomePage : LetterBoxWizardPage() {
    private val controller: AccountWizardController by di()

    override var next: LetterBoxWizardPage?= null

    override val page = vbox {
        vgrow = Priority.ALWAYS
        alignment = Pos.CENTER
        label {
            addClass(BaseLetterBoxTheme.logo)
            text = "LetterBox"
        }
        pane {
            minHeight = 20.0
        }
        label {
            addClass(BaseLetterBoxTheme.caption)
            text = "Welcome to your Supercharged Email Client"
        }
        pane {
            minHeight = 40.0
        }
        label {
            textFill = Color.DARKGRAY
            text = "Choose your email provider"
        }
        pane {
            minHeight = 10.0
        }
        tilepane {
            vgap = 20.0
            hgap = 50.0
            vgrow = Priority.ALWAYS
            padding = Insets(20.0, 100.0, 20.0, 100.0)
            style {
                backgroundColor += Color.BLUEVIOLET
            }
            for (provider in controller.accountProviders) {
                val tile = ProviderTile(provider)
                tile.setOnClick {
                    next = provider.wizardPage
                    next()
                }
                add(tile)
            }
        }
        pane {
            minHeight = 5.0
            style {
                backgroundColor += Color.BLUEVIOLET
            }
        }
    }
}