package org.dizitart.letterbox.email.service

import javafx.scene.image.Image
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.pf4j.Extension
import org.pf4j.ExtensionPoint

/**
 *
 * @author Anindya Chatterjee
 */
interface EmailAddressInfoProvider : ExtensionPoint {
    val domain: String
    fun extractAvatarImage(address: EmailAddress): Image?
}

@Extension
class DummyEmailAddressInfoProvider : EmailAddressInfoProvider {
    override val domain: String
        get() = "dummy.com"

    override fun extractAvatarImage(address: EmailAddress): Image? {
        return null
    }
}