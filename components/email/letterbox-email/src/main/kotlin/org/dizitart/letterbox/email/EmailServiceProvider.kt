package org.dizitart.letterbox.email

import javafx.scene.image.Image
import org.dizitart.letterbox.api.ServiceProvider
import org.dizitart.letterbox.api.StageMode
import org.dizitart.letterbox.api.views.PageView
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.common.styles.IconSet
import org.springframework.stereotype.Component
import tornadofx.find

/**
 *
 * @author Anindya Chatterjee
 */

@Component
class EmailServiceProvider : ServiceProvider {
    private val iconSet: IconSet by configBean(APP_ICON_SET)

    override val name: String = "EmailService"

    override val enabled: Boolean = true

    override val ordinal: Int = 0

    override val tabImage: Image = iconSet.emailButtonImage

    override lateinit var pageView: PageView

    override fun init() {
        pageView = find(EmailView::class)
        pageView.init()
    }

    override fun calculateStageMode(): StageMode {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}