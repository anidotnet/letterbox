package org.dizitart.letterbox.email.views

import javafx.geometry.Pos
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class PageButton : Fragment() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)

    override val root = hbox {
        alignment = Pos.CENTER_LEFT
        button {
            addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
            image = iconSet.backImage
        }
        vbox {
            region {
                minHeight = 7.0
            }
            label {

            }
        }
        vbox {
            region {
                minHeight = 7.0
            }
            label {

            }
        }
        vbox {
            region {
                minHeight = 7.0
            }
            label {

            }
        }
        button {
            addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
            image = iconSet.rightImage
        }
    }
}