package org.dizitart.letterbox.email.views

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.email.i18n.MENU_EMAIL_ATTACHMENT_SAVE_ALL
import org.dizitart.letterbox.email.i18n.MENU_EMAIL_INFO
import org.dizitart.letterbox.email.i18n.MENU_EMAIL_LABELS
import org.dizitart.letterbox.email.controllers.EmailDetailsController
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class EmailDetailsView : View() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    private val controller: EmailDetailsController by di()
    private lateinit var actionButtonBar: HBox
    private lateinit var favoriteRegion: VBox

    override val root = vbox {
        vgrow = Priority.ALWAYS
        hbox {
            pane {
                minWidth = 10.0
            }
            imageview {
                image = iconSet.warningImage
                controller.warningIndicator = this
                managedProperty().bind(visibleProperty())
            }
            pane {
                minWidth = 10.0
                visibleProperty().bind(controller.warningIndicator.visibleProperty())
                managedProperty().bind(visibleProperty())
            }
            copyablelabel {
                addClass(BaseLetterBoxTheme.subjectLabelLarge)
                hgrow = Priority.ALWAYS
                controller.subjectLabel = this
                minHeight = 25.0
            }
            pane {
                minWidth = 20.0
            }
        }
        pane {
            minHeight = 20.0
        }
        vbox {
            vgrow = Priority.ALWAYS
            addClass(BaseLetterBoxTheme.whiteBg)
            hbox {
                pane {
                    minWidth = 10.0
                }
                avatar {
                    size = 60.0
                    controller.avatar = this
                }
                pane {
                    minWidth = 20.0
                }
                vbox {
                    hgrow = Priority.ALWAYS
                    spacing = 5.0
                    emailaddress(large = true) {
                        controller.senderLabel = this
                    }
                    add(RecipientsFragment::class) {
                        controller.recipients = this
                    }
                }
                region {
                    minWidth = 20.0
                    hgrow = Priority.SOMETIMES
                }
                vbox {
                    favoriteRegion = this
                    alignment = Pos.TOP_RIGHT
                    button {
                        addClass(BaseLetterBoxTheme.switchButton)
                        controller.favoriteButton = this
                    }
                    region {
                        vgrow = Priority.ALWAYS
                        minHeight = 10.0
                    }
                    label {
                        addClass(BaseLetterBoxTheme.dateLabel)
                        isWrapText = true
                        controller.dateLabel = this
                    }
                }
                pane {
                    minWidth = 20.0
                }
            }
            pane {
                minHeight = 10.0
            }
            vbox {
                padding = Insets(0.0, 10.0, 0.0, 20.0)
                vgrow = Priority.ALWAYS
                hbox {
                    isVisible = false
                    actionButtonBar = this
                    alignment = Pos.TOP_RIGHT
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.replyImage
                        action {
                            controller.reply()
                        }
                    }
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.replyAllImage
                        action {
                            controller.replyAll()
                        }
                    }
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.forwardImage
                        action {
                            controller.forward()
                        }
                    }
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.trashImage
                        action {
                            controller.trash()
                        }
                    }
                    menubutton {
                        addClass(BaseLetterBoxTheme.dropdownButton, BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.verticalMenuImage
                        item(I18N.get(MENU_EMAIL_LABELS)) {
                            image = iconSet.labelOutlineImage
                            action {
                                controller.markLabels()
                            }
                        }
                        item(I18N.get(MENU_EMAIL_INFO)) {
                            image = iconSet.infoImage
                            action {
                                controller.info()
                            }
                        }
                        separator { }
                        // plugin menu
                    }
                }
                webview {
                    vgrow = Priority.ALWAYS
                    controller.bodyView = this
                }
                hbox {
                    controller.attachmentRegion = this
                    menubutton {
                        addClass(BaseLetterBoxTheme.dropdownButton, BaseLetterBoxTheme.borderlessButton,
                                BaseLetterBoxTheme.smallText, BaseLetterBoxTheme.darkIcon)
                        image = iconSet.attachmentImage
                        controller.attachmentMenu = this
                    }
                    region {
                        hgrow = Priority.ALWAYS
                    }
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.smallText)
                        text = I18N.get(MENU_EMAIL_ATTACHMENT_SAVE_ALL)
                        controller.saveAttachmentButton = this
                    }
                }

                setOnMouseEntered {
                    actionButtonBar.isVisible = true
                }

                setOnMouseExited { event: MouseEvent? ->
                    event?.let {
                        val screenBounds = localToScreen(boundsInLocal)
                        if (!screenBounds.contains(event.screenX, event.screenY)) {
                            actionButtonBar.isVisible = false
                        }
                    }
                }
            }
        }
    }
}

