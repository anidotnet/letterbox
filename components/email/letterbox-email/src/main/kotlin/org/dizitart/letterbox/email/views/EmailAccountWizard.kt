package org.dizitart.letterbox.email.views

import javafx.scene.layout.AnchorPane
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */

class EmailAccountWizard : View() {
    override val root = vbox {
        AnchorPane.setTopAnchor(this, 0.0)
        AnchorPane.setRightAnchor(this, 0.0)
        AnchorPane.setBottomAnchor(this, 0.0)
        AnchorPane.setLeftAnchor(this, 0.0)

        add(EmailAccountWelcomePage::class)
    }
}

