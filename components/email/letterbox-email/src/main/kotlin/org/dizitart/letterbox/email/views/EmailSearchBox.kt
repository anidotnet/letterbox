package org.dizitart.letterbox.email.views

import javafx.scene.layout.Priority
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.email.i18n.TEXT_SEARCH_MAIL
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailSearchBox : View() {
    override val root = hbox {
        textfield {
            addClass(BaseLetterBoxTheme.searchBox)
            promptText = I18N.get(TEXT_SEARCH_MAIL)
            hgrow = Priority.ALWAYS
        }
    }
}
