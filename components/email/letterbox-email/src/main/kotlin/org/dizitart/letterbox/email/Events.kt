package org.dizitart.letterbox.email

import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.models.EmailMessageModel
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
object StartAccountSync : FXEvent()
object AccountWizardCompleted : FXEvent()
data class EmailFolderSelected(val emailFolder: EmailFolderModel) : FXEvent()
data class EmailMessageSelected(val emailMessage: EmailMessageModel?) : FXEvent()
data class SyncEmailMessages(val emailFolder: EmailFolderModel) : FXEvent()
data class EmailMessageRead(val emailMessage: EmailMessageModel?) : FXEvent()