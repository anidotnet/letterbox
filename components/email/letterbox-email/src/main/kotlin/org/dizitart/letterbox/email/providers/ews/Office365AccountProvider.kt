package org.dizitart.letterbox.email.providers.ews

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension

@Extension
class Office365AccountProvider: EmailAccountProvider {
    override val providerColor: Color = Color.LIGHTCORAL
    override val providerLogo = Image("/images/office365.png", 50.0, 50.0, true, true)
    override val providerName = "Office365"
    override val wizardPage: LetterBoxWizardPage? = null
}