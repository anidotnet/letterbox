package org.dizitart.letterbox.email.views

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Priority
import javafx.stage.Popup
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.email.controllers.EmailAddressController
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailAddressFragment(var emailAddress: EmailAddress? = null,
                           closable: Boolean = false,
                           large: Boolean = false,
                           private val decorated: Boolean = false) : Fragment() {

    private val controller: EmailAddressController by di()
    private val iconSet: IconSet by configBean(APP_ICON_SET)

    private var closeAction: () -> Unit = {}
    private var expanded: Boolean = false

    private lateinit var nameLabel: Label

    override val root = hbox {
        label {
            when {
                large -> addClass(BaseLetterBoxTheme.largeEmailAddressLabel)
                else -> addClass(BaseLetterBoxTheme.emailAddressLabel)
            }

            hgrow = Priority.ALWAYS
            nameLabel = this
            isWrapText = false
        }

        setOnMouseClicked { event: MouseEvent? ->
            event?.let {
                if (it.button == MouseButton.PRIMARY && !expanded) {
                    val screenBounds = localToScreen(boundsInLocal)
                    popUp().show(this, screenBounds.minX, screenBounds.maxY)
                    expanded = true
                }
            }
        }

        if (closable) {
            button {
                addClass(BaseLetterBoxTheme.borderlessButton)
                graphic = ImageView(iconSet.closeImage)
                action {
                    closeAction()
                }
            }
        }

    }

    fun onClose(op: () -> Unit) {
        closeAction = op
    }

    fun render() {
        nameLabel.text = emailAddress?.name
        if (decorated) {
            nameLabel.text += ";"
        }
    }

    private fun popUp(): Popup {
        val popUp = Popup()
        popUp.content.add(
                vbox {
                    addClass(BaseLetterBoxTheme.addressPopup)
                    hbox {
                        padding = Insets(10.0)
                        avatar(circular = false) {
                            size = 70.0
                            render(emailAddress!!)
                        }
                        region {
                            minWidth = 30.0
                        }
                        vbox {
                            hgrow = Priority.ALWAYS
                            copyablelabel {
                                addClass(BaseLetterBoxTheme.largeEmailAddressLabel)
                                text = emailAddress?.name
                            }
                            borderpane {
                                addClass(BaseLetterBoxTheme.lineBreak)
                                minHeight = 10.0
                            }
                            region {
                                minHeight = 10.0
                            }
                            hbox {
                                alignment = Pos.CENTER_LEFT
                                imageview {
                                    image = iconSet.emailAddressImage
                                }
                                region {
                                    minWidth = 5.0
                                }
                                copyablelabel {
                                    addClass(BaseLetterBoxTheme.emailAddressLabel)
                                    text = emailAddress?.address
                                }
                            }
                        }
                    }
                    region {
                        minWidth = 15.0
                    }
                    this += controller.infoUi(emailAddress!!)
                    region {
                        minWidth = 10.0
                    }
                    hbox {
                        alignment = Pos.CENTER_RIGHT
                        button {
                            addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                            image = iconSet.saveAddressImage
                            action {
                                controller.saveAddress(emailAddress)
                            }
                        }
                        button {
                            addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                            image = iconSet.sendEmailImage
                            action {
                                controller.sendEmail(emailAddress)
                            }
                        }
                    }
                }
        )
        popUp.isHideOnEscape = true
        popUp.isAutoHide = true
        popUp.setOnAutoHide {
            expanded = !expanded
        }
        return popUp
    }
}
