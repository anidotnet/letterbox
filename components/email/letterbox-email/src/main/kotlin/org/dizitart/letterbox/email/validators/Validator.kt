package org.dizitart.letterbox.email.validators

/**
 * @author Anindya Chatterjee.
 */
interface Validator<in T> {
    fun validate(item: T) : Boolean
}