package org.dizitart.letterbox.email.service

import org.dizitart.letterbox.api.email.entities.*
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.models.EmailMessageModel
import org.dizitart.letterbox.email.store.EmailStore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

/**
 *
 * @author Anindya Chatterjee
 */
@Component
class EmailService {
    @Autowired private lateinit var emailStore: EmailStore



    fun populateMessageItems(folder: EmailFolderModel): List<EmailMessageModel> {
        val messages = emailStore.getAllMessages(folder.id)
        val models = ArrayList<EmailMessageModel>()
        messages.forEach {
            models.add(EmailMessageModel(it, folder))
        }
        return models
    }
    fun markRead(emailMessage: EmailMessageModel) {
        val message = emailStore.getMessageById(emailMessage.id)
        message?.let {
            message.unread = false
            emailStore.saveMessage(message)
        }
    }

    fun body(id: MailBoxItemId?): String? {
        return "Blah Blah Blah"
    }

    fun bodyContentType(id: MailBoxItemId?): String? {
        return "text/plain"
    }

    fun attachments(id: MailBoxItemId?): List<Attachment> {
        return emptyList()
    }

    fun isPotentialSpam(id: MailBoxItemId?): Boolean {
        return Random().nextBoolean()
    }

    var random = Random()
    fun getRecipients(id: MailBoxItemId?, type: RecipientType): List<EmailAddress> {
        return when(type) {
            RecipientType.TO -> {
                val list = mutableListOf<EmailAddress>()
                var i = 0
                while (i < random.nextInt(100)) {
                    list.add(EmailAddress("Anindya Chatterjee", "anidotnet@gmail.com"))
                    i++
                }
                list
            }
            RecipientType.CC -> {
                val list = mutableListOf<EmailAddress>()
                var i = 0
                while (i < random.nextInt(100)) {
                    list.add(EmailAddress("Sandip Chatterjee", "sandy@gmail.com"))
                    i++
                }
                list
            }
            RecipientType.BCC -> {
                val list = mutableListOf<EmailAddress>()
                var i = 0
                while (i < random.nextInt(100)) {
                    list.add(EmailAddress("Dharmadas Chatterjee", "dxchatt@gmail.com"))
                    i++
                }
                list
            }
        }
    }

    private fun getUnreadCount(folder: EmailFolder): Long {
        return emailStore.getAllMessages(folder.id).filter { it.unread }.count().toLong()
    }

    private fun getFolderOrdinal(folder: EmailFolder): Int {
        val nameLc = folder.name?.toLowerCase()
        return if (nameLc == null) {
            Int.MAX_VALUE
        } else {
            when {
                nameLc.contains("inbox") -> 1
                nameLc.contains("sent") -> 2
                nameLc.contains("delete") -> 3
                nameLc.contains("trash") -> 3
                nameLc.contains("outbox") -> 4
                nameLc.contains("draft") -> 5
                nameLc.contains("spam") -> 6
                nameLc.contains("junk") -> 6
                else -> Int.MAX_VALUE
            }
        }
    }



    private fun getChildFolders(parent: EmailFolderModel): List<EmailFolderModel> {
        val folders = emailStore.getAllChildFolders(parent.id)
        val folderModels = ArrayList<EmailFolderModel>()

        folders.forEach {
            val model = EmailFolderModel(it)
            model.account = parent.account
            model.parent = parent
            model.unreadCount = getUnreadCount(it)
            model.children.addAll(getChildFolders(model))
            folderModels.add(model)
        }
        return folderModels
    }

    fun validateAndCreateAccount(accountInfo: EmailAccountInfo) {

    }
}