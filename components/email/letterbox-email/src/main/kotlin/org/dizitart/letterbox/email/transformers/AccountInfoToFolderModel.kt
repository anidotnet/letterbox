package org.dizitart.letterbox.email.transformers

import org.dizitart.letterbox.api.email.emailFolder
import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.api.email.entities.EmailFolder
import org.dizitart.letterbox.api.mappers.Transformer
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.service.EmailFolderStoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 *
 * @author Anindya Chatterjee
 */
@Component
internal class AccountInfoToFolderModel : Transformer<EmailAccountInfo, EmailFolderModel> {
    @Autowired private lateinit var folderModelTransformer : Transformer<EmailFolder, EmailFolderModel>
    @Autowired private lateinit var storeService: EmailFolderStoreService

    override fun transform(source: EmailAccountInfo?): EmailFolderModel? {
        if (source == null) return null

        val accountFolder = emailFolder {
            id = source.emailAddress?.hashCode()?.toLong()!!
            name = source.name
            accountId = source.id
        }

        val accountFolderModel = folderModelTransformer.transform(accountFolder)!!
        accountFolderModel.apply {
            parent = null
            this.accountFolder = true
        }

        val topLevelFolders
                = storeService.topLevelFolders(source.id)
        val folderModelList = ArrayList<EmailFolderModel>()

        topLevelFolders.forEach {
            val model = folderModelTransformer.transform(it)!!
            folderModelList.add(model)
        }
        folderModelList.sortBy {
            it.ordinal
        }
        accountFolderModel.children.addAll(folderModelList)
        return accountFolderModel
    }
}