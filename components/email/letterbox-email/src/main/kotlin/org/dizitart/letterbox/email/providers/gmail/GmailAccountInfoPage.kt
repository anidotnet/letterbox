package org.dizitart.letterbox.email.providers.gmail

import javafx.geometry.Pos
import javafx.scene.layout.Priority
import org.apache.commons.validator.routines.EmailValidator
import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.dizitart.letterbox.common.errorAlert
import org.dizitart.letterbox.email.models.EmailAccountInfoModel
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.email.service.EmailAccountService
import org.dizitart.letterbox.email.views.EmailAccountWelcomePage
import tornadofx.*

class GmailAccountInfoPage : LetterBoxWizardPage() {
    private val accountService: EmailAccountService by di()
    private val account: EmailAccountInfoModel by inject()

    override val previous: LetterBoxWizardPage? by lazy { find(EmailAccountWelcomePage::class) }

    init {
        account.item = EmailAccountInfo()
    }

    override val page = vbox {
        vgrow = Priority.ALWAYS
        region {
            minHeight = 50.0
        }
        hbox {
            alignment = Pos.CENTER
            form {
                alignment = Pos.CENTER
                fieldset("Provide GMail Account Details") {
                    pane {
                        minHeight = 40.0
                    }
                    field("Name") {
                        textfield(account.name) {
                            minWidth = 300.0
                            required(message = "Account name is required")
                        }
                    }
                    pane {
                        minHeight = 20.0
                    }
                    field("Email Address") {
                        textfield(account.address) {
                            minWidth = 300.0
                            required(message = "Email address is required")
                            validator {
                                if (it.isNullOrEmpty()) {
                                    error("Email address is required")
                                } else if (!EmailValidator.getInstance().isValid(it)) {
                                    error("Not a valid email")
                                } else null
                            }
                        }
                    }
                    pane {
                        minHeight = 20.0
                    }
                    field("Password") {
                        passwordfield(account.password) {
                            minWidth = 300.0
                            required(message = "Password is required")
                        }
                    }
                    pane {
                        minHeight = 60.0
                    }
                    hbox {
                        alignment = Pos.CENTER
                        button {
                            addClass(BaseLetterBoxTheme.borderlessButton)
                            image = iconSet.finishImage
                            text = "Finish"
                            enableWhen { account.valid }
                            action {
                                runAsyncWithProgress {
                                    saveAccount()
                                } success {
                                    finish()
                                } fail {
                                    errorAlert(it)
                                }
                            }
                        }
                    }
                    account.validate(decorateErrors = true)
                }
            }
        }
    }

    private fun saveAccount() {
        account.commit()
        accountService.validateAndSaveAccount(account.item)
    }
}