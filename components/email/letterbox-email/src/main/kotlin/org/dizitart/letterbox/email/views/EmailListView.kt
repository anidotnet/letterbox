package org.dizitart.letterbox.email.views

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.ToggleGroup
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import org.dizitart.letterbox.common.i18n.I18N
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.email.i18n.*
import org.dizitart.letterbox.email.EmailMessageSelected
import org.dizitart.letterbox.email.controllers.EmailListController
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
class EmailListView : View() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    private val searchBox: EmailSearchBox by inject()
    private val pageButton: PageButton = PageButton()
    private val controller: EmailListController by di()

    override val root = vbox {
        spacing = 15.0
        AnchorPane.setBottomAnchor(this, 0.0)
        AnchorPane.setTopAnchor(this, 0.0)
        AnchorPane.setLeftAnchor(this, 0.0)
        AnchorPane.setRightAnchor(this, 0.0)
        padding = Insets(25.0, 0.0, 0.0, 0.0)
        this += searchBox
        hbox {
            alignment = Pos.CENTER
            button {
                addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                image = iconSet.selectAllImage
            }
            button {
                addClass(BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                image = iconSet.syncImage
            }
            region {
                HBox.setHgrow(this, Priority.ALWAYS)
            }
            this += pageButton
            region {
                HBox.setHgrow(this, Priority.ALWAYS)
            }
            menubutton {
                addClass(BaseLetterBoxTheme.dropdownButton, BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                image = iconSet.filterImage
                val tg = ToggleGroup()
                radiomenuitem(I18N.get(MENU_FILTER_NONE)) {
                    toggleGroup = tg
                    image = iconSet.noFilterImage
                }
                radiomenuitem(I18N.get(MENU_FILTER_UNREAD)) {
                    toggleGroup = tg
                    image = iconSet.unreadImage
                }
                radiomenuitem(I18N.get(MENU_FILTER_FAVORITE)) {
                    toggleGroup = tg
                    image = iconSet.favoriteImageLight
                }
                // todo: add filter plugins
            }
            menubutton {
                addClass(BaseLetterBoxTheme.dropdownButton, BaseLetterBoxTheme.borderlessButton, BaseLetterBoxTheme.darkIcon)
                image = iconSet.sortImage
                val tg = ToggleGroup()
                radiomenuitem(I18N.get(MENU_SORT_DATE)) {
                    toggleGroup = tg
                    image = iconSet.dateImage
                }
                radiomenuitem(I18N.get(MENU_SORT_SUBJECT)) {
                    toggleGroup = tg
                    image = iconSet.subjectImage
                }
                radiomenuitem(I18N.get(MENU_SORT_FROM)) {
                    toggleGroup = tg
                    image = iconSet.personImage
                }
                separator {  }
                radiomenuitem(I18N.get(MENU_SORT_ASCENDING)) {
                    toggleGroup = tg
                    image = iconSet.ascendingImage
                }
            }
        }
        listview(controller.emailMessageItems) {
            controller.listView = this
            addClass(BaseLetterBoxTheme.itemList, BaseLetterBoxTheme.vScroll, BaseLetterBoxTheme.noHScroll)
            vgrow = Priority.ALWAYS
            cellFragment(EmailMessageItemFragment::class)

            selectionModel.selectedItemProperty().onChange {
                it?.let {
                    fire(EmailMessageSelected(it))
                }
            }
        }
    }
}
