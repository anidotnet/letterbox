package org.dizitart.letterbox.email.controllers

import javafx.scene.control.ListView
import org.dizitart.letterbox.email.models.EmailFolderModel
import org.dizitart.letterbox.email.models.EmailMessageModel
import org.dizitart.letterbox.common.collections.containsId
import org.dizitart.letterbox.common.collections.getById
import org.dizitart.letterbox.common.collections.update
import org.dizitart.letterbox.email.service.EmailService
import org.dizitart.letterbox.email.EmailFolderSelected
import org.dizitart.letterbox.email.SyncEmailMessages
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
@Component
class EmailListController : Controller() {
    @Autowired private lateinit var emailService: EmailService

    lateinit var listView: ListView<EmailMessageModel>
    val emailMessageItems = SortedFilteredList<EmailMessageModel>()

    init {
        subscribe<EmailFolderSelected> { event ->
            listView.selectionModel.clearSelection()
            retrieveEmailMessages(event.emailFolder)
        }

        subscribe<SyncEmailMessages> { event ->
            retrieveEmailMessages(event.emailFolder)
        }
    }

    private fun retrieveEmailMessages(folder: EmailFolderModel) {
        runAsync(true) {
            val messageItems = emailService.populateMessageItems(folder)
            runLater {
                updateMessageList(emailMessageItems, messageItems)
            }
        }
    }

    private fun updateMessageList(emailMessages: SortedFilteredList<EmailMessageModel>,
                                  messageItems: List<EmailMessageModel>) {
        emailMessages.filterNot { messageItems.containsId(it) }
                .forEach { emailMessages.remove(emailMessages.getById(it.id)) }
        messageItems.forEach {
            emailMessages.update(it)
        }
    }
}