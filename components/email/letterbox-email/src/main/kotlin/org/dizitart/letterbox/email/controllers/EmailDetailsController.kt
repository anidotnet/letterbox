package org.dizitart.letterbox.email.controllers

import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.web.WebView
import org.dizitart.letterbox.api.email.entities.Attachment
import org.dizitart.letterbox.api.email.entities.RecipientType
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import org.dizitart.letterbox.common.formatter.toLongDateTime
import org.dizitart.letterbox.email.models.EmailMessageModel
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.email.EmailMessageRead
import org.dizitart.letterbox.email.EmailMessageSelected
import org.dizitart.letterbox.email.service.EmailService
import org.dizitart.letterbox.email.views.Avatar
import org.dizitart.letterbox.email.views.CopyableLabel
import org.dizitart.letterbox.email.views.EmailAddressFragment
import org.dizitart.letterbox.email.views.RecipientsFragment
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
@Component
class EmailDetailsController : Controller() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    @Autowired private lateinit var emailService: EmailService

    lateinit var favoriteButton: Button
    lateinit var subjectLabel: CopyableLabel
    lateinit var recipients: RecipientsFragment
    lateinit var dateLabel: Label
    lateinit var warningIndicator: ImageView
    lateinit var attachmentRegion: HBox
    lateinit var attachmentMenu: MenuButton
    lateinit var saveAttachmentButton: Button
    lateinit var avatar: Avatar
    lateinit var bodyView: WebView
    lateinit var senderLabel: EmailAddressFragment

    init {
        subscribe<EmailMessageSelected> { (emailMessage) ->
            if (emailMessage != null) {
                populateFavorite(emailMessage)
                populateSubject(emailMessage)
                populateSender(emailMessage)
                populateRecipients(emailMessage)
                populateDate(emailMessage)
                populateWarning(emailMessage)
                populateAttachments(emailMessage)
                populateAvatar(emailMessage)
                populateBody(emailMessage)

                fire(EmailMessageRead(emailMessage))
            }
        }

        subscribe<EmailMessageRead> { (emailMessage) ->
            markRead(emailMessage)
        }
    }

    fun reply() {

    }

    fun replyAll() {

    }

    fun forward() {

    }

    fun markLabels() {

    }

    fun trash() {

    }

    fun info() {

    }

    private fun populateBody(emailMessage: EmailMessageModel) {
        runAsync(true) {
            val body = emailService.body(emailMessage.id)
            val contentType = emailService.bodyContentType(emailMessage.id)
            runLater {
                bodyView.engine.loadContent(body, contentType)
            }
        }
    }

    private fun populateAvatar(emailMessage: EmailMessageModel) {
        avatar.render(emailMessage.from)
    }

    private fun populateAttachments(emailMessage: EmailMessageModel) {
        if (emailMessage.attachment) {
            runAsync(true) {
                val list: List<Attachment> = emailService.attachments(emailMessage.id)
                runLater {
                    attachmentRegion.isVisible = true
                    list.forEach {
                        val menuItem = MenuItem(it.fileName)
                        //TODO: try to extract file icon by extension
                        menuItem.action {
                            //TODO: save attachment blobs
                        }
                        attachmentMenu.items.add(menuItem)
                    }
                }
            }
        } else {
            attachmentRegion.isVisible = false
        }
    }

    private fun populateWarning(emailMessage: EmailMessageModel) {
        runAsync(true) {
            val isSpam = emailService.isPotentialSpam(emailMessage.id)
            runLater {
                warningIndicator.isVisible = isSpam
            }
        }
    }

    private fun populateDate(emailMessage: EmailMessageModel) {
        dateLabel.text = emailMessage.date.toLongDateTime()
    }

    private fun populateRecipients(emailMessage: EmailMessageModel) {
        runAsync(true) {
            val recipientMap = mapOf(
                    RecipientType.TO to emailService.getRecipients(emailMessage.id, RecipientType.TO),
                    RecipientType.CC to emailService.getRecipients(emailMessage.id, RecipientType.CC),
                    RecipientType.BCC to emailService.getRecipients(emailMessage.id, RecipientType.BCC)
            )
            runLater {
                recipients.showRecipients(emailMessage.folder.account.emailAddress, recipientMap)
            }
        }
    }

    private fun populateSender(emailMessage: EmailMessageModel) {
        senderLabel.emailAddress = emailMessage.from
        senderLabel.render()
    }

    private fun populateSubject(emailMessage: EmailMessageModel) {
        subjectLabel.text = emailMessage.subject
    }

    private fun populateFavorite(emailMessage: EmailMessageModel) {
        favoriteButton.graphic = ImageView(if (emailMessage.favorite) iconSet.favoriteImage else iconSet.notFavoriteImage)
        favoriteButton.action {
            emailMessage.favorite = !emailMessage.favorite
            favoriteButton.graphic = ImageView(if (emailMessage.favorite) iconSet.favoriteImage else iconSet.notFavoriteImage)
        }
    }

    private fun markRead(emailMessage: EmailMessageModel?) {
        emailMessage?.let {
            val emailFolder = emailMessage.folder
            emailFolder?.let {
                if (emailFolder.unreadCount > 0 && emailMessage.unread) {
                    emailFolder.unreadCount = emailFolder.unreadCount - 1
                }
                emailMessage.unread = false
                runAsync(true) {
                    emailService.markRead(emailMessage)
                }
            }
        }
    }
}