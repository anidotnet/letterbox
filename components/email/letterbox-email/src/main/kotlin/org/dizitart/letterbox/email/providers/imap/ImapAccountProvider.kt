package org.dizitart.letterbox.email.providers.imap

import javafx.scene.image.Image
import javafx.scene.paint.Color
import org.dizitart.letterbox.common.providers.EmailAccountProvider
import org.dizitart.letterbox.common.views.LetterBoxWizardPage
import org.pf4j.Extension

@Extension
class ImapAccountProvider: EmailAccountProvider {
    override val providerColor: Color = Color.DARKORANGE
    override val providerLogo = Image("/images/imap.svg", 50.0, 50.0, true, true)
    override val providerName = "IMAP Account"
    override val wizardPage: LetterBoxWizardPage? = null
}

