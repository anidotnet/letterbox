package org.dizitart.letterbox.email.models

import javafx.beans.property.*
import javafx.collections.ObservableList
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.dizitart.letterbox.api.Identifiable
import org.dizitart.letterbox.api.email.StandardFolder
import tornadofx.*

class EmailFolderModel : ViewModel(), Identifiable<Long> {
    val idProperty = SimpleObjectProperty<Long>()
    override var id by idProperty

    val nameProperty = SimpleStringProperty()
    var name by nameProperty

    val accountIdProperty = SimpleLongProperty()
    var accountId by accountIdProperty

    val parentProperty = SimpleObjectProperty<EmailFolderModel>()
    var parent by parentProperty

    val unreadCountProperty = SimpleLongProperty()
    var unreadCount by unreadCountProperty

    val childrenProperty = SimpleObjectProperty<ObservableList<EmailFolderModel>>(
            mutableListOf<EmailFolderModel>().observable())
    var children by childrenProperty

    val ordinalProperty = SimpleIntegerProperty()
    var ordinal by ordinalProperty

    val accountFolderProperty = SimpleBooleanProperty(false)
    var accountFolder by accountFolderProperty

    val folderTypeProperty = SimpleObjectProperty<StandardFolder>()
    var folderType by folderTypeProperty

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other === this) return true
        if (other is EmailFolderModel) {
            return EqualsBuilder()
                    .append(id, other.id)
                    .append(unreadCount, other.unreadCount)
                    .build()!!
        }
        return false
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(id)
                .append(unreadCount)
                .build()!!
    }
}