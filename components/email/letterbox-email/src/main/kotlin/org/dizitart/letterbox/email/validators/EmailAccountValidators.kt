package org.dizitart.letterbox.email.validators

import org.dizitart.letterbox.api.email.entities.EmailAccountInfo
import org.springframework.stereotype.Component

/**
 * @author Anindya Chatterjee.
 */
@Component
class EmailAccountValidators : Validator<EmailAccountInfo> {

    override fun validate(item: EmailAccountInfo): Boolean {
        return true
    }
}