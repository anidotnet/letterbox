package org.dizitart.letterbox.email.i18n

/**
 *
 * @author Anindya Chatterjee
 */
const val MENU_FILTER_NONE = "menu.filter.none"
const val MENU_FILTER_UNREAD = "menu.filter.unread"
const val MENU_FILTER_FAVORITE = "menu.filter.favorite"

const val MENU_SORT_DATE = "menu.sort.date"
const val MENU_SORT_SUBJECT = "menu.sort.subject"
const val MENU_SORT_FROM = "menu.sort.from"
const val MENU_SORT_ASCENDING = "menu.sort.ascending"

const val MENU_SETTINGS = "menu.settings"
const val MENU_EMAIL_LABELS = "menu.email.labels"
const val MENU_EMAIL_TRASH = "menu.email.trash"
const val MENU_EMAIL_BLOCK = "menu.email.block"
const val MENU_EMAIL_INFO = "menu.email.info"
const val MENU_EMAIL_ATTACHMENT_SAVE_ALL = "menu.email.attachment.save.all"

const val TEXT_SEARCH_MAIL = "text.find.mail"
const val RECIPIENTS = "recipients"