package org.dizitart.letterbox.email.views

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.input.MouseButton
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import javafx.stage.Popup
import org.dizitart.letterbox.api.email.entities.EmailAddress
import org.dizitart.letterbox.api.email.entities.RecipientType
import org.dizitart.letterbox.common.views.image
import org.dizitart.letterbox.common.styles.BaseLetterBoxTheme
import org.dizitart.letterbox.common.styles.IconSet
import org.dizitart.letterbox.common.APP_ICON_SET
import org.dizitart.letterbox.api.config.configBean
import tornadofx.*


/**
 *
 * @author Anindya Chatterjee
 */
class RecipientsFragment : Fragment() {
    private val iconSet: IconSet by configBean(APP_ICON_SET)
    private lateinit var containerLabel: Label
    private var recipients: MutableMap<RecipientType, List<EmailAddress>> = mutableMapOf()
    private var expanded: Boolean = false

    override val root = hbox {
        label {
            hgrow = Priority.ALWAYS
            containerLabel = this

            setOnMouseClicked { event ->
                event?.let {
                    if (it.button == MouseButton.PRIMARY
                            && !expanded) {
                        val screenBounds = localToScreen(boundsInLocal)
                        popUp().show(this, screenBounds.minX, screenBounds.maxY)
                        expanded = true
                    }
                }
            }
        }
    }

    fun showRecipients(myAddress: EmailAddress?, recipients: Map<RecipientType, List<EmailAddress>>) {
        this.recipients.putAll(recipients)
        containerLabel.text = ""
        render(myAddress)
    }

    private fun render(myAddress: EmailAddress?) {
        if (recipients.isNotEmpty()) {
            val list = recipients.flatMap { it.value }

            if (myAddress != null && list.contains(myAddress)) {
                containerLabel.text += "me"
                if (list.size > 2) {
                    containerLabel.text += " and ${list.size - 1} others"
                } else if (list.size == 2) {
                    containerLabel.text += " and ${list.last().name}"
                }
            } else {
                if (list.size == 1) {
                    containerLabel.text = list.last().name
                } else {
                    containerLabel.text = "${list.size} recipients"
                }
            }
        }
    }

    private fun popUp(): Popup {
        val popUp = Popup()
        popUp.content.add(
                vbox {
                    alignment = Pos.TOP_RIGHT
                    addClass(BaseLetterBoxTheme.recipientPopup,
                            BaseLetterBoxTheme.vScroll, BaseLetterBoxTheme.noHScroll)
                    button {
                        addClass(BaseLetterBoxTheme.borderlessButton)
                        image = iconSet.closeImage
                        action {
                            popUp.hide()
                        }
                    }
                    scrollpane(fitToWidth = true) {
                        addClass(BaseLetterBoxTheme.borderless, BaseLetterBoxTheme.whiteBg)
                        minWidth = root.width * 0.8
                        hbarPolicy = ScrollPane.ScrollBarPolicy.NEVER
                        vbarPolicy = ScrollPane.ScrollBarPolicy.AS_NEEDED
                        maxHeight = 200.0

                        content = vbox {
                            style {
                                backgroundColor += Color.WHITE
                                borderStyle += BorderStrokeStyle.NONE
                            }
                            fitToHeight(this@scrollpane)

                            padding = Insets(0.0, 10.0, 10.0, 10.0)
                            for ((r, l) in recipients) {
                                if (l.isNotEmpty()) {
                                    hbox {
                                        label {
                                            padding = Insets(10.0, 0.0, 0.0, 0.0)
                                            minWidth = 50.0
                                            style {
                                                fontWeight = FontWeight.SEMI_BOLD
                                                fontSize = 14.px
                                                text = "${r.text()}: "
                                            }
                                        }
                                        region {
                                            minWidth = 10.0
                                        }
                                        flowpane {
                                            padding = Insets(10.0, 10.0, 10.0, 10.0)
                                            hgrow = Priority.ALWAYS
                                            vgap = 5.0
                                            hgap = 5.0
                                            for (address in l) {
                                                this += emailaddress(emailAddress = address, decorated = true) {
                                                    render()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        )
        popUp.isHideOnEscape = true
        popUp.isAutoHide = true
        popUp.setOnAutoHide {
            expanded = !expanded
        }
        return popUp
    }
}
