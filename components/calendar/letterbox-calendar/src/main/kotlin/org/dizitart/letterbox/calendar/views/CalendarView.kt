package org.dizitart.letterbox.calendar.views

import javafx.scene.Parent
import javafx.scene.image.Image
import javafx.scene.layout.AnchorPane
import org.dizitart.letterbox.common.views.LetterBoxMenu
import org.dizitart.letterbox.common.views.PageView
import tornadofx.*

class CalendarView : PageView("Calendar") {
    override val ordinal: Int get() = 1
    override val tabImage: Image get() = iconSet.calendarButtonImage

    override val root: Parent = anchorpane {
        AnchorPane.setTopAnchor(this, 0.0)
        AnchorPane.setRightAnchor(this, 0.0)
        AnchorPane.setBottomAnchor(this, 0.0)
        AnchorPane.setLeftAnchor(this, 0.0)

        label("Calendar") {
            AnchorPane.setTopAnchor(this, 0.0)
        }
        add(LetterBoxMenu::class)
    }
}