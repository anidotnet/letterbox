package org.dizitart.letterbox.api.di

import kotlin.reflect.KClass

/**
 * @author Anindya Chatterjee.
 */
internal class BeanRegistry {
    private val beanRepository = mutableMapOf<KClass<*>, BeanDefinition<*>>()
    private val beanFactory = BeanFactory()

    fun register(definition: BeanDefinition<*>) {
        if (beanRepository.containsKey(definition.clazz)) {
            throw DuplicateBeanDefFoundException("A bean definition for ${definition.clazz} is already registered")
        }

        beanRepository[definition.clazz] = definition
        if (definition.beanType == BeanType.Singleton
                && EagerBean::class.java.isAssignableFrom(definition.clazz.java)) {
            val bean = beanFactory.singleton(definition) as EagerBean
            bean.init()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getBean(name: String? = null, clazz: KClass<T>): T {
        val definitions = resolveBean(clazz)
        if (definitions.isEmpty()) {
            throw NoBeanDefFoundException("No bean definition found to resolve type ${clazz.java.canonicalName}")
        }

        if (name.isNullOrEmpty() && definitions.size > 1) {
            throw NoUniqueBeanDefinitionException(definitions.size, "Multiple bean definitions found to resolve type " +
                    "${clazz.java.canonicalName} - $definitions")
        }

        var beanFound = false
        var foundDefinition: BeanDefinition<*>? = null

        if (!name.isNullOrEmpty()) {
            definitions.forEach { definition ->
                if (name != null && definition.name == name) {
                    beanFound = true
                    foundDefinition = definition
                    return@forEach
                }
            }
        } else {
            beanFound = true
            foundDefinition = definitions.first()
        }

        if (!beanFound || foundDefinition == null) {
            throw NoBeanDefFoundException("No bean definition found with name $name")
        }

        foundDefinition as BeanDefinition<*>
        return beanFactory.create(foundDefinition!!) as T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getBeans(clazz: KClass<T>): List<T> {
        val definitions = resolveBean(clazz)
        if (definitions.isEmpty()) {
            throw NoBeanDefFoundException("No bean definition found to resolve type ${clazz.java.canonicalName}")
        }

        val list = mutableListOf<T>()
        definitions.forEach { definition ->
            val result = beanFactory.create(definition)
            list.add(result as T)
        }

        return list
    }

    fun destroy() {
        beanRepository.clear()
        beanFactory.destroy()
    }

    private fun <T : Any> resolveBean(clazz: KClass<T>): List<BeanDefinition<*>> {
        val beanDefinitionList = mutableListOf<BeanDefinition<*>>()
        beanRepository.forEach { type, definitions ->
            if (clazz.java.isAssignableFrom(type.java)) {
                beanDefinitionList.add(definitions)
            }
        }
        return beanDefinitionList
    }
}