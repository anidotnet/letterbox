package org.dizitart.letterbox.api.view

import javafx.geometry.BoundingBox
import javafx.geometry.NodeOrientation
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.stage.Screen
import org.dizitart.letterbox.api.addImage
import org.dizitart.letterbox.api.styles.LetterBoxGraphicsCatalog
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
abstract class ThemedView(private val showClose: Boolean = true,
                          private val showMinimize: Boolean = true,
                          private val showMaximize: Boolean = true,
                          private val titleBarHeight: Double,
                          title: String? = null,
                          icon: Node? = null) : View(title, icon) {
    private var maximized = false
    private var originalBox: BoundingBox? = null
    private var maximizedBox: BoundingBox? = null
    private var xOffset = 0.0
    private var yOffset = 0.0
    private lateinit var titleBar: HBox

    abstract val container: Parent
    open val titleBarExtensions: HBox = hbox {  }

    override val root = vbox {
        addClass(LetterBoxStylesheet.window)

        hbox {
            addClass(LetterBoxStylesheet.titleBar)
            titleBar = this
            minHeight = titleBarHeight
            maxHeight = titleBarHeight
            nodeOrientation = NodeOrientation.RIGHT_TO_LEFT
            alignment = Pos.CENTER_LEFT

            if (showClose) {
                button {
                    addClass(LetterBoxStylesheet.controlButton, LetterBoxStylesheet.lbButton)
                    addImage(LetterBoxGraphicsCatalog.closeButton)

                    action {
                        closeView()
                    }
                }
            }

            if (showMaximize) {
                button {
                    addClass(LetterBoxStylesheet.controlButton, LetterBoxStylesheet.lbButton)
                    addImage(LetterBoxGraphicsCatalog.maximizeButton)

                    action {
                        maximizeView()
                        if (maximized) {
                            this@vbox.removeClass(LetterBoxStylesheet.window)
                            this@vbox.addClass(LetterBoxStylesheet.windowMaximized)
                        } else {
                            this@vbox.removeClass(LetterBoxStylesheet.windowMaximized)
                            this@vbox.addClass(LetterBoxStylesheet.window)
                        }
                    }
                }
            }

            if (showMinimize) {
                button {
                    addClass(LetterBoxStylesheet.controlButton, LetterBoxStylesheet.lbButton)
                    addImage(LetterBoxGraphicsCatalog.minimizeButton)

                    action {
                        minimizeView()
                    }
                }
            }

            setOnMousePressed { event ->
                xOffset = event.sceneX
                yOffset = event.sceneY
            }

            setOnMouseDragged { event ->
                currentStage?.x = event.screenX - xOffset
                currentStage?.y = event.screenY - yOffset
            }

            onDoubleClick {
                if (showMaximize) {
                    maximizeView()
                }
            }
        }
    }

    open fun closeView() {
        close()
    }

    open fun maximizeView() {
        val stage = currentStage
        if (stage != null) {
            if (!maximized) {
                this.originalBox = BoundingBox(stage.x, stage.y, stage.width, stage.height)
                val screen = Screen.getScreensForRectangle(stage.x,
                        stage.y,
                        stage.width,
                        stage.height)[0]
                val bounds = screen.visualBounds
                maximizedBox = BoundingBox(bounds.minX,
                        bounds.minY,
                        bounds.width,
                        bounds.height)
                stage.x = maximizedBox?.minX!!
                stage.y = maximizedBox?.minY!!
                stage.width = maximizedBox?.width!!
                stage.height = maximizedBox?.height!!
            } else {
                stage.x = originalBox?.minX!!
                stage.y = originalBox?.minY!!
                stage.width = originalBox?.width!!
                stage.height = originalBox?.height!!
                originalBox = null
            }
            maximized = !maximized
        }
    }

    open fun minimizeView() {
        currentStage?.isIconified = true
    }

    override fun onDock() {
        root += container
        container.vgrow = Priority.ALWAYS

        titleBar += titleBarExtensions
    }
}

abstract class ModalView(title: String? = null, icon: Node? = null)
    : ThemedView(true, false, false, 30.0, title, icon)

abstract class WindowView(title: String? = null, icon: Node? = null, titleBarHeight: Double = 30.0)
    : ThemedView(true, true, true, titleBarHeight, title, icon)