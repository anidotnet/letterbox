package org.dizitart.letterbox.api.config

import org.dizitart.letterbox.api.di.LetterBoxContext
import tornadofx.*
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @author Anindya Chatterjee.
 */
@Suppress("UNCHECKED_CAST")
internal class ConfigDelegate<T : Any>(id: String) : ReadWriteProperty<Any, T?> {
    private var configElement: ConfigElement<T>

    init {
        val configRepository = LetterBoxContext.getBean(ConfigRepository::class)!!
        if (configRepository.containsId(id)) {
            configElement = configRepository[id] as ConfigElement<T>
        } else {
            configElement = ConfigElement(id)
            configRepository[id] = configElement
        }

        configElement.valueProperty.onChange {
            configRepository.markDirty()
        }
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): T? {
        return configElement.value
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T?) {
        configElement.value = value
    }
}