package org.dizitart.letterbox.api.styles

import javafx.scene.paint.Color

/**
 *
 * @author Anindya Chatterjee
 */
interface LetterBoxPalette : ColorPalette {
    val transparent: Color
    val controlBackground: Color
    val controlBorder: Color
    val buttonBorder: Color
    val buttonBackground: Color
}