package org.dizitart.letterbox.api.di

/**
 * @author Anindya Chatterjee.
 */
interface EagerBean {
    fun init()
}