package org.dizitart.letterbox.api.styles

import tornadofx.Stylesheet

/**
 *
 * @author Anindya Chatterjee
 */
abstract class PaletteAwareStylesheet : Stylesheet() {
    protected abstract val colorPalette: ColorPalette
}