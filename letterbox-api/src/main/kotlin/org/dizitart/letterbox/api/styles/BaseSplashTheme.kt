package org.dizitart.letterbox.api.styles

import tornadofx.cssclass

/**
 *
 * @author Anindya Chatterjee
 */
abstract class BaseSplashTheme(override val colorPalette: ColorPalette) : PaletteAwareStylesheet() {
    companion object {
        val screen by cssclass()
        val loadProgressBar by cssclass()
        val progressLabel by cssclass()
        val firstLargeLabel by cssclass()
        val secondLargeLabel by cssclass()
        val versionLabel by cssclass()
    }
}