package org.dizitart.letterbox.api.exceptions

import mu.KotlinLogging

/**
 *
 * @author Anindya Chatterjee
 */
val logger = KotlinLogging.logger {}

open class LetterBoxError(message: String? = "",
                          cause: Throwable? = null,
                          var reason: String = "") : RuntimeException(message, cause)

fun errorAlert(exception: Throwable) {
    logger.error(exception) {}

    if (exception is LetterBoxError) {
        val content = when {
            exception.cause != null -> "Reason:\n\n${exception.cause?.message} \n"
            exception.reason.isNotEmpty() -> "Reason:\n\n${exception.reason} \n"
            else -> ""
        }

        tornadofx.error(
                title = "Error",
                header = if (exception.message != null) exception.message!! else "",
                content = content
        )
    } else {
        tornadofx.error(
                title = "Error",
                header = if (exception.message != null) exception.message!! else "",
                content = if (exception.cause != null) "Reason:\n\n${exception.cause?.message} \n" else ""
        )
    }
}