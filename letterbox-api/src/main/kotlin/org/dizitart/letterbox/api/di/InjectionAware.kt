package org.dizitart.letterbox.api.di

import kotlin.reflect.KClass

/**
 * @author Anindya Chatterjee.
 */
interface InjectionAware

inline fun <reified T : Any> InjectionAware.inject(name: String = "") = lazy {
    LetterBoxContext.getBean(T::class, name)
            ?: throw NoBeanDefFoundException("No bean definition found to resolve type ${T::class.java.canonicalName}")
}

inline fun <reified T: Any> InjectionAware.injectAll(type: KClass<T>) = lazy {
    LetterBoxContext.getBeans(type).toSet()
}
