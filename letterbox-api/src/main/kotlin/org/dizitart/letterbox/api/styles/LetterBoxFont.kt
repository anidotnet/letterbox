package org.dizitart.letterbox.api.styles

import javafx.scene.text.Font
import org.dizitart.letterbox.api.io.ResourceLoader

/**
 *
 * @author Anindya Chatterjee
 */
abstract class LetterBoxFont {
    abstract val name: String
    abstract val fontPaths: List<String>

    fun loadFont() {
        fontPaths.forEach { path ->
            val fontLoader = ResourceLoader(this.javaClass.classLoader)
            val fontStream = fontLoader.loadResource(path)
            Font.loadFont(fontStream, -1.0)
        }
    }
}
