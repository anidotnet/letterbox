package org.dizitart.letterbox.api.di

/**
 * @author Anindya Chatterjee.
 */
internal class BeanFactory {
    private val singletonMap = mutableMapOf<String, Any>()

    fun singleton(definition: BeanDefinition<*>): Any {
        val clazzName = definition.clazz.java.canonicalName
        var instance = singletonMap[clazzName]
        if (instance == null) {
            try {
                instance = definition.builder.invoke()
            } catch (t: Throwable) {
                throw BeanCreationException("Can't instantiate bean $definition", t)
            }
            singletonMap[clazzName] = instance!!
        }
        return instance
    }

    fun create(definition: BeanDefinition<*>): Any {
        return when (definition.beanType) {
            BeanType.Singleton -> singleton(definition)
            BeanType.Prototype -> {
                try {
                    definition.builder.invoke()
                } catch (t: Throwable) {
                    throw BeanCreationException("Can't instantiate bean $definition", t)
                }
            }
        } ?: throw NoBeanDefFoundException("No bean definition found to resolve type ${definition.clazz.java.canonicalName}")
    }

    fun destroy() {
        singletonMap.clear()
    }
}