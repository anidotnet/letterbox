package org.dizitart.letterbox.api.config

import kotlin.properties.ReadWriteProperty

/**
 * @author Anindya Chatterjee.
 */
interface Configuration {
    val groups: Array<String>

    fun <T: Any> configElement(id: String): ReadWriteProperty<Any, T?> = ConfigDelegate(id)
    fun getView(): ConfigView?
}