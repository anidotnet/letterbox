package org.dizitart.letterbox.api.di

/**
 *
 * @author Anindya Chatterjee
 */
enum class BeanType {
    Singleton,
    Prototype
}