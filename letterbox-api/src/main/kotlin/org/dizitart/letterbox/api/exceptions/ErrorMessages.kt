package org.dizitart.letterbox.api.exceptions

/**
 *
 * @author Anindya Chatterjee
 */
const val EMAIL_ADDRESS_LIST_PARSING_ERROR = "error.parse.email-address-list"
const val EMAIL_ADDRESS_ENCODING_ERROR = "error.encoding.email-address"
const val EMAIL_TO_MIME_CONVERSION_ERROR = "error.conversion.email-to-mime"
const val MIME_TO_EMAIL_CONVERSION_ERROR = "error.conversion.mime-to-email"
const val EML_TO_EMAIL_CONVERSION_ERROR = "error.conversion.eml-to-email"
const val EMAIL_TO_EML_CONVERSION_ERROR = "error.conversion.email-to-eml"
const val FAILED_TO_CREATE_CONNECTION = "error.connection.create"