package org.dizitart.letterbox.api.config

import javafx.beans.property.SimpleObjectProperty
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class ConfigElement<T : Any>
internal constructor(val id: String) {
    val valueProperty = SimpleObjectProperty<T?>()
    var value by valueProperty
}