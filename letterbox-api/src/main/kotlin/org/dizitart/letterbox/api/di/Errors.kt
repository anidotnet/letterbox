package org.dizitart.letterbox.api.di

/**
 *
 * @author Anindya Chatterjee
 */
class NoBeanDefFoundException(msg: String) : Exception(msg)
class DuplicateBeanDefFoundException(msg: String) : Exception(msg)
class NoUniqueBeanDefinitionException(val numberOfBeansFound: Int, msg: String) : Exception(msg)
class BeanCreationException(msg: String, cause: Throwable) : Exception(msg, cause)