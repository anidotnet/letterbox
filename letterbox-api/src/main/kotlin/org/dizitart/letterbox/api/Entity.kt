package org.dizitart.letterbox.api

import java.io.Serializable


/**
 * @author Anindya Chatterjee.
 */
class ItemContext {
    private val contextMap = mutableMapOf<String, String>()

    operator fun get(key: String): Any? {
        return contextMap[key]
    }

    operator fun set(key: String, value: String) {
        contextMap[key] = value
    }
}

interface ContextAware {
    var itemContext: ItemContext
}

interface Identifiable<Id> {
    var id: Id
    fun createId() : Id
}

interface Entity<Id> : Serializable, ContextAware, Identifiable<Id>