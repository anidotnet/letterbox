package org.dizitart.letterbox.api.styles

import org.dizitart.letterbox.api.io.ResourceLoader
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
interface GraphicsCatalog {
    companion object {
        val resourceMap = mutableMapOf<String, Graphics>()
    }

    fun registerGraphics()

    fun registerElement(cssRule: CssRule, graphics: Graphics) {
        graphics.graphicsLoader = ResourceLoader(this.javaClass.classLoader)
        resourceMap[cssRule.render()] = graphics
    }
}