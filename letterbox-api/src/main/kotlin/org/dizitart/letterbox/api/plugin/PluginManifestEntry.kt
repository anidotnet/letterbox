package org.dizitart.letterbox.api.plugin

import org.dizitart.letterbox.api.di.BeanCatalog

/**
 *
 * @author Anindya Chatterjee
 */
interface PluginManifestEntry {
    val descriptor: PluginDescriptor
    val beanCatalog: BeanCatalog

    /**
     * Start of a plugin
     * */
    fun start()

    /**
     * Stop of a plugin
     * */
    fun stop()
}

