package org.dizitart.letterbox.api.di

/**
 * @author Anindya Chatterjee.
 */
class BeanCatalog {
    val beanDefinitions = mutableListOf<BeanDefinition<*>>()

    inline fun <reified T: Any> singleton(name: String = "", noinline builder: BeanBuilder<T>): BeanDefinition<T> {
        val definition = BeanDefinition(name, T::class, BeanType.Singleton, builder)
        beanDefinitions.add(definition)
        return definition
    }

    inline fun <reified T: Any> prototype(name: String = "" , noinline builder: BeanBuilder<T>): BeanDefinition<T> {
        val definition = BeanDefinition(name, T::class, BeanType.Prototype, builder)
        beanDefinitions.add(definition)
        return definition
    }

    inline fun <reified T: Any> get(name: String? = null): T {
        return LetterBoxContext.getBean(T::class, name)
                ?: throw NoBeanDefFoundException("No bean definition found to resolve type ${T::class.java.canonicalName}")
    }
}

fun beans(op: BeanCatalog.() -> Unit): BeanCatalog {
    val config = BeanCatalog()
    config.op()
    return config
}