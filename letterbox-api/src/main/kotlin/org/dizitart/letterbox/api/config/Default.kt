package org.dizitart.letterbox.api.config

/**
 * @author Anindya Chatterjee.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Default

