package org.dizitart.letterbox.api.styles

import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
interface LetterBoxGraphicsCatalog : GraphicsCatalog {
    companion object {
        val closeButton by cssclass()
        val maximizeButton by cssclass()
        val minimizeButton by cssclass()
        val menuButton by cssclass()
        val notificationButton by cssclass()
    }

    val closeButtonIcon: ButtonGraphics
    val maximizeButtonIcon: ButtonGraphics
    val minimizeButtonIcon: ButtonGraphics
    val menuButtonIcon: ButtonGraphics
    val notificationButtonIcon: ButtonGraphics

    override fun registerGraphics() {
        registerElement(closeButton, closeButtonIcon)
        registerElement(maximizeButton, maximizeButtonIcon)
        registerElement(minimizeButton, minimizeButtonIcon)
        registerElement(menuButton, menuButtonIcon)
        registerElement(notificationButton, notificationButtonIcon)
    }
}