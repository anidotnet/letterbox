package org.dizitart.letterbox.api.mappers

/**
 *
 * @author Anindya Chatterjee
 */
interface Transformer<in Source, out Target> {
    fun transform(source: Source?): Target?
}

fun <S, T> Iterable<S>.transform(transformer: Transformer<S, T>) : Iterable<T> {
    return this.map { s -> transformer.transform(s)!! }
}

