package org.dizitart.letterbox.api.di

import kotlin.reflect.KClass

/**
 * @author Anindya Chatterjee.
 */
object LetterBoxContext {
    private val beanRegistry: BeanRegistry = BeanRegistry()

    fun register(vararg beanConfigs: BeanCatalog) {
        beanConfigs.forEach { beanConfiguration ->
            beanConfiguration.beanDefinitions.forEach(beanRegistry::register)
        }
    }

    fun <T: Any> getBean(clazz: KClass<T>, name: String? = null): T? {
        return beanRegistry.getBean(name, clazz)
    }

    fun <T: Any> getBeans(clazz: KClass<T>): List<T> {
        return beanRegistry.getBeans(clazz)
    }

    fun destroy() {
        beanRegistry.destroy()
    }
}