package org.dizitart.letterbox.api

import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ButtonBase
import javafx.scene.control.Labeled
import javafx.scene.control.MenuButton
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import org.dizitart.letterbox.api.io.ResourceLoader
import org.dizitart.letterbox.api.styles.ButtonGraphics
import org.dizitart.letterbox.api.styles.GraphicsCatalog
import org.dizitart.letterbox.api.styles.NodeGraphics
import tornadofx.CssRule
import kotlin.collections.set

/**
 * @author Anindya Chatterjee.
 */
private const val BUTTON_GRAPHICS = "ButtonGraphics"
private const val ACTIVE = "Active"
private const val GRAPHIC_SIZE = "GraphicSize"

fun <T : Node> T.addImage(cssClass: CssRule, graphicSize: Double = 16.0) = apply {
    if (GraphicsCatalog.resourceMap.containsKey(cssClass.render())) {
        when (this) {
            is Button -> this.addImage(graphicSize, cssClass)
            is MenuButton -> this.addImage(graphicSize, cssClass)
            is Labeled -> this.addImage(graphicSize, cssClass)
        }
    }
}

fun ButtonBase.setActive(active: Boolean) {
    this.properties[ACTIVE] = active
    val buttonGraphics = properties[BUTTON_GRAPHICS]
    val graphicSize = properties[GRAPHIC_SIZE]
    if (buttonGraphics != null && buttonGraphics is ButtonGraphics
            && graphicSize != null && graphicSize is Double) {
        val graphicsLoader = buttonGraphics.graphicsLoader
        if (graphicsLoader != null && buttonGraphics.activeImage != buttonGraphics.image) {
            val hoverImage = Image(graphicsLoader.loadResource(buttonGraphics.hoverImage), graphicSize, graphicSize, true, true)
            if (active) {
                val activeImage = Image(graphicsLoader.loadResource(buttonGraphics.activeImage), graphicSize, graphicSize, true, true)
                graphic = ImageView(activeImage)

                setOnMouseEntered {
                    graphic = ImageView(hoverImage)
                }

                setOnMouseExited {
                    graphic = ImageView(activeImage)
                }
            } else {
                val image = Image(graphicsLoader.loadResource(buttonGraphics.image), graphicSize, graphicSize, true, true)
                graphic = ImageView(image)

                setOnMouseEntered {
                    graphic = ImageView(hoverImage)
                }

                setOnMouseExited {
                    graphic = ImageView(image)
                }
            }
        }
    }
}

fun ButtonBase.addImage(graphicSize: Double = 16.0, cssClass: CssRule) {
    val buttonGraphics = (GraphicsCatalog.resourceMap[cssClass.render()] ?: return)
            as? ButtonGraphics ?: return
    addImage(graphicSize, buttonGraphics)
}

fun ButtonBase.addImage(graphicSize: Double = 16.0, buttonGraphics: ButtonGraphics) {
    properties[BUTTON_GRAPHICS] = buttonGraphics
    addImage(graphicSize, buttonGraphics.activeImage, buttonGraphics.image, buttonGraphics.hoverImage, buttonGraphics.graphicsLoader)
}

fun ButtonBase.addImage(graphicSize: Double = 16.0, activeImage: String,
                        image: String, hoverImage: String, graphicsLoader: ResourceLoader?) {
    properties[GRAPHIC_SIZE] = graphicSize
    if (graphicsLoader != null) {
        addImage(
                Image(graphicsLoader.loadResource(image), graphicSize, graphicSize, true, true),
                Image(graphicsLoader.loadResource(hoverImage), graphicSize, graphicSize, true, true),
                Image(graphicsLoader.loadResource(activeImage), graphicSize, graphicSize, true, true)
        )
    } else {
        addImage(
                Image(image, graphicSize, graphicSize, true, true),
                Image(hoverImage, graphicSize, graphicSize, true, true),
                Image(activeImage, graphicSize, graphicSize, true, true)
        )
    }
}

fun ButtonBase.addImage(image: Image, hoverImage: Image, activeImage: Image) {
    graphic = ImageView(image)

    setOnMouseEntered {
        graphic = ImageView(hoverImage)
    }

    setOnMouseExited {
        graphic = ImageView(image)
    }
}

fun Labeled.addImage(graphicSize: Double = 16.0, cssClass: CssRule) {
    val nodeGraphics = (GraphicsCatalog.resourceMap[cssClass.render()] ?: return) as? NodeGraphics ?: return
    addImage(graphicSize, nodeGraphics)
}

fun Labeled.addImage(graphicSize: Double = 16.0, nodeGraphics: NodeGraphics) {
    properties[BUTTON_GRAPHICS] = nodeGraphics
    addImage(graphicSize, nodeGraphics.image, nodeGraphics.graphicsLoader)
}

fun Labeled.addImage(graphicSize: Double = 16.0, image: String, graphicsLoader: ResourceLoader?) {
    properties[GRAPHIC_SIZE] = graphicSize
    if (graphicsLoader != null) {
        addImage(Image(graphicsLoader.loadResource(image), graphicSize, graphicSize, true, true))
    } else {
        addImage(Image(image, graphicSize, graphicSize, true, true))
    }
}

fun Labeled.addImage(image: Image) {
    graphic = ImageView(image)
}