package org.dizitart.letterbox.api.database

import org.dizitart.no2.FindOptions
import org.dizitart.no2.Nitrite
import org.dizitart.no2.objects.Cursor
import org.dizitart.no2.objects.ObjectFilter
import org.dizitart.no2.objects.ObjectRepository
import org.dizitart.no2.objects.filters.ObjectFilters.eq
import org.dizitart.letterbox.api.Entity

/**
 * @author Anindya Chatterjee.
 */
class RepositoryOperation<Id, T : Entity<Id>>(db: Nitrite, clazz: Class<T>) {

    private val repository: ObjectRepository<T> = db.getRepository(clazz)

    fun save(vararg items: T) {
        items.forEach {
            it.id = it.createId()
        }
        repository.insert(items)
    }

    fun update(item: T) {
        repository.update(item)
    }

    fun remove(item: T) {
        repository.remove(item)
    }

    fun remove(id: Id) {
        repository.remove(eqId(id))
    }

    fun find(id: Id): T? {
        return repository.find(eqId(id)).firstOrDefault()
    }

    fun find(filter: ObjectFilter, findOptions: FindOptions? = null): Cursor<T> {
        return if (findOptions == null) repository.find(filter)
        else repository.find(filter, findOptions)
    }

    fun findAll(): Cursor<T> {
        return repository.find()
    }

    fun size(): Long {
        return repository.size()
    }

    private fun eqId(id: Id) = eq("id", id)
}