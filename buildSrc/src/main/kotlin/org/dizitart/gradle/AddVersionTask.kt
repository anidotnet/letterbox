package org.dizitart.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File

/**
 *
 * @author Anindya Chatterjee
 */
open class AddVersionTask : DefaultTask() {

    @TaskAction
    fun addVersion() {
        println("Generating version info")

        val group = if ("${project.parent?.group}".isNotEmpty()) "${project.parent?.group}" else "${project.group}"
        val name = project.name

        val groupSplit = group.split(".")
        val nameSplit = name.split(".", "-")

        val nameSet = groupSplit.toMutableSet()
        nameSet.addAll(nameSplit)
        val packageName = nameSet.joinToString(".")
        println("Package name - $packageName")

        val dir = File("${project.buildDir}/version-generated/source/main/" +
                packageName.replace('.', '/'))
        if (!dir.exists()) {
            dir.mkdirs()
        }

        val file = File("$dir/version.kt")
        file.createNewFile()

        val appVersion = if (project.version == "unspecified") project.parent?.version else project.version
        file.writeText(
"""
package $packageName

const val appVersion: String = "$appVersion"
""".trimIndent()
        )
    }
}