package org.dizitart.letterbox.common.di

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 *
 * @author Anindya Chatterjee
 */
class AssignableTest {

    @Test
    fun test() {
        assertTrue(I::class.java.isAssignableFrom(II::class.java))
        assertTrue(I::class.java.isAssignableFrom(A::class.java))
        assertTrue(I::class.java.isAssignableFrom(C::class.java))
        assertTrue(I::class.java.isAssignableFrom(B::class.java))
        assertTrue(I::class.java.isAssignableFrom(D::class.java))

        assertTrue(A::class.java.isAssignableFrom(C::class.java))
        assertTrue(A::class.java.isAssignableFrom(D::class.java))
        assertTrue(C::class.java.isAssignableFrom(D::class.java))

        assertTrue(C::class.java.isAssignableFrom(C::class.java))
        assertTrue(D::class.java.isAssignableFrom(D::class.java))
        assertTrue(A::class.java.isAssignableFrom(A::class.java))
        assertTrue(I::class.java.isAssignableFrom(I::class.java))
        assertTrue(B::class.java.isAssignableFrom(B::class.java))

        assertFalse(B::class.java.isAssignableFrom(I::class.java))
        assertFalse(C::class.java.isAssignableFrom(I::class.java))
        assertFalse(D::class.java.isAssignableFrom(I::class.java))
        assertFalse(A::class.java.isAssignableFrom(I::class.java))
        assertFalse(C::class.java.isAssignableFrom(A::class.java))
        assertFalse(D::class.java.isAssignableFrom(C::class.java))
        assertFalse(II::class.java.isAssignableFrom(I::class.java))
    }


    interface I

    interface II : I

    abstract class A : I

    open class C : A()
    data class D(val t: Int) : C()

    class B: I
}