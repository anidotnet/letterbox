package org.dizitart.letterbox.common.di

import org.dizitart.letterbox.api.di.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

/**
 *
 * @author Anindya Chatterjee
 */
class DITest {

    @After
    fun cleanUp() {
        context.destroy()
    }

    @Test
    fun test() {
        val config = beans {
            singleton("c") { C() }
            singleton("d") { D(get()) }
            singleton { B() }
            singleton("f") { F() }
            singleton { G() }
        }

        context.register(config)
        val b = context.getBean(B::class)
        assertNotNull(b)
        assertNotNull(b?.c)
        assertNotNull(b?.d)
        assertTrue(b?.a?.isNotEmpty()!!)
        assertTrue(b.cList.isNotEmpty())
        assertTrue(b.i.isNotEmpty())
        assertNotNull(b.e)

        val config2 = beans {
            singleton { X() }
        }
        context.register(config2)

        val x = context.getBean(X::class)
        assertNotNull(x)
        assertNotNull(x?.b)
    }

    @Test
    fun testSpring() {
        val appContext = AnnotationConfigApplicationContext(Config::class.java)
        val b = appContext.getBean(B::class.java)
        assertNotNull(b)
        assertNotNull(b.c1)
        assertNotNull(b.d1)
        assertNotNull(b.a1.isNotEmpty())
        assertNotNull(b.c1List.isNotEmpty())
        assertNotNull(b.i1.isNotEmpty())
        assertNotNull(b.e1)
        assertNotNull(b.appContext)

        appContext.register(X::class.java)

        val x = appContext.getBean(X::class.java)
        assertNotNull(x)
        assertNotNull(x.b1)
    }

    @Test(expected = DuplicateBeanDefFoundException::class)
    fun testDuplicateBean() {
        val config = beans {
            singleton("c") { C() }
            singleton("d") { D(get()) }
            singleton { B() }
            singleton("f") { F() }
            singleton { G() }
        }

        context.register(config)

        val config2 = beans {
            singleton { G() }
        }

        context.register(config2)
    }

    @Test
    fun testContextImport() {
        val config = beans {
            singleton("c") { C() }
            singleton("d") { D(get()) }
            singleton { B() }
            singleton("f") { F() }
            singleton { G() }
            singleton { H() }
            singleton { J(get()) }
        }

        context.register(config)

        val config2 = beans {
            singleton { E() }
        }

        context.register(config2)

        val b = config2.get<B>()
        assertNotNull(b)
        assertNotNull(b.c)
        assertNotNull(b.d)
        assertTrue(b.a.isNotEmpty())
        assertTrue(b.cList.isNotEmpty())
        assertTrue(b.i.isNotEmpty())
        assertNotNull(b.e)

        val config3 = beans {
            singleton { X() }
        }
        context.register(config3)

        val x = context.getBean(X::class)
        assertNotNull(x)
        assertNotNull(x?.b)
    }

    @Test
    fun testPrototype() {
        val config = beans {
            singleton { E() }
            prototype { J(get()) }
        }

        context.register(config)

        val e = context.getBean(E::class)
        assertNotNull(e)

        val j = context.getBean(J::class)
        assertNotNull(j)
        assertNotNull(j?.e)
        assertEquals(j?.e, e)

        val j1 = context.getBean(J::class)
        assertNotEquals(j, j1)
    }
}


interface I

interface II : I

abstract class A : I

open class C : A()
data class D(val b: B) : C()

open class E
open class F : E()
class G : F()
class H : InjectionAware {
    val e: E by inject()
}

class J (val e: E)

class B: I, InjectionAware {
    val a: Set<A> by injectAll(A::class)
    val c: C by inject("c")
    val d: D by inject("d")
    val cList: Set<C> by injectAll(C::class)
    val e: E by inject("f")
    val i: Set<I> by injectAll(I::class)

    @Autowired lateinit var appContext: ApplicationContext
    @Autowired lateinit var a1: Set<A>
    @Autowired @Qualifier("c") lateinit var c1: C
    @Autowired @Qualifier("d") lateinit var d1: D
    @Autowired lateinit var c1List: Set<C>
    @Autowired @Qualifier("f") lateinit var e1: E
    @Autowired lateinit var i1: Set<I>
}

@Component
class X: InjectionAware {
    val b: B by inject()

    @Autowired lateinit var b1: B
}


@Configuration
open class Config {
    @Bean open fun c(): C = C()
    @Bean open fun b(): B = B()
    @Bean open fun d(): D = D(b())
    @Bean open fun f(): F = F()
    @Bean open fun g(): G = G()
}