package org.dizitart.letterbox.common

import com.google.common.hash.Hasher
import com.google.common.hash.Hashing

/**
 * @author Anindya Chatterjee.
 */
fun hash(op: Hasher.() -> Unit) : Long {
    val hf = Hashing.sha512()
    val hc = hf.newHasher()
    op(hc)
    return hc.hash().asLong()
}

fun Hasher.appendString(string: String?)= this.putString(string ?: "", Charsets.UTF_8)!!
fun Hasher.appendLong(long: Long?)= this.putLong(long ?: 0)!!
fun Hasher.appendInt(int: Int?)= this.putInt(int ?: 0)!!
fun Hasher.appendByteArray(array: ByteArray?) = this.putBytes(array ?: kotlin.byteArrayOf())!!