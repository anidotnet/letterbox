package org.dizitart.letterbox.common

import java.text.NumberFormat
import java.util.*

/**
 * Simple stop watch, allowing for timing of a number of tasks,
 * exposing total running time and running time for each named task.
 *
 *
 * Conceals use of `System.currentTimeMillis()`, improving the
 * readability of application code and reducing the likelihood of calculation errors.
 *
 *
 * Note that this object is not designed to be thread-safe and does not
 * use synchronization.
 *
 *
 * This class is normally used to verify performance during proof-of-concepts
 * and in development, rather than as part of production applications.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @since May 2, 2001
 */
class StopWatch
/**
 * Construct a new stop watch with the given id.
 * Does not start any task.
 * @param id identifier for this stop watch.
 * Handy when we have output from multiple stop watches
 * and need to distinguish between them.
 */
@JvmOverloads constructor(
        /**
         * Identifier of this stop watch.
         * Handy when we have output from multiple stop watches
         * and need to distinguish between them in log or console output.
         */
        /**
         * Return the id of this stop watch, as specified on construction.
         * @return the id (empty String by default)
         * @since 4.2.2
         * @see .StopWatch
         */
        val id: String = "") {

    private var keepTaskList = true

    private val taskList = LinkedList<TaskInfo>()

    /** Start time of the current task  */
    private var startTimeMillis: Long = 0

    /** Is the stop watch currently running?  */
    /**
     * Return whether the stop watch is currently running.
     * @see .currentTaskName
     */
    var isRunning: Boolean = false
        private set

    /** Name of the current task  */
    private var currentTaskName: String? = null

    private var lastTaskInfo: TaskInfo? = null

    /**
     * Return the number of tasks timed.
     */
    var taskCount: Int = 0
        private set

    /** Total running time  */
    /**
     * Return the total time in milliseconds for all tasks.
     */
    var totalTimeMillis: Long = 0
        private set


    /**
     * Return the time taken by the last task.
     */
    val lastTaskTimeMillis: Long
        @Throws(IllegalStateException::class)
        get() {
            if (this.lastTaskInfo == null) {
                throw IllegalStateException("No tasks run: can't get last task interval")
            }
            return this.lastTaskInfo!!.timeMillis
        }

    /**
     * Return the name of the last task.
     */
    val lastTaskName: String
        @Throws(IllegalStateException::class)
        get() {
            if (this.lastTaskInfo == null) {
                throw IllegalStateException("No tasks run: can't get last task name")
            }
            return this.lastTaskInfo!!.taskName
        }

    /**
     * Return the total time in seconds for all tasks.
     */
    val totalTimeSeconds: Double
        get() = this.totalTimeMillis / 1000.0

    /**
     * Return an array of the data for tasks performed.
     */
    val taskInfo: Array<TaskInfo>
        get() {
            if (!this.keepTaskList) {
                throw UnsupportedOperationException("Task info is not being kept!")
            }
            return this.taskList.toTypedArray()
        }

    /**
     * Determine whether the TaskInfo array is built over time. Set this to
     * "false" when using a StopWatch for millions of intervals, or the task
     * info structure will consume excessive memory. Default is "true".
     */
    fun setKeepTaskList(keepTaskList: Boolean) {
        this.keepTaskList = keepTaskList
    }

    /**
     * Start a named task. The results are undefined if [.stop]
     * or timing methods are called without invoking this method.
     * @param taskName the name of the task to start
     * @see .stop
     */
    @Throws(IllegalStateException::class)
    @JvmOverloads
    fun start(taskName: String = "") {
        if (this.isRunning) {
            throw IllegalStateException("Can't start StopWatch: it's already running")
        }
        this.isRunning = true
        this.currentTaskName = taskName
        this.startTimeMillis = System.currentTimeMillis()
    }

    /**
     * Stop the current task. The results are undefined if timing
     * methods are called without invoking at least one pair
     * `start()` / `stop()` methods.
     * @see .start
     */
    @Throws(IllegalStateException::class)
    fun stop() {
        if (!this.isRunning) {
            throw IllegalStateException("Can't stop StopWatch: it's not running")
        }
        val lastTime = System.currentTimeMillis() - this.startTimeMillis
        this.totalTimeMillis += lastTime
        this.lastTaskInfo = TaskInfo(this.currentTaskName!!, lastTime)
        if (this.keepTaskList) {
            this.taskList.add(lastTaskInfo!!)
        }
        ++this.taskCount
        this.isRunning = false
        this.currentTaskName = null
    }

    /**
     * Return the name of the currently running task, if any.
     * @since 4.2.2
     * @see .isRunning
     */
    fun currentTaskName(): String? {
        return this.currentTaskName
    }

    /**
     * Return the last task as a TaskInfo object.
     */
    @Throws(IllegalStateException::class)
    fun getLastTaskInfo(): TaskInfo {
        if (this.lastTaskInfo == null) {
            throw IllegalStateException("No tasks run: can't get last task info")
        }
        return this.lastTaskInfo!!
    }


    /**
     * Return a short description of the total running time.
     */
    fun shortSummary(): String {
        return "StopWatch '$id': running time (millis) = $totalTimeMillis"
    }

    /**
     * Return a string with a table describing all tasks performed.
     * For custom reporting, call getTaskInfo() and use the task info directly.
     */
    fun prettyPrint(): String {
        val sb = StringBuilder(shortSummary())
        sb.append('\n')
        if (!this.keepTaskList) {
            sb.append("No task info kept")
        } else {
            sb.append("-----------------------------------------\n")
            sb.append("ms     %     Task name\n")
            sb.append("-----------------------------------------\n")
            val nf = NumberFormat.getNumberInstance()
            nf.minimumIntegerDigits = 5
            nf.isGroupingUsed = false
            val pf = NumberFormat.getPercentInstance()
            pf.minimumIntegerDigits = 3
            pf.isGroupingUsed = false
            for (task in taskInfo) {
                sb.append(nf.format(task.timeMillis)).append("  ")
                sb.append(pf.format(task.timeSeconds / totalTimeSeconds)).append("  ")
                sb.append(task.taskName).append("\n")
            }
        }
        return sb.toString()
    }

    /**
     * Return an informative string describing all tasks performed
     * For custom reporting, call `getTaskInfo()` and use the task info directly.
     */
    override fun toString(): String {
        val sb = StringBuilder(shortSummary())
        if (this.keepTaskList) {
            for (task in taskInfo) {
                sb.append("; [").append(task.taskName).append("] took ").append(task.timeMillis)
                val percent = Math.round(100.0 * task.timeSeconds / totalTimeSeconds)
                sb.append(" = ").append(percent).append("%")
            }
        } else {
            sb.append("; no task info kept")
        }
        return sb.toString()
    }


    /**
     * Inner class to hold data about one task executed within the stop watch.
     */
    class TaskInfo internal constructor(
            /**
             * Return the name of this task.
             */
            val taskName: String,
            /**
             * Return the time in milliseconds this task took.
             */
            val timeMillis: Long) {

        /**
         * Return the time in seconds this task took.
         */
        val timeSeconds: Double
            get() = this.timeMillis / 1000.0
    }

}

