package org.dizitart.letterbox.common.styles

import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.di.LetterBoxContext
import org.dizitart.letterbox.api.styles.*
import org.dizitart.letterbox.common.hasAnnotation
import tornadofx.ConfigProperties
import tornadofx.FX
import tornadofx.Stylesheet
import java.util.*
import kotlin.collections.set

/**
 * @author Anindya Chatterjee.
 */
object ThemeManager {
    private val graphicsDefinitions = mutableSetOf<Class<out GraphicsCatalog>>()
    private val stylesheetDefinitions = mutableSetOf<Class<out PaletteAwareStylesheet>>()
    private val fontDefinitions = mutableSetOf<Class<out LetterBoxFont>>()

    fun <T : PaletteAwareStylesheet> registerStylesheet(definition: Class<T>) = stylesheetDefinitions.add(definition)

    fun getStylesheetDefinitions(): Set<Class<out PaletteAwareStylesheet>> = stylesheetDefinitions.toSet()

    fun <T : GraphicsCatalog> registerGraphicsCatalog(definition: Class<T>) = graphicsDefinitions.add(definition)

    fun getGraphicsDefinitions(): Set<Class<out GraphicsCatalog>> = graphicsDefinitions.toSet()

    fun <T : LetterBoxFont> registerFont(definition: Class<T>) = fontDefinitions.add(definition)

    fun getFontDefinitions(): Set<Class<out LetterBoxFont>> = fontDefinitions.toSet()

    fun loadThemes() {
        // register definition of letterbox core stylesheet and graphics
        registerStylesheet(LetterBoxStylesheet::class.java)
        registerGraphicsCatalog(LetterBoxGraphicsCatalog::class.java)
        registerFont(LetterBoxFont::class.java)

        loadStylesheet()
        loadGraphics()
        loadFonts()
    }

    fun saveStylesheet(stylesheet: Class<out PaletteAwareStylesheet>) {
        saveResource(getStylesheetDefinitions(), stylesheet)
    }

    fun saveGraphics(graphicsType: Class<out GraphicsCatalog>) {
        saveResource(getGraphicsDefinitions(), graphicsType)
    }

    fun saveFont(fontType: Class<out LetterBoxFont>) {
        saveResource(getFontDefinitions(), fontType)
    }

    private fun loadStylesheet() {
        loadResource(getStylesheetDefinitions()) {
            ThemeManager.importStylesheet(this)
        }
    }

    private fun loadGraphics() {
        loadResource(getGraphicsDefinitions()) {
            registerGraphics()
        }
    }

    private fun loadFonts() {
        loadResource(getFontDefinitions()) {
            loadFont()
        }
    }

    private inline fun <reified T: Any> saveResource(definitions: Set<Class<out T>>, resourceType: Class<out T>) {
        val config = LetterBoxContext.getBean(ConfigProperties::class) ?: return
        definitions.forEach { definition ->
            if (definition.isAssignableFrom(resourceType)) {
                config[definition.name] = resourceType.name
                return
            }
        }
    }

    private inline fun <reified T: Any> loadResource(definitions: Set<Class<out T>>, crossinline action: T.() -> Unit) {
        val config = LetterBoxContext.getBean(ConfigProperties::class) ?: return
        definitions.forEach { definition ->
            val resources = LetterBoxContext.getBeans(definition.kotlin)

            if (resources.isNotEmpty()) {
                resources.forEach { resource ->
                    if (resource::class.java.hasAnnotation(Default::class)) {
                        resource.action()
                    }
                }

                val chosenType = config[definition.name] ?: return@forEach
                chosenType as String

                if (chosenType.isEmpty()) return@forEach

                resources.forEach { resource ->
                    if (chosenType == resource.javaClass.name) {
                        resource.action()
                    }
                }
            }
        }
    }

    private inline fun <reified T : Stylesheet> importStylesheet(style: T) {
        val cssString = Base64.getEncoder().encodeToString(style.render().toByteArray())
        val url = StringBuilder("css://$cssString:64")
        val urlString = url.toString()
        if (urlString !in FX.stylesheets) FX.stylesheets.add(url.toString())
    }
}