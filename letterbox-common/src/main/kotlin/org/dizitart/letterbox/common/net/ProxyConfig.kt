package org.dizitart.letterbox.common.net

/**
 *
 * @author Anindya Chatterjee
 */
class ProxyConfig {
    var host: String? = null
    var port: Int = 0
    var userName: String? = null
    var password: String? = null
    var domain: String? = null
    var socksProxy: Boolean = false
}