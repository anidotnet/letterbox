package org.dizitart.letterbox.common.fx

import javafx.scene.Scene
import javafx.scene.control.Labeled
import javafx.scene.control.MenuItem
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.paint.Color
import javafx.stage.Modality
import javafx.stage.Screen
import javafx.stage.Stage
import javafx.stage.StageStyle
import org.dizitart.letterbox.api.view.ModalView
import org.dizitart.letterbox.api.view.WindowView
import tornadofx.FX
import tornadofx.Stylesheet
import java.net.URI

/**
 *
 * @author Anindya Chatterjee
 */
fun Stylesheet.url(url: String): URI {
    val loader = this.javaClass.classLoader
    return loader.getResource(url).toURI()
}

var <T: MenuItem> T.image: Image?
    get() = (this.graphic as ImageView).image
    set(value) {
        this.graphic = ImageView(value)
    }

var <T: Labeled> T.image: Image?
    get() = (this.graphic as ImageView).image
    set(value) {
        this.graphic = ImageView(value)
    }

fun screenWidth() : Double {
    val primaryScreenBounds = Screen.getPrimary().visualBounds
    return primaryScreenBounds.width
}

fun screenHeight() : Double {
    val primaryScreenBounds = Screen.getPrimary().visualBounds
    return primaryScreenBounds.height
}

fun createWindow(view: WindowView): Stage {
    val scene = Scene(view.root)
    scene.fill = Color.TRANSPARENT

    val stage = Stage()
    stage.initStyle(StageStyle.TRANSPARENT)

    stage.scene = scene
    ResizeHelper.addResizeListener(stage)
    FX.applyStylesheetsTo(scene)
    return stage
}

fun createModal(view: ModalView): Stage {
    val scene = Scene(view.root)
    scene.fill = Color.TRANSPARENT

    val stage = Stage()
    stage.initStyle(StageStyle.TRANSPARENT)
    stage.initModality(Modality.WINDOW_MODAL)

    stage.scene = scene
    ResizeHelper.addResizeListener(stage)
    FX.applyStylesheetsTo(scene)
    return stage
}