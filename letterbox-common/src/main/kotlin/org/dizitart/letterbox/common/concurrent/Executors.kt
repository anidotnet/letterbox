package org.dizitart.letterbox.common.concurrent

import mu.KotlinLogging
import org.dizitart.letterbox.common.SCHEDULED_THREAD_NAME
import org.dizitart.letterbox.common.THREAD_NAME
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicLong

/**
 *
 * @author Anindya Chatterjee
 */
abstract class ErrorAwareThreadFactory : ThreadFactory {
    private val logger = KotlinLogging.logger {}
    private val threadCounter = AtomicLong(0L)

    abstract fun createThread(runnable: Runnable): Thread

    override fun newThread(r: Runnable): Thread {
        val thread = createThread(r)
        if (thread.uncaughtExceptionHandler == null) {
            thread.uncaughtExceptionHandler = uncaughtErrorHandler
            thread.name = thread.name + "-${threadCounter.incrementAndGet()}"
        }
        return thread
    }

    private val uncaughtErrorHandler: Thread.UncaughtExceptionHandler
        get() = Thread.UncaughtExceptionHandler { t, e ->
            logger.error(e) { "Unhandled error in ${t.name}" }
        }
}

private val logger = KotlinLogging.logger {}

fun daemonExecutor(): ExecutorService {
    val threadPool = ThreadPoolExecutor(10, 10,
            60L, TimeUnit.SECONDS,
            LinkedBlockingQueue<Runnable>(),
            object : ErrorAwareThreadFactory() {
                override fun createThread(runnable: Runnable): Thread {
                    val thread = Thread(runnable)
                    thread.name = THREAD_NAME
                    thread.isDaemon = true
                    return thread
                }
            })
    threadPool.allowCoreThreadTimeOut(true)
    return threadPool
}

fun executor(): ExecutorService {
    return Executors.newCachedThreadPool(object : ErrorAwareThreadFactory() {
        override fun createThread(runnable: Runnable): Thread {
            val thread = Thread(runnable)
            thread.name = THREAD_NAME
            return thread
        }
    })
}

fun scheduledExecutor(): ScheduledExecutorService {
    return Executors.newScheduledThreadPool(1,
            object : ErrorAwareThreadFactory() {
                override fun createThread(runnable: Runnable): Thread {
                    val thread = Thread(runnable)
                    thread.name = SCHEDULED_THREAD_NAME
                    thread.isDaemon = true
                    return thread
                }
            })
}

fun shutdownAndAwaitTermination(pool: ExecutorService, timeout: Int) {
    synchronized(pool) {
        // Disable new tasks from being submitted
        pool.shutdown()
    }
    try {
        // Wait a while for existing tasks to terminate
        if (!pool.awaitTermination(timeout.toLong(), TimeUnit.SECONDS)) {
            synchronized(pool) {
                pool.shutdownNow() // Cancel currently executing tasks
            }
            // Wait a while for tasks to respond to being cancelled
            if (!pool.awaitTermination(timeout.toLong(), TimeUnit.SECONDS)) {
                logger.error { "Executor did not terminate" }
            }
        }
    } catch (ie: InterruptedException) {
        // (Re-)Cancel if current thread also interrupted
        synchronized(pool) {
            pool.shutdownNow()
        }
        // Preserve interrupt status
        Thread.currentThread().interrupt()
    }
}