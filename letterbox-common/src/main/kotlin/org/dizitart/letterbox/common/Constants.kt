package org.dizitart.letterbox.common

import java.io.File

/**
 *
 * @author Anindya Chatterjee
 */
const val APP_NAME = "LetterBox"
const val APP_VERSION = "0.1.0"
const val SCHEDULED_THREAD_NAME = "ScheduledWorker.$APP_NAME"
const val THREAD_NAME = "Worker.$APP_NAME"

const val FORMAT_SUMMARY_DATE = "dd-MM-yyyy"

const val APP_ICON = "icons/icon.png"
val APP_DATA_DIR = System.getProperty("user.home") + File.separator + ".letterbox"
const val APP_CONFIG_FILE = "letterbox.properties"

const val APP_THEME = "app.theme"
const val APP_ICON_SET = "app.icons"
const val LAST_OPENED_PAGE = "page.saved"
const val MAIN_WINDOW_MAXIMIZED = "window.main.maximized"
const val MAIN_WINDOW_WIDTH = "window.main.width"
const val MAIN_WINDOW_HEIGHT = "window.main.height"
const val MAIL_TREE_WIDTH = "mail.folder-tree.width"
const val MAIL_LIST_WIDTH = "mail.folder-list.width"

const val SPLASH_WIDTH = 600.0
const val SPLASH_HEIGHT = 375.0

const val EMAIL_STORE_PATH = "email.store.path"
const val ACCOUNT_STORE_PATH = "account.store.path"
const val DEFAULT_FONT_FAMILY = "font.family.default"
const val DEFAULT_FONT_SIZE = "font.size.default"
