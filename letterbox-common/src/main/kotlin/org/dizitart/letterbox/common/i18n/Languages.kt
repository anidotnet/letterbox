package org.dizitart.letterbox.common.i18n

import java.util.*

/**
 *
 * @author Anindya Chatterjee
 */
enum class Languages(val locale: Locale) {
    ENGLISH(Locale.ENGLISH),
    FRENCH(Locale.FRENCH),
    GERMAN(Locale.GERMAN),
    SIMPLIFIED_CHINESE(Locale.SIMPLIFIED_CHINESE),
    TRADITIONAL_CHINESE(Locale.TRADITIONAL_CHINESE),
    ITALIAN(Locale.ITALIAN),
    JAPANESE(Locale.JAPANESE),
    KOREAN(Locale.KOREAN),
    TAIWAN(Locale.TAIWAN),
}