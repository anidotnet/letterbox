package org.dizitart.letterbox.common.collections

import javafx.collections.ObservableList
import org.dizitart.letterbox.api.Identifiable

/**
 *
 * @author Anindya Chatterjee
 */
inline infix fun <reified Id, reified T : Identifiable<Id>> MutableList<T>.update(item: T) : Boolean {
    if (this.containsId(item)) {
        val element = this.getById(item.id)
        element?.let {
            if (element != item) {
                this.remove(element)
                this.add(item)
                return true
            }
        }
    } else {
        this.add(item)
        return true
    }
    return false
}

inline infix fun <reified Id, reified T : Identifiable<Id>> List<T>.containsId(item: T): Boolean {
    return this.any { it == item }
}

inline infix fun <reified Id, reified T : Identifiable<Id>> List<T>.getById(id: Id): T? {
    return this.firstOrNull { it.id == id }
}

inline infix fun <reified T> ObservableList<out T>.contentEquals(other: ObservableList<out T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}

inline infix fun <reified T> List<T>.contentEquals(other: ObservableList<out T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}

inline infix fun <reified T> ObservableList<out T>.contentEquals(other: List<T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}